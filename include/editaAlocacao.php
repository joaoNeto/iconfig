<?php

// A sessão precisa ser iniciada em cada página diferente

    @session_start();
    
   
$nivel_necessario = '5';

// Verifica se não há a variável da sessão que identifica o usuário
if (isset($_SESSION['UsuarioID']) && ($_SESSION['UsuarioNivel'] == $nivel_necessario || $_SESSION['UsuarioNivel'] == 1)) {
	

?>

<?php 
	require 'dados/trava.php';
	date_default_timezone_set('America/Sao_Paulo');

?><head>

<script src="js/jquery.maskedinput.js" type="text/javascript"></script>
<script src="js/EditaAlocacaoJS.js" type="text/javascript"></script>
</head>

<title>Editar Alocação</title>
<center>
<?php 
require 'dados/conexao.php';
	mysql_query("SET NAMES UTF8;");
        
require 'dados/alocacao.php';
	

            
if($_SESSION['UsuarioNivel'] == 1){
    $TP = 1;
    $setor = null;
    $time = null;
    $wherePro = "";
}else{
    $TP=0;
     $resultSetoTime = getSetorTimeUsuario($_SESSION['UsuarioID']);
   
     $setor = $resultSetoTime['id_setor'];
     $time  = $resultSetoTime['id_time'];
     $wherePro = " WHERE id_setor=$setor AND id_time=$time ";    
     }

if(isset($_GET['Col']) && isset($_GET['Mes'])){
    $funcionario = $_GET['Col'];
    
if(isset($_GET['Mes']) && isset($_GET['Ano'])){
    
    $mes  = base64_decode($_GET['Mes']);
    $ano  = base64_decode($_GET['Ano']);
}else{
    $mes  = date('m');
    $ano = date('Y');
}

$mesdoano = $mes;
if($mesdoano <10){
    $mesdoano = substr($mesdoano,1);
}else{
    $mesdoano = $mesdoano;
}
$num = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
$inicioMes = $ano.'-'.$mes.'-'.'01 00:00:00';
$fimMes    = $ano.'-'.$mes.'-'.$num.' 00:00:00';
$meses = array(1 => "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", 
"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");


$GetPro =  mysql_query("SELECT distinct pro.id_profissional, 
                               pro.nome_profissional
                          FROM control_profissional pro,
                               control_alocacao al
                          WHERE al.id_colaborador = $funcionario
                            AND al.id_colaborador = pro.id_profissional  
                            AND al.dataCrcFim >= '$inicioMes'
                            AND al.dataCrcIni <= '$fimMes'");

$GetCRC = mysql_query("SELECT distinct 
                               al.num_crc                             
                          FROM control_profissional pro,
                               control_alocacao al
                          WHERE al.id_colaborador = $funcionario
                            AND al.id_colaborador = pro.id_profissional  
                            AND al.dataCrcFim >= '$inicioMes'
                            AND al.dataCrcIni <= '$fimMes'");



$NewPro =  mysql_query("SELECT id_profissional, nome_profissional FROM control_profissional $wherePro  ORDER BY 2 ASC;");


$FetchPro = mysql_fetch_array($GetPro);
?>

<script src="js/ajaxAlocacao.js"></script>
 <div id="EditaRel">
  <form class="form-horizontal" name="FormAlocacao" action=""  enctype="multipart/form-data" method="POST">
<fieldset>

<!-- Form Name -->
<legend>Editar Alocações de <?php echo $FetchPro['nome_profissional'] ; ?> em <?php  echo $meses[$mesdoano] ; ?> </legend>
<input type="hidden" id="inicioAtual" value="<?php echo $inicioMes; ?>">
<input type="hidden" id="fimAtual" value="<?php echo $fimMes; ?>">

      <input type="hidden" id="colaboradorAtual" name="colaboradorAtual" value="<?php echo $FetchPro['id_profissional'] ;?> ">

<div class="form-group">
    <label class="col-md-4"  for="selectbasic"><span>Colaborador [Novo]</span></label>
  <div class="col-md-4">
      <select id="colaborador" name="colaborador"  class="form-control">
                        <option></option>
			<?php while($FetchNewPro = mysql_fetch_array($NewPro)){ ?>      			
			<option value="<?php echo $FetchNewPro['id_profissional'];?>"><?php echo $FetchNewPro['nome_profissional'];?></option>
			<?php } ?>
    </select>
  </div>
  
</div>

<div class="form-group">
    <label class="col-md-4"  for="selectbasic"><span>CRC [Atual]</span></label>
  <div class="col-md-4">
      <select id="crcAtual" name="crcAtual" onchange="getAlocacoes()"  class="form-control" required="">
                        <option></option>
			<?php while($FetchCRC = mysql_fetch_array($GetCRC)){ ?>      			
			<option value="<?php echo $FetchCRC['num_crc'];?>"><?php echo $FetchCRC['num_crc'];?></option>
			<?php } ?>
    </select>
  </div>
  <div class="col-md-4" style="height: 20px; margin-top: 5px;" id="resultadoCRCAtual"></div>
</div>

       <div id="result"></div>
      
 
  

<div class="form-group">
    <label class="col-md-4" for="textinput"><span>CRC [Nova]</span></label>  
  <div class="col-md-4">
    <input id="numeroCrc" name="numeroCrc" maxlength="5" placeholder="00000" value=""  class="form-control input-md branch" >
  </div>
    <div class="col-md-4" style="height: 20px; margin-top: 5px;" id="resultadoCRC"></div>
</div>
<div class="form-group">
    <label class="col-md-4" for="textinput"><span>Inicio Atividade:</span></label>  
  <div class="col-md-4">
      <input id="dataCrcIni" name="dataCrcIni" type="date" class="form-control input-md " placeholder="__/__/____"  value="" required="required">
  </div>
    <div class="col-md-4" style="height: 20px; margin-top: 5px;"  id="resultadoInicio"></div>
</div>
<div class="form-group">
    <label class="col-md-4" for="textinput"><span>Fim Atividade:</span></label>  
  <div class="col-md-4">
      <input id="dataCrcFim" name="dataCrcFim" type="date" class="form-control input-md " placeholder="__/__/____"  value="" required="required">
  </div>
      <div class="col-md-4" style="height: 20px; margin-top: 5px;" id="resultadoFim"></div>
</div>
</fieldset>
     
</form>
<br><br>
<center>
    <div id="botaoConfirma" style="margin-top: -40px;">     
        <a href="index.php?p=GerenAloc"><img src="image/Voltar.png"  title="Voltar" style="width: 3%; margin-bottom: 20px; margin-right: 10px;"></a>
        
        <input style="width: 2%;" name title="Editar Alocação" onclick="ValidaCadastroAloc()" type="image" src="image/BotaoSalvar.png" >
    </div>    
</center>

 </div> 
</center>

    


<?php 
		if(isset($_POST['crcAtual']) &&                   
                   isset($_POST['idAlocacao']) &&
                   isset($_POST['dataCrcIni']) &&
                   isset($_POST['dataCrcFim'])){

			function date_converter($_date = null) {
                        $format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';
                        if ($_date != null && preg_match($format, $_date, $partes)) {
                        return $partes[3].'-'.$partes[2].'-'.$partes[1];
                        }
                        return false;
                        }
        
                        function ValidaData($dat){
                                $data = explode("/","$dat"); // fatia a string $dat em pedados, usando / como referência
                                $d = $data[0];
                                $m = $data[1];
                                $y = $data[2];

                                // verifica se a data é válida!
                                // 1 = true (válida)
                                // 0 = false (inválida)
                                $res = checkdate($m,$d,$y);
                                if ($res == 1){
                                   return true;
                                } else {
                                   return false;
                                }
                        }
                        
                        //DADOS ATUAIS PARA USAR NA CONDIÇÃO DO UPDATE
                       $idAlocacao = @$_POST['idAlocacao'];
                                               
                        
                        
                        if(!empty($_POST['colaborador'])){
                            $colaborador = strip_tags(trim($_POST['colaborador']));
                        }  else {
                            $colaborador = strip_tags(trim($_POST['colaboradorAtual']));
                        }
                        
                        if(!empty($_POST['crc'])){
                            $crc = strip_tags(trim($_POST['crc']));
                        }else{
                            $crc = strip_tags(trim($_POST['crcAtual']));
                        }
                         
                         
			 $inicio  = $_POST['dataCrcIni'];
			 $fim   = $_POST['dataCrcFim'];
                         
                         
			$GetAloc =  mysql_query("SELECT al.id_alocacao,
                                                        al.num_crc,
                                                        al.id_colaborador
                                                   FROM control_alocacao al
                                                  WHERE al.num_crc = '$crc'
                                                    AND al.id_colaborador = '$colaborador'
                                                    AND al.dataCrcini = '$inicio'
                                                    AND al.dataCrcFim = '$fim' ");
                        $rows = mysql_num_rows($GetAloc);
			mysql_query("SET NAMES 'UTF8';");				
                        if($rows < 1){
			$gravaAlocacao = mysql_query("UPDATE control_alocacao SET "
                                . "id_colaborador =$colaborador,"
                                . "num_crc = $crc,"
                                . "dataCrcIni = '$inicio',"
                                . "dataCrcFim = '$fim'"
                                . " WHERE id_alocacao = '$idAlocacao'");
            
						
			echo '<script>alert("Editado Com Sucesso!");window.location="?p=GerenAloc";</script>' ;
		
                        }
                        else {
                            
                            
                      echo '<div style="background-color:red;"><script>alert("Esta Crc já está alocada para este colaborador com este mesmo período!");window.location="?op=EditAloc&Col='.$_GET['Col'].'&Mes='.$_GET['Mes'].'";</script></div>' ;   
                            
                        }
                        
                        }
		
                    
                
		
		?>


<?php 

}else{
   echo '<div><script>alert("Esta alocacao não existe!");window.location="?op=GerenAloc";</script></div>' ;     
}
                        }else {
     
// Destrói a sessão por segurança
	session_destroy();
	// Redireciona o visitante de volta pro login
	header("Location: index.php?p=Geren"); exit;

    
    
}?>

