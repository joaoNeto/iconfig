<?php

// A sessão precisa ser iniciada em cada página diferente

    @session_start();
    require 'dados/alocacao.php';
    $usuario = ($_SESSION['UsuarioID']);
    $setor = array();
    $setor = getSetorTimeUsuario($usuario);
    $setor = $setor['id_setor'];
     
   
  $nivel_necessario = '5';

// Verifica se não há a variável da sessão que identifica o usuário
if (isset(   $_SESSION['UsuarioID']) 
          && $_SESSION['UsuarioNivel'] == $nivel_necessario
          && isset($_GET['cod']) 
          && !empty($_GET['cod'])
          && $setor == 2) {
	
    
        require 'dados/conexao.php';
        $cod = $_GET['cod'];
         
     $sqlRelAvalidado = "SELECT qua.id_quali_rel,
                               qua.id_release,
                               qua.percentual,
                               rel.branch,
                               qua.observacao,
                               qua.arquivo,
                               qua.avaliado
                          FROM control_quali_rel qua,
                               control_release rel
                         WHERE qua.id_quali_rel = '$cod'
                           AND rel.id_release = qua.id_release";
    if($conn == true){
        mysql_query("SET NAMES UTF8");
        $resultsql = mysql_query($sqlRelAvalidado,$conn);
        $row = mysql_num_rows($resultsql);
        while($fetchResult = mysql_fetch_array($resultsql)){
              $id_quali_rel = $fetchResult['id_quali_rel'];
              $id_rel = $fetchResult['id_release'];                                
              $percentual   = $fetchResult['percentual'];
              $versao       = $fetchResult['branch'];
              $descricao    = $fetchResult['observacao'];
              $arquivo      = $fetchResult['arquivo'];
              $avaliado     = $fetchResult['avaliado'];
       
              if($avaliado == "N"){
                  $checked = 'checked';
              }else{
                  $checked = '';
              }
   
?>

<script src="js/QualidadeReleaseJS.js" ></script>
<script type="text/javascript">
    $(document).ready(function(){
        naoAvaliado();
        var na = document.getElementById('na').checked;
        if(na == false){
        PercentQualiRelease();
        }
    });
</script>
<div class="jumbotron" style="margin-left: 15%; margin-right: 15%;">
    <h2>Avaliação de Qualidade do Release: <?php echo $versao;?></h2>
    <form name="formQualidade" id="formQualidade" enctype="multipart/form-data" method="post" action="dados/editaQualidadeRelease.php">
        <input type="hidden" name="versao" value="<?php echo base64_encode($versao); ?>">
        <input type="hidden" name="id_release" value="<?php echo $id_rel; ?>">
        <input type="hidden" name="id_quali" value="<?php echo $id_quali_rel; ?>">
        <input type="hidden" name="arquivo_atual" value="<?php echo $arquivo; ?>">
        <div id="percente"><label for="Percentual" class="control-label">Percentual:</label>
        <div class="input-group col-md-2">
                
            <input id="valPercent" name="valPercent" type="text" class="form-control" onkeyup="PercentQualiRelease()" value="<?php echo $percentual; ?>" placeholder="" aria-describedby="basic-addon2">
              <span class="input-group-addon" id="basic-addon2">%</span>
              
            </div>
            <div id="resultPercent" class="input-group col-md-12" style="width: 80%;">
                <div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%;padding:0; margin-bottom:0;">
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span>
                                        Informe a Porcentagem
                                        </div>
                </div>
        </div>
        <br>
        <script>
        function naoAvaliado(){
            var na = document.getElementById('na').checked;
            if(na == true){
                document.getElementById('valPercent').value = "";
                document.getElementById('resultPercent').innerHTML = "";
                document.getElementById('valPercent').disabled = true;
                document.getElementById('valFile').disabled = true;
            }else{
                document.getElementById('valPercent').disabled = false;
                document.getElementById('valFile').disabled = false;
            }
        }
        </script>
        <div class="checkbox">
            <label>
                <input id="na" name="na" type="checkbox" onclick="naoAvaliado()" value="N" <?php echo $checked; ?>> Não Avaliado
            </label>
        </div>
<br>
     <div class="form-group">
            <label for="observacao" class="control-label">Observação:</label>
            <div id="resultObservacao" class="input-group col-md-12" style="width: 80%;"></div>
            <textarea class="form-control" style="resize: none;" id="valObservacao" name="valObservacao" rows="5"><?php echo $descricao; ?></textarea>
     </div>
     <div class="form-group">
          <div id="percente"><label for="Percentual" class="control-label">Arquivo Atual:</label>
              <a href="arquivos/QualidadeReleases/<?php echo $arquivo; ?>"><?php echo utf8_decode($arquivo); ?></a>
         <input type="file" id="valFile" name="arquivo"><div id="resultFile" class="input-group col-md-12" style="width: 80%;"></div>    
     </div>

    </form>
    <center>
    <div id="botaoConfirma">     
        <a href="index.php?p=Plan"><img src="image/Voltar.png"  title="Voltar" style="width: 4%; margin-bottom: 20px; margin-right: 10px;"></a>
        
        <input style="width: 3%;" name title="Salvar Avaliação de Qualidade" onclick="validaEditaQualiRelease()" type="image" src="image/BotaoSalvar.png" >
    </div>    
</center> 



</div>
    <?php 
    
        }
        
        } 
        
        }else {
     
// Destrói a sessão por segurança
	session_destroy();
	// Redireciona o visitante de volta pro login
        echo '<script>window.location="?p=Plan";</script>'; exit;

    
    
}?>

