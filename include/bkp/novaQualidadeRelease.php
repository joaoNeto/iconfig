<?php

// A sessão precisa ser iniciada em cada página diferente

    @session_start();
    require 'dados/alocacao.php';
    $usuario = ($_SESSION['UsuarioID']);
    $setor = array();
    $setor = getSetorTimeUsuario($usuario);
    $setor = $setor['id_setor'];
    
   
$nivel_necessario = '5';

// Verifica se não há a variável da sessão que identifica o usuário
if (isset($_SESSION['UsuarioID'])&& ($_SESSION['UsuarioNivel'] == $nivel_necessario) && $setor == 2) {
	

?>

<script src="js/QualidadeReleaseJS.js" ></script>
<script type="text/javascript">
    $(document).ready(function(){
        PercentQualiRelease();
    });
</script>
<div class="jumbotron" style="margin-left: 15%; margin-right: 15%;">
    <h2>Avaliação de Qualidade do Release: <?php echo $versao = base64_decode($_GET['B']); ?></h2>
    <form name="formQualidade" id="formQualidade" enctype="multipart/form-data" method="post" action="dados/qualidadeRelease.php">
        <input type="hidden" name="versao" value="<?php echo $_GET['B']; ?>">
        <input type="hidden" name="id_release" value="<?php echo $_GET['id']; ?>">
        
        <div id="percente"><label for="Percentual" class="control-label">Percentual:</label>
        <div class="input-group col-md-2">
                
            <input id="valPercent" name="valPercent" type="text" class="form-control" onkeyup="PercentQualiRelease()" value="" placeholder="" aria-describedby="basic-addon2">
              <span class="input-group-addon" id="basic-addon2">%</span>
              
            </div>
            <div id="resultPercent" class="input-group col-md-12" style="width: 80%;">
                <div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%;padding:0; margin-bottom:0;">
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span class="sr-only">Error:</span>
                                        Informe a Porcentagem
                                        </div>
                </div>
        </div>
<br>
     <div class="form-group">
            <label for="observacao" class="control-label">Observação:</label>
            <div id="resultObservacao" class="input-group col-md-12" style="width: 80%;"></div>
            <textarea class="form-control" style="resize: none;" id="valObservacao" name="valObservacao" rows="5"></textarea>
     </div>
     <div class="form-group">
         <input type="file" id="valFile" name="arquivo"><div id="resultFile" class="input-group col-md-12" style="width: 80%;"></div>    
     </div>

    </form>
    <center>
    <div id="botaoConfirma">     
        <a href="index.php?p=Plan"><img src="image/Voltar.png"  title="Voltar" style="width: 4%; margin-bottom: 20px; margin-right: 10px;"></a>
        
        <input style="width: 3%;" name title="Salvar Avaliação de Qualidade" onclick="validaCadastroQualiRelease()" type="image" src="image/BotaoSalvar.png" >
    </div>    
</center> 



</div>
<?php }else {
     
// Destrói a sessão por segurança
	session_destroy();
	// Redireciona o visitante de volta pro login
	header("Location: index.php?p=Plan"); exit;

    
    
}?>

