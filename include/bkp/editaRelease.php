<?php
// A sessão precisa ser iniciada em cada página diferente
    @session_start();
    $nivel_necessario = '1';
// Verifica se não há a variável da sessão que identifica o usuário
    if (isset($_SESSION['UsuarioID']) && ($_SESSION['UsuarioNivel'] == $nivel_necessario)) {
?>
<?php 
	require 'dados/trava.php';
	$dia = date("d");
	$mes = date("m");
	$ano = date("Y");
	
	date_default_timezone_set('America/Sao_Paulo');


?>
<head>
<script src="js/jquery.maskedinput.js" type="text/javascript"></script>
<script src="js/ajaxVerificaReleaseAvaliado.js" type="text/javascript"></script>

<script language="javascript">
            function mostra(item){
               
                    item.style.display='';
                    document.getElementById(item).style.display='block';
            }
            
            function requer(itemRequer){
                
                document.getElementById(itemRequer).setAttribute(required);
            }

            function esconde(item){
                item.style.display='none'
            }

            function pintadiv(item){
                var confirma;
                if(document.getElementById(item).style.backgroundColor){
                   confirma =  document.getElementById(item).style.backgroundColor='';
                }else {
                   confirma =  document.getElementById(item).style.backgroundColor= 'lightgreen' ;
                }
                    return confirma;
            }
    
            function checked(itemCheck){
                if(document.getElementById(itemCheck).checked){
                   document.getElementById(itemCheck).checked = false; 
                }
                else{
                   document.getElementById(itemCheck).checked = true;     
                }
            }
 
            function dischecked(intemDisCheck){
                document.getElementById(intemDisCheck).checked = false;
            }
    
</script>  


</head>
<title>EDITAR</title>
<center>

<?php 

    require 'dados/conexao.php';
    mysql_query("SET NAMES UTF8;");

    $id = @$_GET['Rel'];
    
    $release = mysql_query("SELECT  cli.id_cliente,
                                    cli.nome_cliente,
                                    rel.id_release,
                                    rel.branch,
                                    rel.tipo,
                                    rel.dat_ent_fab,
                                    rel.dat_ini_hom,
                                    rel.dat_pla_exp,
                                    rel.data_exp,
                                    rel.observacao,
                                    sis.id_sistema,
                                    sis.nome_sistema,
                                    rel.emergencial
                            FROM control_clientes cli, control_release rel,control_sistema sis
                           WHERE cli.id_cliente = rel.id_cliente
                             AND rel.id_sistema = sis.id_sistema 
                             AND rel.id_release = '$id';");

    $FetchRel = mysql_fetch_array($release)  ; 
    $ver = $FetchRel['branch'];  
    $cli = $FetchRel['id_cliente']; 
    $nomecli = $FetchRel['nome_cliente']; 
    $tip = $FetchRel['tipo'];
    $sis = $FetchRel['id_sistema']; 
    $nomesis = $FetchRel['nome_sistema'];
    $iddown = $FetchRel['id_release'];
    $dataEntFab = $FetchRel['dat_ent_fab'];
    $dataInHom = $FetchRel['dat_ini_hom'];
    $dataPlaExp = $FetchRel['dat_pla_exp'];
    $dataExp = $FetchRel['data_exp'];
    $obs = $FetchRel['observacao'];
    $tipoRel = $FetchRel['emergencial'];
    
    if($tipoRel == 1){
        $styleInput = 'style="display:none"';
    }  else {
        $styleInput = '';
    }

    $Getcli =  mysql_query("SELECT id_cliente, nome_cliente FROM control_clientes WHERE id_cliente <> '$cli' ORDER BY 2 ASC;");
    $Getsis =  mysql_query("SELECT id_sistema, nome_sistema FROM control_sistema WHERE id_sistema <> '$sis' ORDER BY 2 ASC;");
    $Gettip =  mysql_query("SELECT tipo FROM control_release WHERE tipo <> '$tip' ORDER BY 1 ASC;");

?>
    
<div id="EditaRel">
   
    <form class="form-horizontal" name="edicaoRelease" enctype="" method="POST">
        <fieldset>
            <legend>Edição do Release</legend>
            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic"><span>Cliente</span></label>
                    <div class="col-md-3">
                        <select id="cliente" name="cliente"  class="form-control" required="required">
                            <option value="<?php echo $cli;?>"><?php echo $nomecli;?></option>
                            <?php while($Fetchcli = mysql_fetch_array($Getcli)){ ?>      			
                            <option value="<?php echo $Fetchcli['id_cliente'];?>"><?php echo $Fetchcli['nome_cliente'];?></option>
                            <?php } ?>
                        </select>
                    </div>
            </div>

<div class="form-group">
    <label class="col-md-4 control-label" for="selectbasic"><span>Sistema</span></label>
    <div class="col-md-5">
        <select id="sistema" name="sistema" class="form-control" required="required">
            <option value="<?php echo $sis;?>"><?php echo $nomesis;?></option>
            <?php while($Fetchsis = mysql_fetch_array($Getsis)){ ?>      			
            <option value="<?php echo $Fetchsis['id_sistema'];?>"><?php echo $Fetchsis['nome_sistema'];?></option>
            <?php } ?>
        </select>
    </div>
</div>
            
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic"><span>Tipo</span></label>
    <div class="col-md-3">
        <select id="tipo" name="tipo"  class="form-control" required="required">
            <option value="<?php echo $tip;?>"><?php echo $tip;?></option>
            <option value="SAC">SAC</option>
            <option value="PRO">PRO</option>
        </select>
    </div>  
</div>
            
<div class="form-group">
    <label class="col-md-4 control-label" for="textinput"><span>Branch:</span></label>  
    <div class="col-md-4">
      <input type="text" id="branch" name="branch" maxlength="13" onkeypress='mascaraMutuario(this,cpfCnpj)' onblur='clearTimeout()'  placeholder="00.00.00.00_0" value="<?php echo $ver; ?>"   class="form-control input-md branch" required="required">
    </div>
</div>
            
<div class="form-group" <?php echo $styleInput; ?>>
    <label class="col-md-4 control-label" for="textinput"><span>Entrega Fab:</span></label>  
        <div class="col-md-4">
            <input  id="dataEntFab" name="dataEntFab"  disabled=""  class="form-control input-md "  value="<?php echo date('d/m/Y',strtotime($dataEntFab))?>" required="required">
                <div id="newEntFab" style="display: none;" > 
                    <input  id="dataEntFab2" style="margin-left: 200px; margin-top: -34px;" name="dataEntFab2"  class="form-control input-md " value="">
                    <textarea name="obsNovDataEntFab" class="form-control" rows="2" maxlength="150" style="width:383px; max-width: 383px; max-height: 50px;  height:50px; margin-top: 2px; " placeholder="Descreva o motivo da alteração!"></textarea>
                    <img src="image/naook1.jpg" title="Fechar" style="  cursor: pointer; width: 7%; margin-top:-76px; margin-right: -600px;  " onclick='esconde(newEntFab)'>
                </div>
        </div>
    <div id="opsEntFab">
        <!--<input  type="checkbox" id="ConfirmaEntFab" name="ConfirmaEntFab" value="Confirma" style=" width:0px; margin-left: -122px; margin-right: 2px;" ><img src="image/ok.ico" title="Entregue" style=" cursor: pointer; width: 4%; margin-top: -5px;"  onclick='checked("ConfirmaEntFab");pintadiv("dataEntFab")'>-->
        <img src="image/naook.jpg" title="Alterar" style="margin-left: -190px; margin-top: 5; cursor: pointer; width: 3%;" onclick='mostra(newEntFab);' >
    </div>
</div>
<div class="form-group" <?php echo $styleInput; ?>>
    <label class="col-md-4 control-label" for="textinput"><span>Início Hom:</span></label>  
        <div class="col-md-4">
            <input  id="dataInHom" name="dataInHom"  class="form-control input-md "  value="<?php echo date('d/m/Y',strtotime($dataInHom))?>" required="required" disabled="">
                <div id="newInHom" style="display: none;" >
                    <input  id="dataInHom2" style="margin-left: 200px; margin-top: -33px;" name="dataInHom2"  class="form-control input-md "  value="">
                    <textarea name="obsNovInHom" class="form-control" rows="2" maxlength="150" style="width:383px; max-width: 383px; max-height: 50px;  height:50px; margin-top: 2px; " placeholder="Descreva o motivo da alteração!" ></textarea>
                    <img src="image/naook1.jpg" title="Fechar" style="  cursor: pointer; width: 7%; margin-top:-76px; margin-right: -600px;  " onclick='esconde(newInHom)'>
                </div>
        </div>
    <div id="opsInHom">
        <img src="image/naook.jpg" title="Alterar" style=" margin-left: -190px; margin-top: 5; cursor: pointer; width: 3%;" onclick='mostra(newInHom)' >
    </div>
</div>
    
<div class="form-group" >
    <label class="col-md-4 control-label" for="textinput"><span>Plan Expedição:</span></label>  
        <div class="col-md-4">
            <input id="dataPlaExp" name="dataPlaExp" disabled=""  class="form-control input-md "  value="<?php  if($dataPlaExp == '0000-00-00 00:00:00'){
                                                                                                                        $dataPlaExp  = '0000-00-00 00:00:00';
                                                                                                                    }else{
                                                                                                                        $dataPlaExp = date('d/m/Y',strtotime($dataPlaExp));
                                                                                                                    }echo $dataPlaExp ;?> " required="required">
                <div id="newPlaExp" style="display: none;" >
                    <input  id="dataPlaExp2" style="margin-left: 200px; margin-top: -33px;" name="dataPlaExp2"  class="form-control input-md " value="" >
                    <textarea name="obsNovPlaExp" class="form-control" rows="2" maxlength="150" style="width:383px; max-width: 383px; max-height: 50px;  height:50px; margin-top: 2px; " placeholder="Descreva o motivo da alteração!" ></textarea>
                    <img src="image/naook1.jpg" title="Fechar" style="  cursor: pointer; width: 7%; margin-top:-76px; margin-right: -600px;  " onclick='esconde(newPlaExp)'>
                </div>
        </div>
    <div id="opsPlaExp">
            <img src="image/naook.jpg" title="Alterar" style="margin-left: -190px; margin-top: 5; cursor: pointer; width: 3%;" onclick='mostra(newPlaExp)' >
    </div>
</div>

<div class="form-group">
    <label class="col-md-4 control-label" for="textinput"><span>Expedição:</span></label>  
        <div class="col-md-4">
            
            <input type="button" name="crc" value="Verificar Status das CRCs" onclick="verificaReleaseAvaliado(<?php  echo $iddown; ?>);">
            <div id="Resultado">
                
            </div>
            
        </div>
    <div id="opsdataExp">
          <!--  <img src="image/ok.ico" title="Entregue" style=" cursor: pointer; width: 4%; margin-left: -180px;" onclick='pintadiv("dataExp")' >-->
    </div>
</div>  

    <label class="col-md-4 control-label" for="textinput" style="margin-left: -15px; margin-bottom: -50px;"><span>Observação:</span></label>  
        <textarea name="obs" class="form-control" rows="3" maxlength="500" style="width:495px; height:auto; margin-left: 212px; " ><?php echo $obs ;?></textarea>
</fieldset>
          <br>
            <center>
                <div style="margin-top: -20px;">
                    <a href="index.php?p=Geren"><img src="image/Voltar.png"  title="Voltar" style="width: 7%; margin-bottom: 20px; margin-right: 10px;"></a>
                    <input style="width:5%; " title="Salvar" type="image" src="image/BotaoSalvar.png">
                    <input type="hidden" name="enviar" value="Salvar">
                </div>
            </center>
    </form>
</div> 
</center>
<script type="text/javascript">
     
       jQuery("#dataEntFab").mask("99/99/9999");
       jQuery("#dataInHom").mask("99/99/9999");
       jQuery("#dataPlaExp").mask("99/99/9999");
       jQuery("#dataEntFab2").mask("99/99/9999");
       jQuery("#dataInHom2").mask("99/99/9999");
       jQuery("#dataPlaExp2").mask("99/99/9999");
       
       jQuery("#campoData2").mask("99/99/9999");
       jQuery("#campoTelefone").mask("(999) 999-9999");
       
      
</script>


<script>
    function mascaraMutuario(o,f){
 v_obj=o
 v_fun=f
 setTimeout('execmascara()',1)
 }
function execmascara(){
 v_obj.value=v_fun(v_obj.value)
 }
function cpfCnpj(v){
//Remove tudo o que não é dígito
 v=v.replace(/\D/g,"")
if (v.length <= 11) { 
    // //CPF
    // 

//Coloca um ponto entre o terceiro e o quarto dígitos
 v=v.replace(/(\d{2})(\d)/,"$1.$2")
 v=v.replace(/(\d{2})(\d)/,"$1.$2")
 v=v.replace(/(\d{2})(\d)/,"$1.$2")
//Coloca um hífen entre o terceiro e o quarto dígitos
 v=v.replace(/(\d{2})(\d{1,1})$/,"$1_$2")

}
return v
}
  
    </script>

<?php

        $ip=$_SERVER['REMOTE_ADDR'];        
        $hostname = gethostbyaddr($ip);
        $usuario = $_SESSION['UsuarioID'];

if(isset($_POST['enviar']) && $_POST['enviar'] == 'Salvar'){
    if(empty($_POST['dataEntFab2']) && !empty($_POST['obsNovDataEntFab'])){
        @$erro = 'Entrega da Fábrica';
    }
    if(!empty($_POST['dataEntFab2']) && empty($_POST['obsNovDataEntFab'])){
        
        @$erro = $erro.' Motivo de alteração ( Entrega da Fábrica )';
    }if(empty($_POST['dataInHom2']) && !empty($_POST['obsNovInHom'])){
        
        @$erro = $erro.' Início da Homologação ';
    }if(!empty($_POST['dataInHom2']) && empty($_POST['obsNovInHom'])){
        
        @$erro = $erro.' Motivo de alteração ( Início da Homologação )';
    }if(empty($_POST['dataPlaExp2']) && !empty($_POST['obsPlaExp'])){
        
        @$erro = $erro.'Planejada para Expedição';
    }if(!empty($_POST['dataPlaExp2']) && empty($_POST['obsNovPlaExp'])){
        
        @$erro = $erro.' Motivo de alteração ( Planejada para Expedição )';
    }
    
    if(!isset($erro)){ 
   
        function date_converter($_date = null) {
                        $format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';
                        if ($_date != null && preg_match($format, $_date, $partes)) {
                        return $partes[3].'-'.$partes[2].'-'.$partes[1];
                        }
                        return false;
                        }
        $cliente = strip_tags (trim($_POST['cliente']));
        $sistema = trim($_POST['sistema']);
        $tipo    = strip_tags (trim($_POST['tipo']));
        $versao  = strip_tags (trim($_POST['branch']));
        
        if(empty($_POST['dataEntFab2'])){
            $EntFab  = $dataEntFab;
        }else{
            $EntFab = date_converter($_POST['dataEntFab2']);
            $obsEntFab = trim($_POST['obsNovDataEntFab']);
            $EntFabNew = date_converter($_POST['dataEntFab2']);
            $EntFabOld = $dataEntFab;
            
            mysql_query("INSERT INTO control_log_data
                         (id_release,
                          campo,
                          data_old,
                          data_new,
                          motivo,
                          id_usuario,
                          ip,
                          hostname)
                        VALUES ('$iddown',
                                'dat_ent_fab',
                                '$EntFabOld',
                                '$EntFabNew',
                                '$obsEntFab',
                                '$usuario',
                                '$ip',
                                '$hostname')");
        }
        if(empty($_POST['dataInHom2'])){
            $InHom   = $dataInHom;
        }else{
            $InHom = date_converter($_POST['dataInHom2']);
            $obsInHom = trim($_POST['obsNovInHom']);
            $InHomNew = date_converter($_POST['dataInHom2']);
            $InHomOld = $dataInHom;
            
            mysql_query("INSERT INTO control_log_data
                         (id_release,
                          campo,
                          data_old,
                          data_new,
                          motivo,
                          id_usuario,
                          ip,
                          hostname)
                        VALUES ('$iddown',
                                'dat_ini_hom',
                                '$InHomOld',
                                '$InHomNew',
                                '$obsInHom',
                                '$usuario',
                                '$ip',
                                '$hostname')");
            
        }
        if(empty($_POST['dataPlaExp2'])){
            $PlaExp  = date_converter($dataPlaExp);
        }else{
            $PlaExp = date_converter($_POST['dataPlaExp2']);
            $obsPlaExp = trim($_POST['obsNovPlaExp']);
            $PlaExpNew = date_converter($_POST['dataPlaExp2']);
            $PlaExpOld = $dataPlaExp;
            
            mysql_query("INSERT INTO control_log_data
                         (id_release,
                          campo,
                          data_old,
                          data_new,
                          motivo,
                          id_usuario,
                          ip,
                          hostname)
                        VALUES ('$iddown',
                                'dat_pla_exp',
                                '$PlaExpOld',
                                '$PlaExpNew',
                                '$obsPlaExp',
                                '$usuario',
                                '$ip',
                                '$hostname')");
        }
      	if(!empty($_POST['dataExp'])){
           
            $Exp     = date_converter($_POST['dataExp']);
        }
	
	$obser   = trim($_POST['obs']);
			
        if($cliente <> $nomecli   ||
           $sistema <> $nomesis   ||
           $tipo    <> $tip       ||
           $versao  <> $ver       ||
           $EntFab <> $dataEntFab ||
           $InHom  <> $dataInHom  ||
           $PlaExp <> $dataPlaExp ||
           $obser  <> $obs    ){ 
        
        
        
        
			mysql_query("SET NAMES 'UTF8';");					
			//$solicitante =$soli."@informata.com.br";	
	@$gravaexeped = mysql_query("UPDATE control_release  SET id_cliente='$cliente',id_sistema='$sistema',
										tipo='$tipo',
                                                                                branch='$versao',
										dat_ent_fab='$EntFab',
										dat_ini_hom='$InHom',
										dat_pla_exp='$PlaExp',
										data_exp='$Exp',
										observacao = '$obser'																
										WHERE  id_release = '$iddown'");
        
       #variveis do log
        $logEntFab = $EntFab;
        $logInHom = $InHom;
        $logPlaExp = $PlaExp;
        $operacao = 'UPDATE';
        
        
        
        
        
        
        
        
      /*  
      $gravaexepedlog = mysql_query("INSERT INTO control_release_log (id_release,
                                               id_cliente,
                                               id_sistema,
                                               tipo,
                                               branch,
                                               dat_ent_fab,
                                               obs_ent_fab,
                                               dat_ini_hom,
                                               obs_ini_hom,
                                               dat_pla_exp,
                                               obs_pla_exp,
                                               data_exp,
                                               observacao,
                                               usuario,
                                               ip,
                                               hostname,
                                               operacao
                                               )
             VALUES ('$iddown','$cliente','$sistema','$tipo','$versao','$logEntFab','$obsEntFab','$logInHom','$obsInHom','$logPlaExp','$obsPlaExp','$Exp','$obser','$usuario','$ip','$hostname','$operacao')");
        
        
        
        
        
			$gravaexeped = mysql_query("UPDATE control_release  SET id_cliente='$cliente',id_sistema='$sistema',
										tipo='$tipo',
                                                                                branch='$versao',
										dat_ent_fab='$EntFab',
										dat_ini_hom='$InHom',
										dat_pla_exp='$PlaExp',
										data_exp='$Exp',
										observacao = '$obser'																
										WHERE  id_release = '$iddown'");
			@$gravaexepedlog = mysql_query("
                            INSERT INTO control_release_log (id_release,id_cliente,id_sistema,tipo,dat_ent_fab,dat_ini_hom,dat_pla_exp,data_exp,observacao
                                ");
                            
                        echo 'ok';
         
         
		*/				
			 echo '<script>alert("Editado Com Sucesso!");window.location="index.php?p=GerRel";</script>' ;
}
       }
    else {   echo '<script>alert("Preencha todos os campos!");</script>'; 
    
    
    } 
                        }
		
		
		
		?>
<?php }else {
     
// Destrói a sessão por segurança
	session_destroy();
	// Redireciona o visitante de volta pro login
	header("Location: index.php?p=Geren"); exit;

    
    
}?>



