<?php  
require_once 'dados/planejamento_release.php'; 
require_once 'dados/verificaStatusCrcExped.php'; 
require_once 'dados/verificaReleaseAvaliado.php'; 
?>

<head>
    <script src="js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript" src="./js/jquery.contextmenu.r2.js"></script>
    <link rel="stylesheet" href="css/rel_env.css">    
</head>

<!-- GRID DE CRCs CRITICAS -->
<div id="grid"  ng-app="myApp" ng-controller="indexCntrl""> <?php include 'crcCriticas.php'; ?> </div>

<div class="container demo"  width="100%">
    <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Hoje</th>
                <th>Produto</th>
                <th>Versão</th>
                <th>Release</th>
                <th>Tipo</th>
                <th>Entrega Fáb</th>
                <th>Início Homol</th>
                <th id='ordenacao-datatable'>Plan Expedi</th>
                <th>Observação</th>
                <th>Avaliado</th>
                <th>Alterações</th>
                <th>CRCs</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th class="enviado esconder"></th>
                <th class="produto">Produto</th>
                <th class="versao">Versão</th>
                <th class="release">Release</th>
                <th class="tipo">Tipo</th>
                <th class="entrega_fab">Entrega Fáb</th>
                <th class="inicio_hom">Início Homol</th>
                <th class="plan_expedicao">Plan Expedi</th>
                <th class="observacao esconder">Observação</th>
                <th class="esconder"></th>
                <th class="esconder"></th>
                <th class="esconder"></th>
            </tr>
        </tfoot>            
        <tbody>
<?php 

    $exec_sql = "SELECT 
                    control_release.id_release, 
                    control_clientes.nome_cliente, 
                    control_clientes.tag_cliente, 
                    control_sistema.nome_sistema, 
                    control_sistema.tag_sistema, 
                    control_release.branch, 
                    control_release.tipo, 
                    DATE_FORMAT(control_release.dat_ent_fab,'%d/%m/%Y') as dat_ent_fab, 
                    DATE_FORMAT(control_release.dat_ini_hom,'%d/%m/%Y') as dat_ini_hom, 
                    DATE_FORMAT(control_release.dat_pla_exp,'%d/%m/%Y') as dat_pla_exp, 
                    DATE_FORMAT(control_release.data_exp,'%d/%m/%Y') as data_exp, 
                    control_release.observacao, 
                    control_release.emergencial 
                FROM control_release,control_clientes,control_sistema 
                WHERE 
                    control_release.id_sistema      = control_sistema.id_sistema 
                    AND control_release.id_cliente  = control_clientes.id_cliente 
                    AND control_release.data_exp    = '0000-00-00' 
                ORDER BY data_exp DESC";

        $exec_sql = mysql_query($exec_sql);

        while ($fetch_sql = mysql_fetch_array($exec_sql)) {
            
            $entrega_hoje = ( ($fetch_sql['dat_ent_fab'] == date('Y-m-d h:m:s')) || ($fetch_sql['dat_ini_hom'] == date('Y-m-d h:m:s')) || ($fetch_sql['dat_pla_exp'] == date('Y-m-d h:m:s')) )? "HOJE": "";

            $countCrc = count_crc($connect,$fetch_sql['branch']);



            echo "  <tr>
                        <td>{$entrega_hoje}</td>
                        <td>{$fetch_sql['nome_sistema']} </td>
                        <td>{$fetch_sql['nome_cliente']}</td>
                        <td>{$fetch_sql['branch']}<span style=' margin-left:5px; width:10px; padding:2px 2px 2px 2px;background-color:#006697; color:#FFF; border-radius:2px;'><crc id='crc' title='{$countCrc} CRCs '>{$countCrc}</crc></span></td>
                        <td>{$fetch_sql['tipo']}</td>
                        <td>{$fetch_sql['dat_ent_fab']}</td>
                        <td>{$fetch_sql['dat_ini_hom']}</td>
                        <td>{$fetch_sql['dat_pla_exp']}</td>
                        <td>{$fetch_sql['observacao']}</td>
                        <td></td>
                        <td><a href='index.php?p=HistRel&V=Hist&id=".$fetch_sql['id_release']."&B=".base64_encode($fetch_sql['branch'])."'><img  src='image/log.png' style='width: 20px;' title='Alterações' /></a>
                        </td>
                        <td><a href='?p=crc&amp;i=".base64_encode($fetch_sql['id_release'])."}&amp;v=".base64_encode($fetch_sql['branch'])."'><img src='image/crcs.png' style='width: 20px;' title='CRCs' /></a></td>
                    </tr> 
                 ";    
        }
?>


        </tbody>        
    </table>
</div>


<script src="js/datatable.js"></script>
<script src="js/releases_abertos.js"></script>


<?php if (isset($_SESSION['UsuarioID']) && ($_SESSION['UsuarioNivel'] == 5) ||isset($_SESSION['UsuarioID']) && ($_SESSION['UsuarioNivel'] == 1) ) { require_once 'grid/gerencia_release_naoAvaliado.php'; }?>