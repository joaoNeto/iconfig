<?php require_once 'dados/acompanhaCliente.php'?>

<script>

    function mudaCliente(){
        var e = document.getElementById("selectCliente");
        var cod = e.options[e.selectedIndex].value;
        var nome = e.options[e.selectedIndex].text;
        
        location.href="?p=acompCliente&cod="+cod+"&nome="+nome;
       
       e.options[e.selectedIndex].value = 36;
        
    }

</script>
<style>
    
.caixa_corte {
width: 80px;
max-height: 20px;
overflow:hidden;
float:right;
margin-top: -20px;
}
 
.caixa_corte img {
width: 80px;
margin-top: -10px;
}
</style>
<div class="panel panel-default" >

    <div class="panel-heading" >
        <?php if(!empty($_GET['nome'])) echo 'Acompanhamento do Cliente <b>'.$_GET['nome'].'</b>'; else echo '<b>Escolha um Cliente </b>';?></b>

        <select id="selectCliente" style="float:right;" onchange="mudaCliente()">
            <option value="">Clientes</option>
            <option value="18">Carrefour</option>
            <option value="36">Formaggio</option>
            <option value="10">Informata</option>
            <option value="33">JMonte</option>
            <option value="5">Jorge Batista</option>
            <option value="38">Lojas JM</option>
            <option value="27">Negrão</option>
            <option value="8">Pererê</option>
            <option value="32">Potiguar</option>
            <option value="34">Sertão</option>
            <option value="29">São Bento</option>
            <option value="35">TerraZoo</option>
            <option value="25">Walmart</option>
            <option value="37">Zaipo</option>
        </select>

    </div>
    <div id="collapseFet" class="panel-collapse collapse in">
        <div class="table-responsive">
            <table class="table">    
                <thead>
                    <tr>

                        <th colspan="3" style="border-right: 1px solid #CCC; background-color:#E3E409;"><center>Negócio</center><div class="caixa_corte"><img src="image/seta_fluxo_old.png"/></div> </th>
                        
                        <th  colspan="3" style="border-right: 1px solid #CCC; background-color: #c3fdb8"><center>Fábrica</center> <div class="caixa_corte"><img src="image/seta_fluxo_old.png"/></div></th>
                        
            <th  colspan="3" style="background-color: #c7d7f3;"><center>Homologação</center></th>
                    </tr>
                 </thead>
                 <tr>
                 <th class="col-sm-1" style="background-color: #E3E409;">Aprov. Comercial.(<?= $qtdComercial_1; ?>)</th>
                                <th class="col-sm-1" style="background-color: #E3E409;">Ag. Prior.Esp.PO. (<?= $qtdComercial_2; ?>)</th>
                                <th class="col-sm-1" style="border-right: 1px solid #CCC; background-color: #E3E409;">Em Execução (<?= $qtdComercial_3; ?>)</th>
                                <th class="col-sm-1" style="background-color: #c3fdb8">Ag. FET (<?= $qtdFabrica_1; ?>)</th>
                                <th class="col-sm-1" style="background-color: #c3fdb8">Planejamento (<?= $qtdFabrica_2; ?>)</th>
                                <th class="col-sm-1" style="border-right: 1px solid #CCC; background-color: #c3fdb8">Em Execução (<?= $qtdFabrica_3; ?>)</th>
                                <th class="col-sm-1" style="background-color: #c7d7f3;">Em Planeja. Prep. (<?= $qtdHomologacao_1; ?>)</th>
                                <th class="col-sm-1" style="background-color: #c7d7f3;">Em Execução (<?= $qtdHomologacao_2; ?>)</th>
                                <th class="col-sm-1" style="background-color: #c7d7f3;">Exped. Prepara. (<?= $qtdHomologacao_3; ?>)</th>
                 </tr> 
                <tbody>
                
                <td style="border-right: 1px solid #ccc; background-color: #F7EDBE;">
                    <?php while (odbc_fetch_row($resultComercial_1)) {
                         $FetComercial_1 = substr(odbc_result($resultComercial_1, 'SMCCODE'),3,8);
                         $FetComercial_Desc_1 = utf8_encode(odbc_result($resultComercial_1, 'SUBJECT'));
                         echo '<span title=" Descrição da CRC '.$FetComercial_1
                                 .' - '.$FetComercial_Desc_1.'"><b style="color:black;">'.$FetComercial_1.'</b> <br> '.substr($FetComercial_Desc_1,0,26).'</span><br><br>';
                    }?>
                    
                </td>
                <td style="border-right: 1px solid #ccc; background-color: #F7EDBE;">
                    
                    <?php while (odbc_fetch_row($resultComercial_2)) {
                 
                            $FetComercial_2 = substr(odbc_result($resultComercial_2, 'SMCCODE'),3,8);
                            $FetComercial_Desc_2 = utf8_encode(odbc_result($resultComercial_2, 'SUBJECT'));
                            echo '<span title=" Descrição da CRC '.$FetComercial_2
                                 .' - '.$FetComercial_Desc_2.'"><b style="color:black;">'.$FetComercial_2.'</b> <br> '.substr($FetComercial_Desc_2,0,26).'</span><br><br>';
                    }?> 
                    
                </td>
                <td style="border-right: 1px solid #ccc; background-color: #F7EDBE;">
                    
                    <?php while (odbc_fetch_row($resultComercial_3)) {
                 
                            $FetComercial_3 = substr(odbc_result($resultComercial_3, 'SMCCODE'),3,8);
                            $FetComercial_Desc_3 = utf8_encode(odbc_result($resultComercial_3, 'SUBJECT'));
                            echo '<span title=" Descrição da CRC '.$FetComercial_3
                                 .' - '.$FetComercial_Desc_3.'"><b style="color:black;">'.$FetComercial_3.'</b> <br> '.substr($FetComercial_Desc_3,0,26).'</span><br><br>';
                    }?> 
                    
                    
                </td>
                <td style="border-right: 1px solid #ccc; background-color: #e5ffe9">
                    
                    <?php while (odbc_fetch_row($resultFabrica_1)) {
                 
                            $FetFabrica_1 = substr(odbc_result($resultFabrica_1, 'SMCCODE'),3,8);                        
                            $FetFabrica_Desc_1 = utf8_encode(odbc_result($resultFabrica_1, 'SUBJECT'));
                            echo '<span title=" Descrição da CRC '.$FetFabrica_1
                                 .' - '.$FetFabrica_Desc_1.'"><b style="color:black;">'.$FetFabrica_1.'</b> <br> '.substr($FetFabrica_Desc_1,0,26).'</span><br><br>';
                    }?> 
                    
                    
                </td>
                <td style="border-right: 1px solid #ccc; background-color: #e5ffe9">
                    
                    <?php while (odbc_fetch_row($resultFabrica_2)) {
                 
                            $FetFabrica_2 = substr(odbc_result($resultFabrica_2, 'SMCCODE'),3,8);                        
                            $FetFabrica_Desc_2 = utf8_encode(odbc_result($resultFabrica_2, 'SUBJECT'));
                            echo '<span title=" Descrição da CRC '.$FetFabrica_2
                                 .' - '.$FetFabrica_Desc_2.'"><b style="color:black;">'.$FetFabrica_2.'</b> <br> '.substr($FetFabrica_Desc_2,0,26).'</span><br><br>';
                    }?> 
                    
                </td>
                <td style="border-right: 1px solid #ccc; background-color: #e5ffe9">
                    
                    <?php while (odbc_fetch_row($resultFabrica_3)) {
                 
                            $FetFabrica_3 = substr(odbc_result($resultFabrica_3, 'SMCCODE'),3,8);                        
                            $FetFabrica_Desc_3 = utf8_encode(odbc_result($resultFabrica_3, 'SUBJECT'));
                            echo '<span title=" Descrição da CRC '.$FetFabrica_3
                                 .' - '.$FetFabrica_Desc_3.'"><b style="color:black;">'.$FetFabrica_3.'</b> <br> '.substr($FetFabrica_Desc_3,0,26).'</span><br><br>';
                    }?> 
                    
                </td>
                <td style="border-right: 1px solid #ccc; background-color: #e9ecf9;">
                    
                    <?php while (odbc_fetch_row($resultHomologacao_1)) {
                 
                            $FetHomologacao_1 = substr(odbc_result($resultHomologacao_1, 'SMCCODE'),3,8);                        
                            $FetHomologacao_Desc_1 = utf8_encode(odbc_result($resultHomologacao_1, 'SUBJECT'));
                            echo '<span title=" Descrição da CRC '.$FetHomologacao_1
                                 .' - '.$FetHomologacao_Desc_1.'"><b style="color:black;">'.$FetHomologacao_1.'</b> <br> '.substr($FetHomologacao_Desc_1,0,26).'</span><br><br>';
                    }?> 
                    
                </td>
                <td style="border-right: 1px solid #ccc; background-color: #e9ecf9;">
                    
                    <?php while (odbc_fetch_row($resultHomologacao_2)) {
                 
                            $FetHomologacao_2 = substr(odbc_result($resultHomologacao_2, 'SMCCODE'),3,8);                        
                            $FetHomologacao_Desc_2 = utf8_encode(odbc_result($resultHomologacao_2, 'SUBJECT'));
                            echo '<span title=" Descrição da CRC '.$FetHomologacao_2
                                 .' - '.$FetHomologacao_Desc_2.'"><b style="color:black;">'.$FetHomologacao_2.'</b> <br> '.substr($FetHomologacao_Desc_2,0,26).'</span><br><br>';
                    }?> 
                    
                </td>
                <td style="border-right: 1px solid #ccc; background-color: #e9ecf9;">
                    
                    <?php while (odbc_fetch_row($resultHomologacao_3)) {
                 
                            $FetHomologacao_3 = substr(odbc_result($resultHomologacao_3, 'SMCCODE'),3,8);                        
                            $FetHomologacao_Desc_3 = utf8_encode(odbc_result($resultHomologacao_3, 'SUBJECT'));
                            echo '<span title=" Descrição da CRC '.$FetHomologacao_3
                                 .' - '.$FetHomologacao_Desc_3.'"><b style="color:black;">'.$FetHomologacao_3.'</b> <br> '.substr($FetHomologacao_Desc_3,0,26).'</span><br><br>';
                    }?> 
                    
                </td>
                </tbody>
               
            </table>
        </div>
    </div>
</div>
