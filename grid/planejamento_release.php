<?php  require_once 'dados/planejamento_release.php'; 
require_once 'dados/verificaStatusCrcExped.php'; 
require_once 'dados/verificaReleaseAvaliado.php'; ?>

<head>

    <script src="js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript" src="./js/jquery.contextmenu.r2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script type="text/javascript" src="js/jquery_mask/src/jquery.mask.js"></script>


</head>
<script language="JavaScript" type="text/javascript">
    function mudaSetaPlan(){
        classe = document.getElementById('Plan').className; 
        if(classe == 'baixo'){
         document.getElementById('Plan').className = 'cima';
         document.getElementById('paginacao').style.display = 'block';
     }else{
         document.getElementById('Plan').className = 'baixo';
         document.getElementById('paginacao').style.display = 'none';
     }
 }

 var AppAngular = angular.module('myApp', []);

 AppAngular.controller('indexCntrl', function($scope,$http) {

    $http.get('dados/listaDadosJson.php?param=listaSistemasReleasesAbertos')
    .then(function (data) {
        $scope.listaSistemasReleasesAbertos = data.data;
        console.log(data.data);
    });

    $http.get('dados/listaDadosJson.php?param=listaClientesReleasesAbertos')
    .then(function (data) {
        $scope.listaClientesReleasesAbertos = data.data;
        console.log(data.data);
    });

    $scope.filtrarSistema = function(){

        $http.get('dados/listaDadosJson.php?param=listaSistemasReleasesAbertos&filtro='+$('#SearchCliente').val())
        .then(function (data) {
            $scope.listaSistemasReleasesAbertos = data.data;
            console.log(data.data);
        });        
    };

    $scope.filtrarCliente = function(){
        
        console.log($('#SearchCliente').val());
        if ($('#SearchCliente').val() == '' || $('#SearchCliente').val() == undefined) {        
            $http.get('dados/listaDadosJson.php?param=listaClientesReleasesAbertos&filtro='+$('#SearchSistema').val())
            .then(function (data) {
                $scope.listaClientesReleasesAbertos = data.data;
                console.log(data.data);
            });        
        }
    };    

    $scope.limparFiltro = function(){
        
        $http.get('dados/listaDadosJson.php?param=listaSistemasReleasesAbertos')
        .then(function (data) {
            $scope.listaSistemasReleasesAbertos = data.data;
            console.log(data.data);
        });

        $http.get('dados/listaDadosJson.php?param=listaClientesReleasesAbertos')
        .then(function (data) {
            $scope.listaClientesReleasesAbertos = data.data;
            console.log(data.data);
        });
        
        $scope.atb_cliente = '';
        $scope.atb_sistema = '';

    };

});
 
</script>

<div id="grid"  ng-app="myApp" ng-controller="indexCntrl"">

    <?php include 'crcCriticas.php'; ?>
    <?php// include 'releasesEmergenciais.php'; ?>
    
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading" style="background-color: #D8DAE1;">
            <a data-toggle="collapse" data-parent="#accordion" href="#PlanjeamentoRel" style="color: #76649F; text-decoration:none;" onclick="mudaSetaPlan();">
                <p style="margin-top: -9px; margin-bottom: -8px;">
                    <img src="image/planejamento.png" width="30px;"> Planejamento de Releases
                </p>
                <span class="cima" id="Plan"></span></a>    
            </div>
            <!-- Table -->
            <div id="PlanjeamentoRel" class="panel-collapse collapse in">
                <div class="table-responsive">
                    <table class="table"  >

                        <thead>
                            <form  action="" name="Search" method="post">
                                <tr><th></th>
                                    <th>Produto:<br>
                                        <select  name="SearchSistema" id="SearchSistema" onchange="setFilter();getDados();" ng-model="atb_sistema" ng-change="filtrarCliente()" style="width: 60px;" >

                                            <option value=""></option>
                                            <option value="{{atb_sistema.tag_sistema}}" ng-repeat="atb_sistema in listaSistemasReleasesAbertos">{{atb_sistema.tag_sistema}} - {{atb_sistema.nome_sistema}}</option>

                                        </select>

                                    </th>                                
                                    <th>Versão:<br>  
                                        <select name="SearchCliente" id="SearchCliente" onchange="setFilter();getDados();"  ng-model="atb_cliente" ng-change="filtrarSistema()" style="width: 50px;">

                                            <option value=""></option>
                                            <option value="{{atb_cliente.tag_cliente}}" ng-repeat="atb_cliente in listaClientesReleasesAbertos">{{atb_cliente.tag_cliente}} - {{atb_cliente.nome_cliente}}</option>

                                        </select>     
                                    </th>
                                    <th>Release:<br>
                                        <input type="text" id="branch2" name="SearchVersao" value="<?php echo @$SearchVersao; ?>" class="input-medium search-query" style="height: 20px; width: 90px;">
                                    </th>
                                    <th>Tipo<br>
                                        <input type="text" name="SearchSacpro" value="<?php echo @$SearchSacpro; ?>" class="input-medium search-query" style="height: 20px; width: 30px;">
                                    </th>
                                    <th>Entrega Fábrica<br>
                                        <input type="text" id="dataEntFab" name="SearchEntFab" value="<?php echo @$SearchEntFab; ?>" class="input-medium search-query" style=" height: 20px; width: 70px;">

                                    </th>
                                    <th>Início Homologação<br>
                                        <input type="text" id="dataInHom" name="SearchIniHom" value="<?php echo @$SearchIniHom; ?>" class="input-medium search-query" style=" height: 20px; width: 70px;">
                                    </th>
                                    <th>Plan. Expedição<br>
                                        <input type="text" id="dataPlaExp" name="SearchPlaExp" value="<?php echo @$SearchPlaExp; ?>" class="input-medium search-query" style=" height: 20px; width: 70px;">
                                    </th>
                                    <th style="width:40%;  ">Observação
                                        <a href="?p=Plan"><input type="button"  id="limpa_normal" ng-click="limparFiltro()" name="limpa_normal" value="Limpar"></a>
                                        <input type="submit" id="buscar" name="buscar" value="Buscar">
                                    </th>
                                    <th></th>
                                    <th></th>


                                </tr>
                            </form>   

                            <script type="text/javascript">
                              jQuery("#dataEntFab").mask("99/99/9999");
                              jQuery("#dataInHom").mask("99/99/9999");
                              jQuery("#dataPlaExp").mask("99/99/9999");
                              jQuery("#dataExp").mask("99/99/9999");
                              jQuery("#campoData2").mask("99/99/9999");
                              jQuery("#campoTelefone").mask("(999) 999-9999");
                              jQuery("#branch2").mask("99.99.99.99");   
                          </script>


                          <center>
                            <?php

                            echo '<tbody>';
                            $i = 0;
                            $qtd = mysql_num_rows($exec_sql);
                            $demo = 1;
                            $menu = 1;
                            while ($fetch_sql = mysql_fetch_array($exec_sql)) {

                                if ($fetch_sql == false) {

                                    echo 'nenhum dado encontrado';
                                } else
            #carrega todos os dados
                                $codigo = $fetch_sql['id_release'];
                                $nome_cliente = $fetch_sql['nome_cliente'];
                                $cliente = $fetch_sql['tag_cliente'];

                                $sistema = $fetch_sql['tag_sistema'];
                                $versao = $fetch_sql['branch'];
                                $OKversao = verificaStatusCrcQuali($versao);
                                $Avaliado = verificaReleaseAvaliado($codigo,0);
                                if($OKversao == false && $Avaliado == 0){
                                    $sacpro = $fetch_sql['tipo'];
                                    $entregFab = $fetch_sql['dat_ent_fab'];
                                    $VentregFab = $entregFab;
                                    $verificaEntFab = verifica_historico($codigo, 'dat_ent_fab', $VentregFab);            
                                    if($verificaEntFab == true){@$bandEntFab = '<a href="index.php?p=HistRel&V=Hist&id='.$codigo.'&B='.base64_encode($versao).'"><img name="bandeira" src="image/bandred.png" title="Vizualizar Histórico" style="width:10%" ></a>';
                                }
                                else{@$bandEntFab ='';}
                                $inicioHom = $fetch_sql['dat_ini_hom'];
                                $VinicioHom =$inicioHom;
                                $verificaIniHom = verifica_historico($codigo, 'dat_ini_hom', $VinicioHom);
                                if($verificaIniHom == true){@$bandIniHom = '<a href="index.php?p=HistRel&V=Hist&id='.$codigo.'&B='.base64_encode($versao).'"><img name="bandeira" src="image/bandred.png" title="Vizualizar Histórico" style="width:8.5%" ></a>';}
                                else{@$bandIniHom = '';}
                                $planejExp = $fetch_sql['dat_pla_exp'];
                                $VplanejExp = $planejExp;
                                $planejExp = verifica_historico($codigo, 'dat_pla_exp', $VplanejExp);
                                if($planejExp == true){@$bandPlaExp = '<a href="index.php?p=HistRel&V=Hist&id='.$codigo.'&B='.base64_encode($versao).'"><img name="bandeira" src="image/bandred.png" title="Vizualizar Histórico" style="width:10%" ></a>';}
                                else{@$bandPlaExp ='';}
                                $Expedicao = $fetch_sql['data_exp'];
                                $observacao = $fetch_sql['observacao'];

                                $planejExp = $fetch_sql['dat_pla_exp'];

                                $entregFab = date('d/m/Y',  strtotime($entregFab));
                                $inicioHom = date('d/m/Y', strtotime($inicioHom));



                                $countCrc = count_crc($connect,$versao);


                                if($planejExp == '0000-00-00 00:00:00'){
                                    $planejExp  = '<img src="image/interrog.png" width="15%" style="margin-left:25%;">';
                                }else{
                                    $planejExp = date('d/m/Y',strtotime($planejExp));
                                }

                                if ($Expedicao == '0000-00-00 00:00:00'){

                                    $Expedicao = 'Não definido';
                                }else{
                                    $Expedicao = date('d/m/Y',strtotime($Expedicao));
                                }

                                $classDemo = 'demo'.$demo;
                                $classMenu = 'myMenu'.$menu;
                                echo "<script type='text/javascript'>
                                $(document).ready(function() {

                                  $('span.".$classDemo."').contextMenu('". $classMenu."', {
                                    bindings: {

                                    }
                                });

                            });
                            $(document).ready(function() {

                              $('tr.".$classDemo."').contextMenu('". $classMenu."', {
                                bindings: {

                                }
                            });

                        });    

                    </script>  "; 



                    $versao; 
          //$versao = '<span class="demo1"> ' . $versao . '</span>';


                    if ($planejExp == date('d/m/Y')
                     ||$entregFab == date('d/m/Y')
                     ||$inicioHom == date('d/m/Y')
                     ||$Expedicao == date('d/m/Y')
                     ){

                     @$info = '<hoje><img src="image/hoje.png" title="Hoje"></hoje>';

             }else{@$info = '';}

             $logs2 = '<a href="index.php?p=HistRel&V=Hist&id='.$codigo.'&B='.base64_encode($versao).'"><img  src="image/log.png" style="width: 20px;" title="Alterações" /></a>';  
             $visualizar2 = '<a href="?p=crc&amp;i=' . base64_encode($codigo) . '&amp;v=' . base64_encode($versao) . '"><img src="image/crcs.png" style="width: 20px;" title="CRCs" /></a>';
             if (isset($_SESSION['UsuarioID']) && ($_SESSION['UsuarioNivel'] == 5) ||isset($_SESSION['UsuarioID']) && ($_SESSION['UsuarioNivel'] == 1) ) {
                require_once 'dados/alocacao.php';
                $setor = getSetorTimeUsuario($_SESSION['UsuarioID']);
                if($setor['id_setor'] == 2 || ($_SESSION['UsuarioNivel'] == 1)){
                    if($OKversao){
                        if($Avaliado){
                                //$qualidade = '<a href="#"><img  src="image/cheklistQualidade.png" style="width: 20px;" title="Release Já Avaliado '.getPercQualiRel($codigo).'" /></a>';  
                            $qualidade =getPercQualiRel($codigo);
                        }else{
                            if($setor['id_setor'] == 2){
                                $qualidade = '<a href="index.php?p=QualiRel&id='.$codigo.'&B='.base64_encode($versao).'"><img  src="image/cheklistQualidade.png" style="width: 20px;" title="Avaliar a qualidade deste Release" /></a>';  
                            }else{
                                $qualidade = '<a href="#"><img  src="image/cheklistQualidade.png" style="width: 20px;" title="Release falta ser Avaliado" /></a>';      
                            }
                        }
                        
                    }else{
                        $qualidade = null;
                    }
                }else{
                    $qualidade = '';
                }

            }else{
                $qualidade = '';
            }

            if(($i%2)==0){

             $tr = '<tr style="background-color:#EEE;" class="'.$classDemo.'">';
             $td = '<td style="color:#000;">';
         }else{
             $tr = '<tr class="'.$classDemo.'">';
             $td = '<td>';
         }$i++;

         echo $tr;
         echo $td;
         echo @$info;
         echo '</td>';
         echo $td;
         echo $sistema;
         echo '</td>';         
         echo $td;
         echo $cliente;
         echo '</td>';
         echo $td;
         echo $versao;echo '<span style=" margin-left:5px; width:10px; padding:2px 2px 2px 2px;background-color:#006697; color:#FFF; border-radius:2px;"><crc id="crc" title="'.$countCrc.' CRCs ">'.$countCrc.'</crc></span>';
         echo '</td>';
         echo $td;
         echo $sacpro;
         echo '</td>';
         echo $td;
         echo $entregFab;
         echo '</td>';
         echo $td;
         echo $inicioHom;
         echo '</td>';
         echo $td;
         echo $planejExp;
         echo '</td>';
         echo $td;
         echo wordwrap($observacao,100, "<br />\n");
         echo '</td>';
         echo $td;
         echo $visualizar2; 
         echo '</td>';
         echo $td;
         echo $logs2;
         echo '</td>';
            #echo $td;
            #echo $qualidade;
            #echo '</td>';
         echo '</tr>';
         echo '</tbody>';


         $logs = '<a href="index.php?p=HistRel&V=Hist&id='.$codigo.'&B='.base64_encode($versao).'"><li id="log"><span style="font-size: 10px;" onclick=""><img src="image/log.png" style="width: 20%;" /><span class="demo1"> Alterações </span></span></li></a>';  
         $visualizar = '<a href="?p=crc&amp;i=' . base64_encode($codigo) . '&amp;v=' . base64_encode($versao) . '"><li id="crc"><span style="font-size: 10px;" onclick=""><img src="image/crcs.png" style="width: 20%;" /><span class="demo1"> CRCs </span></span></li></a>';

         $demo ++;
         $menu ++;
         echo '<div class="contextMenu" id="'.$classMenu.'">
         <ul>
            '.$visualizar.'
            '.$logs.'
        </ul>
    </div>';

}

}
?>
</thead>     
</table>
</div>
</div>

</div>
<style>
    .contextMenu{display: none;}

</style>

<div class="paginacao" id="paginacao" style="margin-top:-10;">
	<?php
	
	$totalPosts = mysql_num_rows($execSqlPag);
	$pags = ceil($totalPosts/$maximo);
	$links =  5;
	echo"<a href=\"?p=Plan&pag=1\">Primeira Página</a>";	
	for($i = $pg - $links; $i <= $pg -1; $i++){
		if($i <= 0){}else{
			echo "<a href=\"?p=Plan&pag=$i\">$i</a>";
     }
 }
		//echo $pg;
 echo "<div id class=\"pgSel\">".$pg."</div>";
 for($i = $pg + 1; $i <= $pg + $links; $i ++){
     if($i > $pags){}else{
        echo"<a href=\"?p=Plan&pag=$i\">$i</a>";

    }

}
echo"<a href=\"?p=Plan&pag=$pags\">Ultima Página</a>";	


?>
</div>

</div>
<br>
<?php 

if (isset($_SESSION['UsuarioID']) && ($_SESSION['UsuarioNivel'] == 5) ||isset($_SESSION['UsuarioID']) && ($_SESSION['UsuarioNivel'] == 1) ) {
    require_once 'grid/gerencia_release_naoAvaliado.php'; 

}?>