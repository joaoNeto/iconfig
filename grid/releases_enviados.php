<?php

$sqlCliBox = "SELECT tag_cliente,nome_cliente FROM control_clientes WHERE tag_cliente <> '' order by 2";
$sqlSisBox = "SELECT tag_sistema,nome_sistema FROM `control_sistema` order by 2";
$sqlTipBox = "SELECT distinct(tipo) as tipo FROM control_release";
$execSqlCliBox = mysql_query($sqlCliBox,$conn);
$execSqlSisBox = mysql_query($sqlSisBox,$conn);
$execSqlTipBox = mysql_query($sqlTipBox,$conn);


?>
<head>
   
    <script type="text/javascript" src="js/ajax.js"></script> 
    <script type="text/javascript" src="js/jquery.contextmenu.r2.js"></script>
    <script type="text/javascript" src="js/jquery_mask/src/jquery.mask.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>


    <script>
     $( window ).load(function() {
  // Run code
  getDados();
});


     var AppAngular = angular.module('myApp', []);

     AppAngular.controller('indexCntrl', function($scope,$http) {

        $http.get('dados/listaDadosJson.php?param=listaSistemas')
        .then(function (data) {
            $scope.listaSistemas = data.data;
            console.log(data.data);
        });

        $http.get('dados/listaDadosJson.php?param=listaClientes')
        .then(function (data) {
            $scope.listaClientes = data.data;
            console.log(data.data);
        });

        $scope.filtrarSistema = function(){
            $http.get('dados/listaDadosJson.php?param=listaSistemas&filtro='+$('#SearchCliente').val())
            .then(function (data) {
                $scope.listaSistemas = data.data;
                console.log(data.data);
            });        
        };

        $scope.filtrarCliente = function(){
            console.log($('#SearchCliente').val());
            if ($('#SearchCliente').val() == '' || $('#SearchCliente').val() == undefined) {        
                $http.get('dados/listaDadosJson.php?param=listaClientes&filtro='+$('#SearchSistema').val())
                .then(function (data) {
                    $scope.listaClientes = data.data;
                    console.log(data.data);
                });        
            }
        };    

        $scope.limparFiltro = function(){

            $http.get('dados/listaDadosJson.php?param=listaSistemas')
            .then(function (data) {
                $scope.listaSistemas = data.data;
                console.log(data.data);
            });

            $http.get('dados/listaDadosJson.php?param=listaClientes')
            .then(function (data) {
                $scope.listaClientes = data.data;
                console.log(data.data);
            });
            
            $scope.atb_cliente = '';
            $scope.atb_sistema = '';

        };

    });

     $(document).ready(function(){
      $('#SearchVersao').mask('00.00.00.00');
      $('#SearchEntFab').mask('00/00/0000');
      $('#SearchIniHom').mask('00/00/0000');
      $('#SearchPlaExp').mask('00/00/0000');
      $('#SearchExped').mask('00/00/0000');
  });

</script>



</head>

<?php if(isset($_GET['pagi'])){
    $pag = $_GET['pagi'];
}else { $pag = 1;}
?>

<div id="grid" ng-app="myApp" ng-controller="indexCntrl">
    <div class="panel panel-default">
        <div class="panel-heading" style=" background-color: #D8DAE1;"><p style="margin-top: -9px; margin-bottom: -8px;">Releases Enviados</p></div>
        <div class="table-responsive">
            <table class="table" id="tabela"  >
                <thead>                         
                 <tr>
                    <th width="50px"> </th><input type="hidden"  onLoad="getDados();" name="p" id="p" value="Env"/>
                    <input type="hidden"  onLoad="getDados(); " name="pagina" id="pagina"  value="<?php echo @$pag;?>"/>
                    <th>Produto:<br>
                        <select  name="SearchSistema" id="SearchSistema" onchange="setFilter();getDados();" ng-model="atb_sistema" ng-change="filtrarCliente()" style="width: 60px;" >
                            
                            <option value=""></option>
                            <option value="{{atb_sistema.tag_sistema}}" ng-repeat="atb_sistema in listaSistemas">{{atb_sistema.tag_sistema}} - {{atb_sistema.nome_sistema}}</option>

                        </select>

                    </th>
                    <th>Versão:<br>  
                        <select name="SearchCliente" id="SearchCliente" onchange="setFilter();getDados();"  ng-model="atb_cliente" ng-change="filtrarSistema()" style="width: 50px;">
                            
                            <option value=""></option>
                            <option value="{{atb_cliente.tag_cliente}}" ng-repeat="atb_cliente in listaClientes">{{atb_cliente.tag_cliente}} - {{atb_cliente.nome_cliente}}</option>

                        </select>     
                    </th>
                    <th>Release:<br>
                        
                        <input type="text" onKeyUp="setFilter();getDados();" name="SearchVersao" id="SearchVersao" style="height: 20px; width: 90px;" value=""/>
                    </th>
                    <th>Tipo:<br>              
                        <select  name="SearchSacpro" id="SearchSacpro" onchange="setFilter();getDados();" style="width: 55px;">
                            <option value=""></option>
                            <?php while($fetchTip = mysql_fetch_array($execSqlTipBox)){
                                echo '<option value="'.$fetchTip['tipo'].'">'.$fetchTip['tipo'].'</option>';
                            }?>
                        </select>
                    </th>
                    <th>Entrega Fáb:<br>
                        <input type="text" onchange="setFilter();" onKeyUp="getDados();" name="SearchEntFab"  id="SearchEntFab" style="height: 20px; width: 70px;" value=""/>
                    </th>
                    <th>Início Homol:<br> 
                        <input type="text" onchange="setFilter();" onKeyUp="getDados();" name="SearchIniHom" id="SearchIniHom" style="height: 20px; width: 70px;" value=""/>
                    </th>
                    <th>Plan Expedi:<br>
                        <input type="text" onchange="setFilter();" onKeyUp="getDados();" name="SearchPlaExp" id="SearchPlaExp" style="height: 20px; width: 70px;" value=""/>
                    </th>
                    <th>Expedição:<br>
                        <input type="text" onchange="setFilter();" onKeyUp="getDados();" name="SearchExped" id="SearchExped"   style="height: 20px; width: 70px;" value=""/>
                    </th>
                    <th style="width:35%;">Observação<br>
                        <input type="button" onclick="limpaFiltro();" ng-click="limparFiltro()" name="limpa" value="Limpar Filtro">
                    </th>
                    <th></th>
                    <th></th>   

                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

<div id="Resultado">
    
</div>



<script>
    
    
    function limpaFiltro(){
        
       document.getElementById("SearchCliente").value = ''; 
       document.getElementById("SearchSistema").value = '';
       document.getElementById("SearchVersao").value = '';
       document.getElementById("SearchSacpro").value = '';
       document.getElementById("SearchEntFab").value = '';
       document.getElementById("SearchIniHom").value = '';
       document.getElementById("SearchPlaExp").value = '';
       document.getElementById("SearchExped").value = '';

       
       getDados();
   }    
   
</script>
<script>
    
    
    function setFilter(){
        
       document.getElementById("pagina").value = 1; 

   }    
   
</script>   



<!-- the result of the search will be rendered inside this div -->



</div>