<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
<?php 
require_once'dados/crcCriticas.php';


$cabecalho = '<center><p style="margin-top: -9px; margin-bottom: -8px;">FETs Pendentes</p></center>';

?>

<script language="JavaScript" type="text/javascript">
function mudaSetaFet(){
    classe = document.getElementById('cbFet').className; 
    if(classe == 'baixo'){
       document.getElementById('cbFet').className = 'cima';
   }else{
       document.getElementById('cbFet').className = 'baixo';
   }
}
</script>



<div class="panel panel-default" >
    
    <div class="panel-heading" >
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFet" style="color: #0E4571; text-decoration:none;" onclick="mudaSetaFet();"> <?php echo $cabecalho;?><span class="cima" id="cbFet"></span></a>
    </div>
    <div id="collapseFet" class="panel-collapse collapse in">
        <div class="table-responsive">
            <?php 
            if($verificaFetPend != FALSE){
             ?>
            <table class="table">    
            <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Produto</th>
                    <th>CRC</th>
                    <th>Responsável</th>
                    <th style="width: 50%;">Descrição</th>
                    <th>Dias</th>
                </tr>
            <tbody >
            <?php

              function quant_dias($data1, $data2){ 
              return round((strtotime($data2) - 
                 strtotime($data1)) / (24 * 60 * 60), 0); 
                } 
            
            
            
            $i = 0;
            
             while (odbc_fetch_row($resultFetPend)) {
                 
                $FetPendCliente = odbc_result($resultFetPend, 'CustomerName');
                $FetPendProduto = odbc_result($resultFetPend, 'projDescription');
                $FetPendCrc = odbc_result($resultFetPend, 'SMCCode');
                $FetPendCrc = substr($FetPendCrc, 3, 8);
                $FetPendDesc = odbc_result($resultFetPend, 'Subject');
                $FetPendSub =  odbc_result($resultFetPend, 'DtSubmissao');
                $TimeFetPendSub = strtotime($FetPendSub);
                $TimeFetPendSub2 = strtotime($FetPendSub);
                $FetPenDate = date('d/m/Y', $TimeFetPendSub2);
                $FetPendSub = date('m/d/Y', $TimeFetPendSub);
                $inicio = date("m/d/Y"); // data atual 
                $fim = $FetPendSub; // data no formato mês/dia/ano 
                $dias = quant_dias($fim,$inicio);
                $FetPendResp = getResponsavel($FetPendCrc, $connect);
                $FetPendSitucao = odbc_result($resultFetPend, 'sitDesc');
                $FetPendEstado = odbc_result($resultFetPend, 'estadoDesc');
                
                if(($i%2)==0){
			
			$tr = '<tr style="background-color:#EEE;">';
			$td = '<td style="color:#000;">';
			}else{
			$tr = '<tr> ';
			$td = '<td>';
			}$i++;
                
            echo $tr;
            echo $td;
            echo '<span >'.utf8_encode($FetPendCliente).'</span>';
            echo '</td>';
            echo $td;
            echo '<span >'.utf8_encode($FetPendProduto).'</span>';
            echo '</td>';
            echo $td;
            echo '<span >'.$FetPendCrc.'</span>';
            echo '</td>';
            echo $td;
            echo '<span >'.utf8_encode($FetPendResp).'</span>';
            echo '</td>';
            echo $td;
            echo '<span >'.utf8_encode($FetPendDesc).'</span>';
            echo '</td>';
            echo $td;
            echo '<span >'.utf8_encode($dias).'</span>';
            echo '</td>';
            echo '</tr>';
            echo '</tbody>';
                
                
             }
            
            ?>
            </thead>
            <?php
            }            
            ?>
        </table>

    
          </div>
    </div>
</div>

    </div>
  </div>
</div>