<?php //require 'dados/crcCriticas.php' ;


if(isset($verificaCritica) && $verificaCritica != FALSE){
    $c = odbc_num_rows($resultCriticas);
$cabecalho = '<p style="margin-top: -9px; margin-bottom: -8px; margin-left: 5px;"><img src="image/cartaoVermelho.png" style="width: 15px;"> '.$c .' Problemas Críticos</p>';
$borderColor = 'border-color:;' ;
$headingStyle = 'background-color: #D8DAE1; color: #B22222;';
$a = 'color: #B22222; text-decoration:none;';
}else if(!isset($verificaCritica)) {
    
$cabecalho = '<p style="margin-top: -9px; margin-bottom: -8px; margin-left: 5px;"><img src="image/cartaoVermelho.png" style="width: 15px;"> Não possível estabelecer conexão com o OneStudio!</p>';
$borderColor = 'border-color:;' ;
$headingStyle = 'background-color: #D8DAE1; color: #B22222;';
$a = 'color: #B22222; text-decoration:none;';
}
else{
    
$cabecalho = '<p style="margin-top: -9px; margin-bottom: -8px; margin-left: 5px;"><img src="image/like.png" style="width: 25px; margin-bottom:3px;"><span style="">Não Existem Problemas Críticos</span></p>';    
$borderColor = 'border-color:' ;
$headingStyle = 'background-color: #D8DAE1;';
$a = 'color:#76649F; text-decoration:none;';
}
?>

<script language="JavaScript" type="text/javascript">
function mudaSeta(){
    classe = document.getElementById('cb').className; 
    if(classe == 'baixo'){
       document.getElementById('cb').className = 'cima';
   }else{
       document.getElementById('cb').className = 'baixo';
   }
}
</script>



<div class="panel panel-default" style="<?php echo $borderColor;?>">
    
    <div class="panel-heading" style="<?php echo $headingStyle;?>">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="<?php  echo $a; ?>" onclick="mudaSeta();"> <?php echo $cabecalho; ?><span class="baixo" id="cb"></span></a>
    </div>
    <div id="collapseOne" class="panel-collapse collapse outset">
      
       
      
        <div class="table-responsive">
            <?php 
            if(isset($verificaCritica) && $verificaCritica != FALSE){
             ?>
        <table class="table">    
            <thead>
                <tr>
                    <th>Cliente</th>
                    <th>CRC</th>
                    <th>Responsável</th>
                    <th style="width: 50%;">Descrição</th>
                    <th>Data</th>
                    <th>Situação</th>
                    <th>Status</th>
                </tr>
            <tbody >
            <?php
            $i = 0;
            
             while (odbc_fetch_row($resultCriticas)) {
                 
                $CriticoCliente = odbc_result($resultCriticas, 'CustomerName');
                $CriticoCrc = odbc_result($resultCriticas, 'SMCCode');
                $CriticoCrc = substr($CriticoCrc, 3, 8);
                $CriticoDesc = odbc_result($resultCriticas, 'Subject');
                $CriticoSub =  odbc_result($resultCriticas, 'DtSubmissao');
                $TimeCriticoSub = strtotime($CriticoSub);
                $CriticoSub = date('d/m/Y h:i:s', $TimeCriticoSub);
                $CriticoResp = getResponsavel($CriticoCrc, $connect);
                $CriticoSitucao = odbc_result($resultCriticas, 'sitDesc');
                $CriticoEstado = odbc_result($resultCriticas, 'estadoDesc');
                
                if(($i%2)==0){
			
			$tr = '<tr style="background-color:#EEE;">';
			$td = '<td style="color:#000;">';
			}else{
			$tr = '<tr> ';
			$td = '<td>';
			}$i++;
                
            echo $tr;
            echo $td;
            echo '<span style="color:#B22222">'.utf8_encode($CriticoCliente).'</span>';
            echo '</td>';
            echo $td;
            echo '<span style="color:#B22222">'.$CriticoCrc.'</span>';
            echo '</td>';
            echo $td;
            echo '<span style="color:#B22222">'.utf8_encode($CriticoResp).'</span>';
            echo '</td>';
            echo $td;
            echo '<span style="color:#B22222">'.utf8_encode($CriticoDesc).'</span>';
            echo '</td>';
            echo $td;
            echo '<span style="color:#B22222">'.utf8_encode($CriticoSub).'</span>';
            echo '</td>';
            echo $td;
            echo '<span style="color:#B22222">'.utf8_encode($CriticoSitucao).'</span>';
            echo '</td>';
            echo $td;
            echo '<span style="color:#B22222">'.utf8_encode($CriticoEstado).'</span>';
            echo '</td>';
            echo '</tr>';
            echo '</tbody>';
                
            $c++;     
             }
            
            ?>
            </thead>
            <?php
            }            
            ?>
        </table>

    
          </div>
    </div>
</div>
