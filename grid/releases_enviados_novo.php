<?php
include 'dados/funcoesGrid.php';
include 'dados/verificaReleaseAvaliado.php';

$sqlCliBox = "SELECT tag_cliente,nome_cliente FROM control_clientes WHERE tag_cliente <> '' order by 2";
$sqlSisBox = "SELECT tag_sistema,nome_sistema FROM `control_sistema` order by 2";
$sqlTipBox = "SELECT distinct(tipo) as tipo FROM control_release";
$execSqlCliBox = mysql_query($sqlCliBox,$conn);
$execSqlSisBox = mysql_query($sqlSisBox,$conn);
$execSqlTipBox = mysql_query($sqlTipBox,$conn);

$sqlReleasesEnviados = "SELECT 
                            control_release.id_release, 
                            control_clientes.nome_cliente, 
                            control_clientes.tag_cliente, 
                            control_sistema.nome_sistema, 
                            control_sistema.tag_sistema, 
                            control_release.branch, 
                            control_release.tipo, 
                            DATE_FORMAT(control_release.dat_ent_fab,'%d/%m/%Y') as dat_ent_fab, 
                            DATE_FORMAT(control_release.dat_ini_hom,'%d/%m/%Y') as dat_ini_hom, 
                            DATE_FORMAT(control_release.dat_pla_exp,'%d/%m/%Y') as dat_pla_exp, 
                            DATE_FORMAT(control_release.data_exp,'%d/%m/%Y')    as data_exp, 
                            control_release.observacao, 
                            control_release.emergencial 
                        FROM control_release,control_clientes,control_sistema 
                        WHERE 
                            control_release.id_sistema      = control_sistema.id_sistema 
                            AND control_release.id_cliente  = control_clientes.id_cliente 
                            AND control_release.data_exp    <> '0000-00-00' 
                        ORDER BY control_release.data_exp desc";

$listaRelEnv = mysql_query($sqlReleasesEnviados);

?>
<head>

    <script type="text/javascript" src="js/ajax.js"></script> 
    <script type="text/javascript" src="js/jquery.contextmenu.r2.js"></script>
    <!-- <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css'> -->
    <link rel="stylesheet" href="css/rel_env.css">
</head> 

<div class="container demo"  width="100%">
    <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Enviado</th>
                <th>Produto</th>
                <th>Versão</th>
                <th>Release</th>
                <th>Tipo</th>
                <th>Entrega Fáb</th>
                <th>Início Homol</th>
                <th>Plan Expedi</th>
                <th>Expedição</th>
                <th>Observação</th>
                <th>Avaliado</th>
                <th>Alterações</th>
                <th>CRCs</th>
                <!--  id='ordenacao-datatable' -->
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th class="enviado esconder">Enviado</th>
                <th class="produto">Produto</th>
                <th class="versao">Versão</th>
                <th class="release">Release</th>
                <th class="tipo">Tipo</th>
                <th class="entrega_fab">Entrega Fáb</th>
                <th class="inicio_hom">Início Homol</th>
                <th class="plan_expedicao">Plan Expedi</th>
                <th class="expedicao">Expedição</th>
                <th class="observacao esconder">Observação</th>
                <th class="esconder"></th>
                <th class="esconder"></th>
                <th class="esconder"></th>
            </tr>
        </tfoot>            
        <tbody>
            <?php 
            while($atb = mysql_fetch_array($listaRelEnv, MYSQL_ASSOC))
            {

                /* --- NAO AVALIADO --- */
                $OKversao  = verificaReleaseAvaliado($atb['id_release'],0);
                $nao_avaliado = ($OKversao != null)? $OKversao : null;

                /* --- CORES E STATUS DO RELEASE --- */
                if ($atb['data_exp'] >  $atb['dat_pla_exp']){
                    $cor = "red";
                    $title = "Enviado Após o Planejado!";
                }else if($atb['data_exp'] <  $atb['dat_pla_exp']){
                    $cor = "green";
                    $title = "Enviado Antes do Planejado!";
                }else if($atb['data_exp'] ==  $atb['dat_pla_exp']){
                    $cor = "green";
                    $title = "Enviado Conforme o Planejado!";
                }else if($atb['emergencial'] == "1"){
                    $cor = "red";
                    $title = "situação é emergencial";
                }

                echo "<tr>";
                echo "<td><div  id='emergencial' style='background-color:".$cor.";' title='".$title."'>$cor</div></td>";
                echo "<td>{$atb['nome_sistema']}</td>";
                echo "<td>{$atb['nome_cliente']}</td>";
                echo "<td>{$atb['branch']}";
                echo "<td>{$atb['tipo']}</td>";
                echo "<td>{$atb['dat_ent_fab']}</td>";
                echo "<td>{$atb['dat_ini_hom']}</td>";
                echo "<td>{$atb['dat_pla_exp']}</td>";
                echo "<td>{$atb['data_exp']}</td>";
                echo "<td>".utf8_encode($atb['observacao'])."</td>";
                echo "<td>{$nao_avaliado}</td>";
                echo "<td><a href='index.php?p=HistRel&V=Hist&id=".$atb['id_release']."&B=".base64_encode($atb['branch'])."' target='_blank'><img  src='image/log.png' style='width: 20px;' title='Alterações' /></a></td>";
                echo "<td><a href='?p=crc&amp;i=".base64_encode($atb['id_release'])."&amp;v=". base64_encode($atb['branch']) . "' target='_blank'><img src='image/crcs.png' style='width: 20px; padding-right:-30px;'' title='CRCs' /></a></td>";
                echo "</tr>";
            }
            ?>
        </tbody>        
    </table>
</div>

</div>


<script src="js/datatable.js"></script>
<script src="js/releases_enviados.js"></script>
