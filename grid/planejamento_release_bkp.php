<?php  require_once 'dados/planejamento_release.php'; 
require_once 'dados/verificaStatusCrcExped.php'; 
require_once 'dados/verificaReleaseAvaliado.php'; ?>

<head>

    <script src="js/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="js/ajax_release_aberto.js" type="text/javascript"></script>
    <script type="text/javascript" src="./js/jquery.contextmenu.r2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>


</head>
<script language="JavaScript" type="text/javascript">

   var AppAngular = angular.module('myApp', []);

   AppAngular.controller('indexCntrl', function($scope,$http) {

    $http.get('dados/listaDadosJson.php?param=listaSistemasReleasesAbertos')
    .then(function (data) {
        $scope.listaSistemasReleasesAbertos = data.data;
    });

    $http.get('dados/listaDadosJson.php?param=listaClientesReleasesAbertos')
    .then(function (data) {
        $scope.listaClientesReleasesAbertos = data.data;
    });

    $scope.filtrarSistema = function(){

        $http.get('dados/listaDadosJson.php?param=listaSistemasReleasesAbertos&filtro='+$('#SearchCliente').val())
        .then(function (data) {
            $scope.listaSistemasReleasesAbertos = data.data;
        });        
    };

    $scope.filtrarCliente = function(){

        console.log($('#SearchCliente').val());
        if ($('#SearchCliente').val() == '' || $('#SearchCliente').val() == undefined) {        
            $http.get('dados/listaDadosJson.php?param=listaClientesReleasesAbertos&filtro='+$('#SearchSistema').val())
            .then(function (data) {
                $scope.listaClientesReleasesAbertos = data.data;
            });        
        }
    };    

    $scope.limparFiltro = function(){

        $http.get('dados/listaDadosJson.php?param=listaSistemasReleasesAbertos')
        .then(function (data) {
            $scope.listaSistemasReleasesAbertos = data.data;
        });

        $http.get('dados/listaDadosJson.php?param=listaClientesReleasesAbertos')
        .then(function (data) {
            $scope.listaClientesReleasesAbertos = data.data;
        });
        
        $scope.atb_cliente = '';
        $scope.atb_sistema = '';

    };

    /* --------------------------- LISTA TABELA RELEASES ABERTOS --------------------------- */
    $http.get('dados/listaDadosJson.php?param=exibirReleaseAberto')
    .then(function (data) {
        $scope.listaTabelaReleasesAbertos = data.data;
        console.log(data.data);
    });

});


</script>

<div id="grid"  ng-app="myApp" ng-controller="indexCntrl"">

    <?php include 'crcCriticas.php'; ?>
    <?php// include 'releasesEmergenciais.php'; ?>
    
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading" style="background-color: #D8DAE1;">
            <a data-toggle="collapse" data-parent="#accordion" href="#PlanjeamentoRel" style="color: #76649F; text-decoration:none;" onclick="mudaSetaPlan();">
                <p style="margin-top: -9px; margin-bottom: -8px;">
                    <img src="image/planejamento.png" width="30px;"> Planejamento de Releases
                </p>
                <span class="cima" id="Plan"></span></a>    
            </div>
            <!-- Table -->
            <div id="PlanjeamentoRel" class="panel-collapse collapse in">
                <div class="table-responsive">
                    <table class="table"  >

                        <thead>
                                <tr>
                                    <th>Produto:<br>
                                        <select  name="SearchSistema" placeholder="Sistema" id="SearchSistema" ng-model="atb_sistema" ng-change="filtrarCliente()" style="width: 60px;" >

                                            <option value=""></option>
                                            <option value="{{atb_sistema.tag_sistema}}" ng-repeat="atb_sistema in listaSistemasReleasesAbertos">{{atb_sistema.tag_sistema}} - {{atb_sistema.nome_sistema}}</option>

                                        </select>

                                    </th>                                
                                    <th>Versão:<br>  
                                        <select name="SearchCliente" id="SearchCliente" ng-model="atb_cliente" ng-change="filtrarSistema()" style="width: 50px;">

                                            <option value=""></option>
                                            <option value="{{atb_cliente.tag_cliente}}" ng-repeat="atb_cliente in listaClientesReleasesAbertos">{{atb_cliente.tag_cliente}} - {{atb_cliente.nome_cliente}}</option>

                                        </select>     
                                    </th>
                                    <th>Release:<br>
                                        <input type="text" id="branch2" name="SearchVersao" class="input-medium search-query" style="height: 20px; width: 90px;">
                                    </th>
                                    <th>Tipo<br>
                                        <input type="text" name="SearchSacpro"  class="input-medium search-query" style="height: 20px; width: 30px;">
                                    </th>
                                    <th>Entrega Fábrica<br>
                                        <input type="text" id="dataEntFab" name="SearchEntFab"  class="input-medium search-query" style=" height: 20px; width: 70px;">

                                    </th>
                                    <th>Início Homologação<br>
                                        <input type="text" id="dataInHom" name="SearchIniHom"   class="input-medium search-query" style=" height: 20px; width: 70px;">
                                    </th>
                                    <th>Plan. Expedição<br>
                                        <input type="text" id="dataPlaExp" name="SearchPlaExp"  class="input-medium search-query" style=" height: 20px; width: 70px;">
                                    </th>
                                    <th style="width:40%;  ">Observação
                                    </th>
                                    <th></th>
                                    <th></th>


                                </tr>
                        </thead>

                        <tbody>
                            <tr ng-repeat="atb_release in listaTabelaReleasesAbertos">
                                <td>{{atb_release.tag_cliente}} - {{atb_release.nome_cliente}}</td>
                                <td>{{atb_release.tag_sistema}} - {{atb_release.nome_sistema}}</td>
                                <td>{{atb_release.branch}}</td>
                                <td>{{atb_release.tipo}}</td>
                                <td>{{atb_release.dat_ent_fab}}</td>
                                <td>{{atb_release.dat_ini_hom}}</td>
                                <td>{{atb_release.dat_pla_exp}}</td>
                                <td>{{atb_release.data_exp}}</td>
                                <td>{{atb_release.observacao}}</td>
                                <td>{{atb_release.editar}}</td>
                            </tr>
                        </tbody>

