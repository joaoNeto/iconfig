<!DOCTYPE html>
<html>
<head>
    <title></title>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/heatmap.js"></script>
<link rel="stylesheet" type="text/css" href="css/painel.css">

<script type="text/javascript">
    var x = document.getElementsByClassName("highcharts-legend");
    x[2].style.display = "none";
</script>

</head>
<body>
 <div class="painel-grafico-count">
     <div class="topo-visao-geral"  ng-controller='topocontroller'>
          <div style='color: #8e8e8e;' class='bloco-topo'>
            <font>CHAMADOS</font>
            <div id='caixa-qtd' style='background-color:#8e8e8e; color:#ddd;'>{{chamados}}</div>
          </div>
          <div style='color: #00add5;'  class='bloco-topo'>
            ATENDIMENTO
            <div id='caixa-qtd'>{{atendimento}}</div>            
          </div> 
          <div style='color: #ff9525;' class='bloco-topo'>
            FABRICA
            <div id='caixa-qtd'>{{fabrica}}</div>            
          </div> 
          <div style='color: #9C27B0;' class='bloco-topo'>
            HOMOLOGAÇÃO
            <div id='caixa-qtd'>{{homologacao}}</div>            
          </div> 
          <div style='color: #4CAF50;' class='bloco-topo'>
            EXPEDIÇÃO
            <div id='caixa-qtd'>{{expedicao}}</div>            
          </div>   
          <div  class='bloco-topo' style='color:#d40000; margin-left:9%;' ng-controller='conteudopainel'>
            CRÍTICOS
            <div id='caixa-qtd' style='background-color:#d40000; color:#fff'>{{crcs_criticas}}</div>
           </div>                
     </div>
     <div class="painel-grafico-produto-cliente"  ng-controller='conteudopainel'>
        <div id="graph01" style="min-width: 100%; height: 100%; margin: 0 auto"></div>         
     </div>
 </div>
 
 <div class="painel-grafico-sac"  ng-controller='conteudopainel'>
     
    <div class="area-dia-mes">
        <div id="graph02" style="min-width: 100%; height: 100%; margin: 0 auto"></div>        
    </div>
    <div class="area-mes">
        <div id="graph-mes-sac" style="min-width: 100%; height: 100%; margin: 0 auto"></div>                
    </div>

 </div>

 <div class="painel" ng-controller='conteudopainel'>
    <div id="graph04" style="min-width: 100%; height: 100%; margin: 0 auto"></div>
 </div>
 <div class="painel"  ng-controller='conteudopainel'>
        <div class='area-titulo-grafico-3'>PRODUTO x CLIENTE</div>
        <iframe src="dados/painel.php?metodo=grafico_produto_cliente" style='width:100%;height:90%;'></iframe>
 </div>


</body>

</html>