<?php

// A sessão precisa ser iniciada em cada página diferente

    @session_start();
    
   
$nivel_necessario = '5';

// Verifica se não há a variável da sessão que identifica o usuário
if (isset($_SESSION['UsuarioID']) && ($_SESSION['UsuarioNivel'] == $nivel_necessario || $_SESSION['UsuarioNivel'] == 1)) {
	


include 'dados/alocacao.php';

if($_SESSION['UsuarioNivel'] == 1){
    $TP = 1;
    $setor = null;
    $time = null;
}else{
    $TP=0;
     $resultSetoTime = getSetorTimeUsuario($_SESSION['UsuarioID']);
   
     $setor = $resultSetoTime['id_setor'];
     $time  = $resultSetoTime['id_time'];
    
     }



echo montaGridAlocacao(1,$TP,$setor,$time);

}  else {
     
// Destrói a sessão por segurança
	session_destroy();
	// Redireciona o visitante de volta pro login
	header("Location: index.php?op=Login"); exit;

    
    
}?>        