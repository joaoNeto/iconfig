
<?php	   $conn = mysql_connect("localhost", "root", "");
			mysql_select_db("releasecontrol");
			mysql_query("SET NAMES 'utf8'");
			





date_default_timezone_set('America/Sao_Paulo');
include("../inc/jqgrid_dist.php");


// you can customize your own columns ...

$col = array();
$col["title"] = "Id"; // caption of column
$col["name"] = "id_release"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "1";
$col["editable"] = false;
$col["hidden"] = true;
$col["align"] = "center";
//$col["link"] = "grid_crc.php?id={i_solicitacao}".md5('i_solicitacao');
$cols[] = $col;		



$col = array();
$col["title"] = "Cliente";
$col["name"] = "nome_cliente";
$col["width"] = "1";
$col["editable"] = true; // this column is not editable
$col["align"] = "center"; // this column is not editable
$col["search"] = true; // this column is not searchable
$cols[] = $col;



$col = array();
$col["title"] = "Sistema";
$col["name"] = "nome_sistema";
$col["width"] = "1";
$col["editable"] = true; // this column is not editable
$col["align"] = "center"; // this column is not editable
$col["search"] = true; // this column is not searchable
$cols[] = $col;



$col = array();
$col["title"] = "Versão";
$col["name"] = "branch";
$col["width"] = "1"; // not specifying width will expand to fill space
$col["sortable"] = true; // this column is not sortable
$col["search"] = true; // this column is not searchable
$col["editable"] = true;
$col["align"] = "center";
$col["link"] = "crcs.php?id={id_release}&amp;c=".md5("{id_release}");
$cols[] = $col;


$col = array();
$col["title"] = "Tipo";
$col["name"] = "tipo";
$col["width"] = "1"; // not specifying width will expand to fill space
$col["sortable"] = true; // this column is not sortable
$col["search"] = true; // this column is not searchable
$col["editable"] = true;
$col["align"] = "center";
$cols[] = $col;

$col = array();
$col["title"] = "Data Entrada Fábrica";
$col["name"] = "dat_ent_fab"; 
$col["width"] = "1";
$col["editable"] = false; // this column is editable
$col["editoptions"] = array("size"=>20); // with default display of textbox with size 20
$col["editrules"] = array("required"=>true, "edithidden"=>true); // and is required
$col["hidden"] = false;
$col["formatter"] = "date"; // format as date
$col["formatoptions"] = array("srcformat"=>'Y-m-d',"newformat"=>'d/m/Y'); // @todo: format as date, not working with editing
$col["align"] = "center";
$col["search"] = true;
$cols[] = $col;
		
$col = array();
$col["title"] = "Data Início Homologação";
$col["name"] = "dat_ini_hom"; 
$col["width"] = "1";
$col["editable"] = false; // this column is editable
$col["editoptions"] = array("size"=>20); // with default display of textbox with size 20
$col["editrules"] = array("required"=>true, "edithidden"=>true); // and is required
$col["hidden"] = false;
$col["formatter"] = "date"; // format as date
$col["formatoptions"] = array("srcformat"=>'Y-m-d',"newformat"=>'d/m/Y'); // @todo: format as date, not working with editing
$col["align"] = "center";
$col["search"] = true;
$cols[] = $col;
		
$col = array();
$col["title"] = "Data Plan. Expedição";
$col["name"] = "dat_pla_exp"; 
$col["width"] = "1";
$col["editable"] = false; // this column is editable
$col["editoptions"] = array("size"=>20); // with default display of textbox with size 20
$col["editrules"] = array("required"=>true, "edithidden"=>true); // and is required
$col["hidden"] = false;
$col["formatter"] = "date"; // format as date
$col["formatoptions"] = array("srcformat"=>'Y-m-d',"newformat"=>'d/m/Y'); // @todo: format as date, not working with editing
$col["align"] = "center";
$col["search"] = true;
$cols[] = $col;

$col = array();
$col["title"] = "Data Expedição";
$col["name"] = "data_exp"; 
$col["width"] = "1";
$col["editable"] = false; // this column is editable
$col["editoptions"] = array("size"=>20); // with default display of textbox with size 20
$col["editrules"] = array("required"=>true, "edithidden"=>true); // and is required
$col["hidden"] = false;
$col["formatter"] = "date"; // format as date
$col["formatoptions"] = array("srcformat"=>'Y-m-d',"newformat"=>'d/m/Y'); // @todo: format as date, not working with editing
$col["align"] = "center";
$col["search"] = true;
$cols[] = $col;

$col = array();
$col["title"] = "Observação";
$col["name"] = "observacao";
$col["width"] = "1"; // not specifying width will expand to fill space
$col["sortable"] = true; // this column is not sortable
$col["search"] = true; // this column is not searchable
$col["editable"] = true;
$col["align"] = "center";
$cols[] = $col;

// To mask password field, apply following attribs
# $col["edittype"] = "password";
# $col["formatter"] = "password";

// default render is textbox
// $col["editoptions"] = array("value"=>'10');

// can be switched to select (dropdown)
# $col["edittype"] = "select"; // render as select
# $col["editoptions"] = array("value"=>'10:$10;20:$20;30:$30;40:$40;50:$50'); // with these values "key:value;key:value;key:value"

/*$cols[] = $col;

$col = array();
$col["title"] = "Closed";
$col["name"] = "closed";
$col["width"] = "50";
$col["editable"] = true;
$col["edittype"] = "checkbox"; // render as checkbox
$col["editoptions"] = array("value"=>"1:0"); // with these values "checked_value:unchecked_value"
#$col["edittype"] = "select"; // render as select
#$col["editoptions"] = array("value"=>'No:Not Booked eg. Ñ, Í,É;Yes:Yes it is Booked eg. Ñ, Í,É'); // with these values "key:value;key:value;key:value"

$cols[] = $col;
*/
$g = new jqgrid();


$grid["rowNum"] = 15; // by default 20
$grid["sortname"] = 'id_release'; // by default sort grid by this field
$grid["sortorder"] = "desc"; // ASC or DESC
$grid["grouping"] = true;
$grid["caption"] = "Grid Gerencial"; // caption of grid
$grid["autowidth"] = true; // expand grid to screen width
$grid["multiselect"] = false; // allow you to multi-select through checkboxes

$g->set_options($grid);


$g->set_actions(array(	
						"add"=>false, // allow/disallow add
						"edit"=>false, // allow/disallow edit
						"delete"=>false, // allow/disallow delete
						"rowactions"=>false, // show/hide row wise edit/del/save option
						"search" => "simple" // show single/multi field search condition (e.g. simple or advance)
					) 
				);


// you can provide custom SQL query to display data
$g->select_command = "SELECT control_release.id_release,
							 control_clientes.nome_cliente,
							 control_sistema.nome_sistema,
							 control_release.branch,
							 control_release.tipo,
							 control_release.dat_ent_fab,
							 control_release.dat_ini_hom,
							 control_release.dat_pla_exp,
							control_release.data_exp,
							control_release.observacao
					  FROM control_release,control_clientes,control_sistema
					  WHERE control_release.id_sistema = control_sistema.id_sistema
					  AND   control_release.id_cliente = control_clientes.id_cliente";
					   
					   

// this db table will be used for add,edit,delete
//$g->table = "info_download";

// pass the cooked columns to grid
$g->set_columns($cols);

// generate grid output, with unique grid name as 'list1'
$out = $g->render("list1");

$themes = array("redmond");//,"smoothness","start","dot-luv","excite-bike","flick","ui-darkness","ui-lightness","cupertino","dark-hive");
                $i = rand(0,0); 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>

	<meta name="description" content="Free Web tutorials">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="Hege Refsnes">
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" media="screen" href="../js/themes/<?php echo $themes[$i]?>/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/css/ui.jqgrid.css"></link>
 <link href="../css/menu.css" rel="stylesheet" type="text/css" />  	
  <link href="../css/style.css" rel="stylesheet" type="text/css" />  	
	
	<script src="../js/jquery.min.js" type="text/javascript"></script>
	<script src="../js/jqgrid/js/i18n/grid.locale-pt-br.js" type="text/javascript"></script>
	<script src="../js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="../js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
        
	
        <link rel="stylesheet" type="text/css" media="screen" href="../js/themes/<?php echo $themes[$i]?>/jquery-ui.custom.css"></link>	


</script>
</head>

<body>


     <center><div style="height:auto; width:auto;">
	
    	<?php	echo $out; ?>

	</div>


     </center>
     </div>
     </body>
     </html>
