/** * Função para criar um objeto XMLHTTPRequest */ 
function CriaRequest() {
 try{
 request = new XMLHttpRequest(); }
 catch (IEAtual){ 
 try{ 
 request = new ActiveXObject("Msxml2.XMLHTTP"); }
 catch(IEAntigo){ 
 try{
 request = new ActiveXObject("Microsoft.XMLHTTP"); }
 catch(falha){ 
 request = false; } 
 } 
 } if (!request) alert("Seu Navegador não suporta Ajax!");
 else return request; } 
 /** * Função para enviar os dados */ 
 function selectBases() {
 // Declaração de Variáveis 
 var result = document.getElementById("selectBases"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" style="width:50%;" />';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/selectBases.php" , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }


function selectSchemas() {
 // Declaração de Variáveis
 var base = document.getElementById("base").value; 
 var result = document.getElementById("selectSchemas"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" style="width:50%;" />';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/selectSchemas.php?base=" + base  , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }
 
function selectTabelas() {
 // Declaração de Variáveis
 var base = document.getElementById("base").value;
 var schema = document.getElementById("schema").value;  
 var result = document.getElementById("selectTabelas"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" style="width:50%;" />';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/selectTabelas.php?base=" + base + "&schema=" + schema , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }

function geraDDl() {
 // Declaração de Variáveis
 var base = document.getElementById("base").value;
 var schema = document.getElementById("schema").value;
 var tabela = document.getElementById("tabela").value;  
 var result = document.getElementById("result"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" style="margin-left:20%;"/>';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/ddl/ddl.php?base=" + base + "&schema=" + schema + "&tabela=" + tabela  , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }



function selectBasesJOB() {
 // Declaração de Variáveis 
 var result = document.getElementById("selectBases"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" />';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/selectBasesJOB.php" , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }


function getJobs() {
 // Declaração de Variáveis
 var base = document.getElementById("base").value; 
 var result = document.getElementById("result"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" style="style="margin-left:20%;"" />';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/habilitarJob.php?base=" + base  , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }


function obterJobsTabela() {
 // Declaração de Variáveis
 var base   = document.getElementById("base").value;
 var schema = document.getElementById("schema").value;
 var tabela = document.getElementById("tabela").value;  
 var result = document.getElementById("result"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" style="margin-left:20%;"/>';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/JobRecursivo.php?base=" + base + "&schema=" + schema + "&tabela=" + tabela  , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }



 function btnGerarDDl() {
 // Declaração de Variáveis  
 document.getElementById("GerarDDl").style.visibility = "visible";
 
 }
 
 function btnJob() {
 // Declaração de Variáveis  
 document.getElementById("Job").style.visibility = "visible";
 
 }
 
