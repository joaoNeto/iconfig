/** * Função para criar um objeto XMLHTTPRequest */ 
function CriaRequest() {
 try{
 request = new XMLHttpRequest(); }
 catch (IEAtual){ 
 try{ 
 request = new ActiveXObject("Msxml2.XMLHTTP"); }
 catch(IEAntigo){ 
 try{
 request = new ActiveXObject("Microsoft.XMLHTTP"); }
 catch(falha){ 
 request = false; } 
 } 
 } if (!request) alert("Seu Navegador não suporta Ajax!");
 else return request; } 
 /** * Função para enviar os dados */ 
 function habilitarJob(owner,job_name,base) {
 // Declaração de Variáveis
 var result = document.getElementById("result"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" style="margin-left:20%;" />';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/habilitarJob.php?owner=" + owner +"&job=" + job_name +"&base=" + base + "&ative=S" , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }

 function desabilitarJob(owner,job_name,base) {
 // Declaração de Variáveis
 var result = document.getElementById("result"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" style="margin-left:20%;" />';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/habilitarJob.php?owner=" + owner +"&job=" + job_name +"&base=" + base + "&ative=N" , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }

function executarJob(id,owner,job_name,base) {
 // Declaração de Variáveis
 var result = document.getElementById("resultExecute"+id); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/executando.gif" style="width:30px" />';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/executarJob.php?owner=" + owner +"&job=" + job_name +"&base=" + base , true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }


 function load(base) {
 // Declaração de Variáveis
 var result = document.getElementById("result"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="idb/util/aguarde.gif" style="margin-left:20%;" />';
 // Iniciar uma requisição 
 xmlreq.open("GET", "idb/util/habilitarJob.php?base=" + base, true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }
