<html>
<head>
<meta charset="utf8">
<title>
iDB TOOLS
</title>
<script src="jquery/jquery-1.9.1.min.js"></script>

<link href="bootstrap/css/prism.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/css/justified-nav.css" rel="stylesheet">


<script type='text/javascript' src="bootstrap/js/docs.min.js"></script>
<script type='text/javascript' src="bootstrap/js/ie10-viewport-bug-workaround.js"></script>
<script type='text/javascript' src="bootstrap/js/bootstrap.min.js"></script>
<script type='text/javascript' src="jquery/jquery.min.js"></script>
 <script type="text/javascript" src="jquery/jquery.contextmenu.r2.js"></script>
 <script type='text/javascript' src='util/ajaxHabilitaJob.js'></script>


<style>
	body {margin-left: 1%;margin-right: 1%; }
	
	#selectBases img{ width: 200px;}
	#selectSchemas img{ width: 200px;}
	#selectTabelas img{ width: 200px;}
	#result img{ width: 200px; }
	
	
</style>
</head>
	<body>
	<?php 
		$classActive  = @$_GET['a']; 
		$home = '';
		$recDDL = '';
		$job = '';
		switch ($classActive) {
			case 'home':
				$home = 'class="active"';
				break;
			case 'ddl':
				$recDDL = 'class="active"';
				break;
			case 'job':
				$job = 'class="active"';
				break;
			default:
				$home = 'class="active"';
				break;
		}
	?>

        <h3 class="text-muted">iDatabase</h3>
        
          <ul class="nav nav-pills nav-justified" >
            <li <?php echo $home; ?> ><a href="?index.php&a=home">Home</a></li>
            <li <?php echo $recDDL; ?> ><a href="<?php echo '?op='.base64_encode('RecDDL').'&a=ddl'; ?>">Recuperar DDL</a></li>
            <li <?php echo $job; ?> ><a href="<?php echo '?op='.base64_encode('Job').'&a=job'; ?>">Jobs</a></li>
            <!--<li><a href="#">Downloads</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>-->
          </ul>
        
    

		
      <?php include_once 'page/montaTemplate.php' ; ?>
		
	</body>
</html>
<!--<h3>Recuperação de DDL</h3>
<form name='ddl' method="post" action='ddl/DDL.php'>

Schema: <input name="schema" required>
Tipo Objeto: <select name="tipo">
				<option value="TABLE">TABELA</option>
			 </select>
Nome Objeto: <input name="nome" required>
banco: <select name="base">
				<option value=""></option>
				<option value="POTIGPD">POTIGPD</option>
				<option value="NEGRAOPD">NEGRAOPD</option>
				<option value="TERRAZOO">TERRAZOO</option>
				<option value="WMPH">WMPH</option>
			 </select>
<input type='submit' value="Obter DDL">
</form>
<br>
<h3>Grants do Usuário</h3>
<form name='ddl' method="post" action='ddl/ddlUsr.php'>

Schema: <input name="schema" required>
banco: <select name="base">
				<option value=""></option>
				<option value="POTIGPD">POTIGPD</option>
				<option value="NEGRAOPD">NEGRAOPD</option>
				<option value="TERRAZOO">TERRAZOO</option>
			 </select>
<input type='submit' value="Obter Grants">
</form>
<h3>Jobs Ativos e Inativos</h3>
<form name='job' method="post" action='util/Job.php'>

banco: <select name="base" required>
				<option value=""></option>
				<option value="CARREFPD">CARREFPD</option>
				<option value="CARREFPH">CARREFPH</option>
				<option value="JGBATISTA">JGBATISTA</option>
				<option value="NEGRAOPH">NEGRAOPH</option>
				<option value="NEGRAOSH">NEGRAOSH</option>
				<option value="NEGRAOSD">NEGRAOSD</option>
				<option value="POTIGPD">POTIGPD</option>
				<option value="POTIGPH">POTIGPH</option>
				<option value="WMPH">WMPH</option>
				<option value="WMSH">WMSH</option>
			 </select>
<input type='submit' value="Consultar Jobs">
</form>
</html>

-->
