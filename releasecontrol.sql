-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 05-Jun-2014 às 15:46
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `releasecontrol`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_atividades`
--

CREATE TABLE IF NOT EXISTS `control_atividades` (
  `id_atividades` int(11) NOT NULL AUTO_INCREMENT,
  `id_fabrica_viagem` int(11) NOT NULL,
  `id_profissional` int(11) NOT NULL,
  `data_atividade` datetime NOT NULL,
  `desc_atividades` text NOT NULL,
  `pendencia` varchar(1) NOT NULL DEFAULT 'N',
  `arquivo` varchar(100) DEFAULT NULL,
  `id_responsavel` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_atividades`,`id_fabrica_viagem`,`id_profissional`),
  KEY `fk_resp_pend_idx` (`id_responsavel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_cargos`
--

CREATE TABLE IF NOT EXISTS `control_cargos` (
  `id_cargo` int(11) NOT NULL AUTO_INCREMENT,
  `nome_cargo` varchar(45) NOT NULL,
  `papel_cargo` text,
  PRIMARY KEY (`id_cargo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `control_cargos`
--

INSERT INTO `control_cargos` (`id_cargo`, `nome_cargo`, `papel_cargo`) VALUES
(1, 'SCRUM MASTER', '<ul>\n<li>Planejar e mover a equipe a participar de todos os eventos definidos pelo processo de desenvolvimento;</li>\n<li>Executar o Planning 2 imediatamente após o término do P1;</li>\n<li>Criar o plano de projeto fazendo com que ele reflita tudo que foi discutido nos plannings;</li>\n<li>Divulgar para a gerencia da Fábrica e para o PO o planejamento que foi efetuado no P2 – já incluindo a previsão de testes;</li>\n<li>Realizar as reuniões diárias com a equipe;</li>\n<li>Garantir que as datas passadas no planejamento, em todos os marcos, sejam cumpridas – inclusive acompanhando a entrega em parceria com a equipe de testes;</li>\n<li>Retirar os impedimentos para execução do projeto, acionando a gerencia da fábrica sempre que necessário;</li>\n<li>Solicitar a equipe de banco apoio para que o pacote saia com a melhor qualidade possível no tocante a procedimentos e performance;</li>\n<li>Garantir a qualidade da codificação e a padronização de componentes usados para produção dos produtos;</li>\n<li>Executar o trabalho da Gerência de configuração e criar no próprio time uma estrutura de backup para atuar sempre que necessário;</li>\n</ul>'),
(2, 'ANALISTA DE SISTEMA', '<ul>\n<li>Realizar a Análise de sistema, gerando o artefato para que o desenvolvimento de cada funcionalidade possa ser direcionado da maneira mais produtiva e eficiente possível;</li></li>\n<li>Orientar os desenvolvedores nas soluções que devem ser implementadas;</li></li>\n<li>Discutir com os Regras de negócio cada nova solicitação que for feita, no intuito de atender o desejo do cliente e evoluir o produto de maneira sustentável;</li></li>\n<li>Realizar analise de impacto nas novas solicitações, para que não impactem as funcionalidades já existentes e em funcionamento;</li></li>\n<li>Seguir o planejamento que foi feito para o time, de modo a garantir junto com a equipe, que o que foi planejado será de fato entregue, no tocante a data, escopo e qualidade;</li></li>\n<li>Direcionar a padronização de tudo que será produzido pela equipe;</li></li>\n</ul>'),
(3, 'DESENVOLVEDOR', '<ul>\n<li>Seguir os direcionamentos passados pelos Analistas de Sistemas ao implementar as novas funcionalidades e correções;</li></li>\n<li>Realizar testes unitários de modo a garantir que o que foi feito teve o mais alto grau de qualidade possível;</li>\n<li>Assimilar o conhecimento passado pela equipe de Análise de sistemas de modo a evoluir sempre no conhecimento relativo a regras de negócio;</li>\n<li>Aplicar os procedimentos técnicos obrigatórios antes de liberar qualquer bloco de código para homologação;</li>\n</ul>'),
(4, 'DBA', '<ul>\n<li>Apoiar os times para liberação de novas versões;</li>\n<li>Suportar o cliente, sempre que necessário, para aplicação de novas versões;</li>\n<li>Otimizar os objetos de banco que são trabalhados pela equipe de produção;</li>\n<li>Manter as bases de dados otimizadas, para que as equipes envolvidas na produção efetuem seu trabalho de forma plena;</li>\n<li>Apontar para os Scrum Masters todos od desvios que aconteçam na produção dos times, no tocante a objetos de banco;</li>\n<li>Reportar ao gerente da fábrica erros recorrentes que aconteçam nos times;</li>\n<li>Suportar o cliente sempre que solicitado pela gestão da informata;</li>\n</ul>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_clientes`
--

CREATE TABLE IF NOT EXISTS `control_clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nome_cliente` varchar(45) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `control_clientes`
--

INSERT INTO `control_clientes` (`id_cliente`, `nome_cliente`) VALUES
(1, 'CARREFOUR'),
(2, 'WALMART'),
(3, 'POTIGUAR'),
(4, 'TERRAZOO'),
(5, 'SAO BENTO'),
(6, 'BRASIL'),
(7, 'PERERE'),
(8, 'INFORMATA'),
(9, 'NEGRAO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_crc`
--

CREATE TABLE IF NOT EXISTS `control_crc` (
  `id_crc` int(11) NOT NULL AUTO_INCREMENT,
  `num_crc` int(11) NOT NULL,
  `descricao` text NOT NULL,
  `id_release` int(11) NOT NULL,
  `situacao` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `customer` varchar(100) NOT NULL,
  PRIMARY KEY (`id_crc`),
  UNIQUE KEY `num_crc_UNIQUE` (`num_crc`),
  UNIQUE KEY `id_crc_UNIQUE` (`id_crc`),
  KEY `fk_id_release_idx` (`id_release`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16199 ;

--
-- Extraindo dados da tabela `control_crc`
--

INSERT INTO `control_crc` (`id_crc`, `num_crc`, `descricao`, `id_release`, `situacao`, `estado`, `customer`) VALUES
(15218, 36319, 'Ajuste no processo de importação de XML para Atender ao Financeiro e Fluxo de Caixa', 24, 'Validação', 'Ag.Aprovação', 'Negrao'),
(15219, 32226, 'Tratamento para inativar automaticamente produto descontinuado', 24, 'Validação', 'Ag.Aprovação', 'Negrao'),
(15220, 36654, 'Execução do "Garbage Collection" para MD-Log', 24, 'Validação', 'Ag.Aprovação', 'Negrao'),
(15221, 36896, 'Ao trocar de condição de venda deveremos implementar regras para decidir se o referido FLEX negociado poderá ou não ser respeitado.', 24, 'Validação', 'Validada OK', 'Negrao'),
(15222, 36051, 'Ajuste na manutenção de Gestão de Importação de XML', 24, 'Validação', 'Ag.Aprovação', 'Negrao'),
(15223, 36875, 'Inclusão de novo do campo Descrição', 24, 'Validação', 'Ag.Aprovação', 'Negrao'),
(15224, 34417, 'Implementar Codigo na ANP e CFOP para operação', 24, 'Validação', 'Ag.Aprovação', 'Negrao'),
(15225, 36121, 'Reprocessamento do custo médio', 24, 'Validação', 'Ag.Aprovação', 'Negrao'),
(15226, 36312, 'Alteração no envio de email de divergências comerciais', 24, 'Validação', 'Ag.Aprovação', 'Negrao'),
(15237, 36847, 'Homologação merge.', 25, 'Validação', 'Ag.Aprovação', 'Informata '),
(15238, 31137, 'Implementação de Bloqueio de locais por produto', 25, 'Validação', 'Ag.Aprovação', 'Informata '),
(15239, 36518, 'Projeto Potiguar -  Ajustar a Exclusão do Produto solicitação do ERP', 25, 'Validação', 'Ag.Aprovação', 'Informata '),
(15240, 36837, 'Implantação Potiguar - Criar Crtério de Endereço Misto por Local de Armazenamento', 25, 'Validação', 'Ag.Aprovação', 'Informata '),
(15989, 36809, 'Erro no processamento do job_importa_arquivo_IBPT - Projeto IBPT', 32, 'Validação', 'Ag.Aprovação', 'Wal Mart'),
(16082, 35011, 'Projeto SIFU - Sistema de Inteligência Fiscal Unificada', 18, 'Fábrica', 'Em Planejamento', 'Carrefour'),
(16088, 36590, 'Recepção Unificada', 33, 'Homologação', 'Em Preparação', 'Wal Mart - Interno'),
(16089, 36894, 'Ajuste na emissão do relatório na manutenção de fornecedores', 33, 'Homologação', 'Em Preparação', 'Wal Mart'),
(16090, 35883, 'Ajustar as informações da coluna “tipo de entrada” da tela Gestão / Notas de entrada', 33, 'Homologação', 'Em Preparação', 'Wal Mart'),
(16091, 37003, 'Troca do código da indústria no cadatro do produto', 33, 'Homologação', 'Em Preparação', 'Wal Mart'),
(16092, 36848, 'Ajuste na geração do lote de devolução automática', 33, 'Homologação', 'Em Preparação', 'Wal Mart'),
(16093, 36873, 'Ajuste no processo de denegação de nota fiscal', 33, 'Fábrica', 'Em Progresso', 'Wal Mart'),
(16094, 36982, 'Ajuste na ativação de produtos através da planilha', 33, 'Homologação', 'Em Preparação', 'Wal Mart'),
(16095, 36999, 'Produto do tipo Outros é apresentado na tela de gestão das regras fiscais', 33, 'Homologação', 'Em Preparação', 'Wal Mart'),
(16096, 36616, 'Travamento na aplicação no momento de gerar lote', 33, 'Finalizado', 'OK', 'Wal Mart'),
(16097, 36919, 'Integração de vendas do P2K para o MDLOG', 33, 'Homologação', 'Em Preparação', 'Wal Mart'),
(16098, 36825, 'Ajuste na rotina de acerto de balanço', 26, 'Homologação', 'Em Preparação', 'São Bento'),
(16099, 34838, 'Conversão do relatório Analítico de Compras', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16100, 35421, '(Ret.Tarcisio) Devolução interna não gera pedido', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16101, 36682, 'Quantidade Abatida na Avaria Interna.', 26, 'Homologação', 'Em Preparação', 'Informata '),
(16102, 36738, 'problemas de notas DE SAIDA:  Cliente Empresa nao enviado para TI do Piramide', 26, 'Homologação', 'Em Preparação', 'São Bento'),
(16103, 36397, 'Ajuste no status do pedido', 26, 'Homologação', 'Em Preparação', 'Informata '),
(16104, 36253, 'Erro na geração do arquivo para o coletor no inventário', 26, 'Homologação', 'Em Preparação', 'Informata '),
(16105, 36788, 'Painel Analise de Preço por NF de Compra', 26, 'Homologação', 'Em Preparação', 'Terrazoo'),
(16106, 36214, 'Ajuste na emissão de nota relacionado ao CFOP', 26, 'Homologação', 'Em Preparação', 'Terrazoo'),
(16109, 36232, 'Ajuste na emissão de nota fiscal', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16110, 36226, 'Inconsistencia na Manutenção da Gestão de Integração da Promoção Normal', 26, 'Homologação', 'Em Preparação', 'Informata '),
(16111, 36096, 'Ao solicitar o relatório de produtos com estoque zerado, o sistema abre uma inconsistencia', 26, 'Homologação', 'Em Preparação', 'Informata '),
(16112, 36058, 'Devolução automática NF (Loja tipo CD e integrada ao WMS)', 26, 'Homologação', 'Em Preparação', 'Informata '),
(16113, 36035, 'Não esta trazendo dados na gestão da promoção normal', 26, 'Homologação', 'Em Preparação', 'Informata '),
(16114, 36548, 'Ajuste na importação do arquivo de contagem', 26, 'Homologação', 'Em Preparação', 'Terrazoo'),
(16115, 36527, 'DBAMDATA"."TLOG_MANIPULA_DADOS" 7.566 GB 34.602.646 linhas', 26, 'Homologação', 'Em Preparação', 'Terrazoo'),
(16116, 36018, 'Ajuste na Manutenção de fornecedores dos produtos', 26, 'Fábrica', 'Em Progresso', 'Informata '),
(16117, 36741, 'Ajuste na consulta de estoque e preco', 26, 'Homologação', 'Em Preparação', 'São Bento'),
(16118, 36782, 'Painel de vendas', 26, 'Fábrica', 'Em Progresso', 'Terrazoo'),
(16119, 35122, 'Mudar status de nota - 5 inutilização | Não processada aguardando processamento, para nota autorizada.', 26, 'Homologação', 'Em Preparação', 'Terrazoo'),
(16120, 35588, 'Nota fiscal para dar entrada', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16122, 36257, 'Relatório de Posição de Estoque - Performance', 26, 'Fábrica', 'Em Progresso', 'São Bento'),
(16123, 35763, 'Ajuste na performance', 26, 'Homologação', 'Em Preparação', 'São Bento'),
(16124, 36183, 'Integração de nota fiscal de entrada com erro', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16125, 36790, 'Trigger na tabela de integração referente a PIS/COFINS', 26, 'Homologação', 'Em Preparação', 'São Bento'),
(16126, 36709, 'Performance da manutenção de geração de sugestão centralizada', 26, 'Homologação', 'Em Preparação', 'São Bento'),
(16127, 35224, '(Ret.Tarcísio) Permitir alteração do código de cliente do crédito', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16128, 33745, '(Ret.Tarcísio) Incluir o numero do cupom que originou a devolução na tela de movimentação das notas de crédito', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16129, 28982, '(Ret.Tarcísio) FASE 2 - GAP 07 - No atendimento de produtos estocados, quando o saldo do produto for insuficiente para atender todos os pedidos de lojas, as quantidades devem ser rateadas de forma proporcional às lojas solicitantes.', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16130, 32664, '(Ret.Tarcísio) Imprimir etiqueta de endereçamento do produto com base no preenchimento da manutenção “Controle de Endereço de Produto”.', 26, 'Fábrica', 'Em Progresso', 'Potiguar'),
(16131, 28981, '(Ret.Tarcísio) PILOTO - GAP 06 -  Criar um fator de conversão que possa repassar a litragem do produto de acordo com a sua composição de volume.', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16132, 36716, 'Notas fiscais com status 11 e 24 no integrador.', 26, 'Homologação', 'Em Preparação', 'São Bento'),
(16133, 36581, 'Ajuste na atualização do status de pré-nota', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16134, 34089, 'Ajuste nas divergências entre capa e item das notas fiscais de saída', 26, 'Homologação', 'Em Preparação', 'São Bento'),
(16135, 36662, 'Ajuste no faturamento de nota fiscal', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16136, 36968, 'Entrada por Transferência/Venda Coligada', 26, 'Fábrica', 'Em Progresso', 'Potiguar'),
(16137, 36463, 'Ajuste da emissão de nota fiscal de lote de RDM', 26, 'Homologação', 'Em Preparação', 'Potiguar'),
(16138, 36734, 'Orientação referente a coluna Autom/Manual da manutenção Protocolo de Recebimento', 26, 'Homologação', 'Em Preparação', 'São Bento'),
(16143, 32720, 'GAP 50 - Tratamento na entrada de mercadorias para produtos importados  (despesas de importação incidindo no custo do produto)', 36, 'Homologação', 'Em Progresso', 'Potiguar'),
(16157, 34745, 'Projeto FRETE', 27, 'Fábrica', 'Em Planejamento', 'Carrefour'),
(16158, 34266, 'ProjetoImpostos no Cupom - fase II', 27, 'Fábrica', 'Em Progresso', 'Carrefour'),
(16159, 35455, 'Projeto NF Complementar Entrada', 27, 'Expedição', 'Em Preparação', 'Carrefour'),
(16161, 36897, 'Ajuste na manutenção de devolução manual - Walmart', 31, 'Expedição', 'Em Preparação', 'Informata '),
(16162, 36854, 'Melhorias Regras Fiscais para o modelo Produto x Regra', 31, 'Homologação', 'Em Planejamento', 'Wal Mart - Interno'),
(16163, 34618, 'Lista Adendo - SNGPC', 31, 'Homologação', 'Em Planejamento', 'Wal Mart'),
(16164, 36885, 'Ajuste na importação da planilha de atualização de preço de venda e PMC', 31, 'Expedição', 'Em Preparação', 'Wal Mart'),
(16165, 36763, 'Ajuste na geraçãio automática do registro de não conformidade', 31, 'Atendimento', 'Em Progresso', 'Wal Mart'),
(16166, 36600, 'Ajuste na Importacao da Planilha de Produtos ja cadastrados', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16167, 36605, 'Ajuste no cadastro de promoção', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16168, 36567, 'Importação de dados da planilha não atualiza corretamente a prioridade do fornecedor do cluster de São Paulo', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16169, 36569, 'O campo “forma de rateio de frete” está impossibilitando a liberação de um lote', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16170, 36815, 'Ajuste na atualização de produtos já cadastrados', 30, 'Fábrica', 'Em Planejamento', 'Carrefour'),
(16171, 36923, 'Inconsistência na manutenção de Simulador de Preço', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16172, 36924, 'Ajuste no relatorio Auditoria dos Registros de Preços', 30, 'Homologação', 'Em Planejamento', 'Carrefour'),
(16173, 36453, 'Reposição de produtos sem movimentação de venda nos últimos 5 períodos', 30, 'Comercial', 'Ag. Definição PO', 'Carrefour'),
(16174, 36574, 'inconsistência ao tentar importar arquivo de produtos já cadastrados para a alteração de do campo Controla PMC', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16175, 36205, 'Ajuste no Livro Eletronico', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16176, 36670, 'Ajuste na atualizacao de embalagem do fornecedor', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16177, 34701, 'Ao tentar lançar notas de entrada na manutenção de transações de mensagens XM', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16178, 36851, 'Deadlock quando o EDI está processando algumas notas fiscais.', 30, 'Fábrica', 'Em Progresso', 'Carrefour'),
(16179, 36687, 'Ajuste na emissão do relatório Analítico das Movimentações por Produto', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16180, 36821, 'Ajuste na consulta F2 Consulta de Pedido de Venda Assistida.', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16181, 36706, 'Ajuste na importacao da planilha Reposicao Loja', 30, 'Expedição', 'Em Preparação', 'Carrefour'),
(16182, 36149, 'Melhoria ref retorno do pedido pelo fornecedor', 30, 'Fábrica', 'Em Planejamento', 'Carrefour'),
(16183, 36814, 'Ajuste no cadastro de produtos', 30, 'Fábrica', 'Em Progresso', 'Carrefour'),
(16184, 35657, 'Adequação referente a condição de venda', 29, 'Homologação', 'Em Preparação', 'Negrao'),
(16185, 36432, 'Criar uma pesquisa das Notas Fiscais através do CÓDIGO DO PRODUTO e do CLIENTE.', 29, 'Homologação', 'Em Preparação', 'Negrao'),
(16186, 36291, 'Job MEDI', 29, 'Fábrica', 'Em Progresso', 'Negrao'),
(16187, 36679, 'Incluir nos relatórios de Planilha de Contagem e Boletim de entrada as informações de Lote e NF.', 29, 'Fábrica', 'Em Progresso', 'Negrao'),
(16188, 36222, 'Implementar a manutenção Configuração das Negociações de Frete', 29, 'Fábrica', 'Em Progresso', 'Negrao'),
(16189, 36768, 'Criar um parâmetro retirar a opção de EXCLUIR o XML dos usuários', 29, 'Homologação', 'Em Preparação', 'Negrao'),
(16190, 36433, 'Considerar título  tipo ''JN'' para avaliação e bloqueio de crédito do cliente e para atualizar os dias em atraso desses títulos', 29, 'Homologação', 'Em Preparação', 'Negrao'),
(16191, 36622, 'Integração IFV x AFVserver', 29, 'Homologação', 'Em Preparação', 'Negrao'),
(16192, 36878, 'Manutenção de Condição de Venda - % Percentual de tolerancia para critica de fatura minima', 29, 'Fábrica', 'Em Progresso', 'Negrao'),
(16193, 36879, 'Manutenção de Pedidos Bloqueados - Flex e cobrar Frete', 29, 'Homologação', 'Em Preparação', 'Negrao'),
(16194, 36810, 'Alteração na definição da transportadora na transferência', 29, 'Homologação', 'Em Preparação', 'Negrao'),
(16195, 34834, 'Inclusão do CNPJ do emitente no relatório de lotes de devolução conforme anexo', 29, 'Fábrica', 'Em Progresso', 'Negrao'),
(16196, 34557, 'StocckBox Copia Basica - Algoritimo de Alocação por Curva ABC', 28, 'Fábrica', 'Em Progresso', 'Informata '),
(16197, 36428, 'Projeto Copia Basica StockBox - Faixa de endereços para armazenamento de pequenos volumes ou volumes unicos', 28, 'Fábrica', 'Em Progresso', 'Informata '),
(16198, 30937, 'PRIORIDADE 5 -  WMS - Criar Regra para produtos que separam e embalam por empilhamento: cadeira, sexta de bicicleta, etc..', 28, 'Fábrica', 'Em Progresso', 'Informata ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_fabrica_viagem`
--

CREATE TABLE IF NOT EXISTS `control_fabrica_viagem` (
  `id_fabrica_viagem` int(11) NOT NULL AUTO_INCREMENT,
  `data_inicial` datetime NOT NULL,
  `data_final` datetime NOT NULL,
  `desc_viagem` text NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`id_fabrica_viagem`,`id_cliente`),
  KEY `fk_viagens_cliente_idx` (`id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_fab_has_pro`
--

CREATE TABLE IF NOT EXISTS `control_fab_has_pro` (
  `id_fab_has_pro` int(11) NOT NULL AUTO_INCREMENT,
  `id_fabrica_viagem` int(11) NOT NULL,
  `id_profissional` int(11) NOT NULL,
  PRIMARY KEY (`id_fab_has_pro`,`id_fabrica_viagem`,`id_profissional`),
  UNIQUE KEY `id_profissional_UNIQUE` (`id_profissional`),
  UNIQUE KEY `id_fabrica_viagem_UNIQUE` (`id_fabrica_viagem`),
  KEY `fk_control_fabrica_viagem_has_control_profissional_control__idx1` (`id_fabrica_viagem`),
  KEY `fk_control_fab_has_pro_control_profissional1_idx` (`id_profissional`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_niveis`
--

CREATE TABLE IF NOT EXISTS `control_niveis` (
  `id_nivel` int(11) NOT NULL AUTO_INCREMENT,
  `descricao_nivel` varchar(100) NOT NULL,
  PRIMARY KEY (`id_nivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `control_niveis`
--

INSERT INTO `control_niveis` (`id_nivel`, `descricao_nivel`) VALUES
(1, 'ADM'),
(2, 'USUARIO'),
(3, 'GERENCIA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_profissional`
--

CREATE TABLE IF NOT EXISTS `control_profissional` (
  `id_profissional` int(11) NOT NULL AUTO_INCREMENT,
  `nome_profissional` varchar(200) NOT NULL,
  PRIMARY KEY (`id_profissional`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_release`
--

CREATE TABLE IF NOT EXISTS `control_release` (
  `id_release` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `id_sistema` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `dat_ent_fab` datetime DEFAULT NULL,
  `dat_ini_hom` datetime DEFAULT NULL,
  `dat_pla_exp` datetime DEFAULT NULL,
  `data_exp` datetime DEFAULT NULL,
  `observacao` text,
  `branch` varchar(45) NOT NULL,
  `editar` varchar(500) NOT NULL DEFAULT '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">',
  `deletar` varchar(500) NOT NULL DEFAULT '<img src="http://192.168.0.240/iconfig/image/delete.jpg">',
  PRIMARY KEY (`id_release`),
  KEY `fk_id_cliente_idx` (`id_cliente`),
  KEY `fk_id_sistema_idx` (`id_sistema`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Extraindo dados da tabela `control_release`
--

INSERT INTO `control_release` (`id_release`, `id_cliente`, `id_sistema`, `tipo`, `dat_ent_fab`, `dat_ini_hom`, `dat_pla_exp`, `data_exp`, `observacao`, `branch`, `editar`, `deletar`) VALUES
(18, 1, 1, 'PROJETO', '2014-07-07 00:00:00', '2014-07-09 00:00:00', '2014-08-08 00:00:00', '2014-08-11 00:00:00', 'Projeto SIT.', '12.13.00.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(24, 9, 2, 'PROJETO', '2014-05-28 00:00:00', '2014-05-29 00:00:00', '2014-05-30 00:00:00', '0000-00-00 00:00:00', 'Incluida a CRC solicitada por Ivison em campo para aumento da descrição para produtos importados.', '30.84.00.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(25, 3, 4, 'PROJETO', '2014-05-24 00:00:00', '2014-05-26 00:00:00', '2014-05-30 00:00:00', '0000-00-00 00:00:00', '', '20.29.06.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(26, 3, 1, 'PROJETO', '2014-06-09 00:00:00', '2014-06-11 00:00:00', '2014-06-16 00:00:00', '0000-00-00 00:00:00', 'Release incrementado devido a importados.', '15.32.00.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(27, 1, 1, 'PROJETO', '2014-05-09 00:00:00', '2014-05-12 00:00:00', '2014-06-13 00:00:00', '0000-00-00 00:00:00', 'Frete +\r\nNFC Entrada +\r\nImpostos Fase 2', '12.12.00.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(28, 9, 4, 'PROJETO', '2014-06-13 00:00:00', '2014-06-16 00:00:00', '2014-06-30 00:00:00', '0000-00-00 00:00:00', 'Release referente aos Algoritmos de Alocação.', '20.28.25.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(29, 9, 2, 'PROJETO', '2014-06-06 00:00:00', '2014-06-09 00:00:00', '2014-06-16 00:00:00', '0000-00-00 00:00:00', 'Linha Faturável!', '30.85.00.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(30, 1, 1, 'SAC', '2014-05-30 00:00:00', '2014-06-02 00:00:00', '2014-06-06 00:00:00', '0000-00-00 00:00:00', 'Release de SAC Mensal.', '12.10.01.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(31, 2, 1, 'SAC', '2014-05-30 00:00:00', '2014-05-30 00:00:00', '2014-06-11 00:00:00', '2014-06-11 00:00:00', 'Melhorias Regras Fiscais + SNGPC Lista Adendo', '10.57.02.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(32, 2, 1, 'SAC', '2014-06-03 00:00:00', '2014-06-03 00:00:00', '2014-06-03 00:00:00', '2014-06-03 00:00:00', 'Importação Arquivo IBPT', '10.57.01.02', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(33, 2, 1, 'SAC', '2014-06-13 00:00:00', '2014-06-13 00:00:00', '2014-06-30 00:00:00', '2014-06-30 00:00:00', 'Planejamento aberto', '10.57.03.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">'),
(36, 3, 1, 'PROJETO', '2014-05-16 00:00:00', '2014-05-20 00:00:00', '2014-06-16 00:00:00', '0000-00-00 00:00:00', 'Importados', '15.31.00.00', '<img src="http://192.168.0.240/iconfig/image/edit16_h.jpg">', '<img src="http://192.168.0.240/iconfig/image/delete.jpg">');

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_reponsavel_pendencia`
--

CREATE TABLE IF NOT EXISTS `control_reponsavel_pendencia` (
  `id_reponsavel` int(11) NOT NULL AUTO_INCREMENT,
  `nome_responsavel` varchar(200) NOT NULL,
  PRIMARY KEY (`id_reponsavel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `control_reponsavel_pendencia`
--

INSERT INTO `control_reponsavel_pendencia` (`id_reponsavel`, `nome_responsavel`) VALUES
(1, 'Informata'),
(2, 'Cliente'),
(3, 'Terceiros');

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_sistema`
--

CREATE TABLE IF NOT EXISTS `control_sistema` (
  `id_sistema` int(11) NOT NULL AUTO_INCREMENT,
  `nome_sistema` varchar(45) NOT NULL,
  `num_sistema` varchar(45) NOT NULL,
  PRIMARY KEY (`id_sistema`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `control_sistema`
--

INSERT INTO `control_sistema` (`id_sistema`, `nome_sistema`, `num_sistema`) VALUES
(1, 'MDLOG - RV', '10'),
(2, 'MDLOG - DISTRIBUICAO', '30'),
(3, 'WMS', '40'),
(4, 'STOCKBOX', '40'),
(5, 'TELEVENDAS', '17'),
(6, 'VIEW', '60'),
(7, 'DBADW', '90'),
(8, 'INTEGRADOR', '50'),
(9, 'COMERCIAL', '20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `control_usuarios`
--

CREATE TABLE IF NOT EXISTS `control_usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `login_usuario` varchar(45) NOT NULL,
  `nome_usuario` varchar(100) NOT NULL,
  `senha_usuario` varchar(45) NOT NULL,
  `senha_md5` text NOT NULL,
  `logado` int(11) NOT NULL DEFAULT '0',
  `inativo` int(11) NOT NULL DEFAULT '0',
  `id_nivel` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_nivel_acesso_idx` (`id_nivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `control_usuarios`
--

INSERT INTO `control_usuarios` (`id_usuario`, `login_usuario`, `nome_usuario`, `senha_usuario`, `senha_md5`, `logado`, `inativo`, `id_nivel`) VALUES
(1, 'andre', 'ANDRE RODRIGUES', 'teste', '', 1, 0, 3),
(2, 'ramon', 'Ramon', 'ramon', '', 0, 0, 2),
(3, 'aline', 'ALINE MARTINS', 'projetos', '', 1, 0, 3),
(4, 'adm', 'ADMINISTRADOR', 'admadm', 'b67f8b498ecedfbe30a02be0c204e46421abb031', 1, 0, 1),
(5, 'matheus.souto', 'MATHEUS SOUTO MAIOR', 'matheus@2014', '280749b6428e833bfc99d2b14803a2b7a7aca46f', 0, 0, 1),
(6, 'ceca.coelho', 'CONCEIÇÃO COELHO', 'manuela', 'd34026ada66a57509e40742655cdf270ff97c4f4', 0, 0, 1),
(7, 'helio', 'HELIO SALES', 'helio', '92fa1fc5974653ef9307573a3d37f25bae9636e9', 0, 0, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tns`
--

CREATE TABLE IF NOT EXISTS `tns` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CLIENTE` varchar(30) NOT NULL,
  `USUARIO` varchar(10) NOT NULL,
  `SENHA` varchar(30) NOT NULL,
  `IP` varchar(30) NOT NULL,
  `PORTA` int(10) NOT NULL,
  `SERVICE` varchar(10) NOT NULL,
  `ALERT` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Extraindo dados da tabela `tns`
--

INSERT INTO `tns` (`ID`, `CLIENTE`, `USUARIO`, `SENHA`, `IP`, `PORTA`, `SERVICE`, `ALERT`) VALUES
(30, 'BRASIL', 'BANCO', 'BANCO_2014', '192.168.0.231', 1521, 'BRASILSH', NULL),
(31, 'BRASIL', 'BANCO', 'BANCO_2014', '192.168.0.231', 1521, 'BRASILSD', NULL),
(32, 'CARREFOUR', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'CARREFSH', '\\hulkBDUMPalert_carrefsh.log'),
(33, 'CARREFOUR', 'BANCO', 'BANCO_2014', '192.168.0.232', 1521, 'CARREFSD', NULL),
(34, 'NEGRAO', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'NEGRAOPH', NULL),
(35, 'NEGRAO', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'NEGRAOSH', NULL),
(37, 'NEGRAO', 'BANCO', 'BANCO_2014', '192.168.0.13', 1521, 'NEGRAOPD', NULL),
(38, 'NEGRAO', 'BANCO', 'BANCO_2014', '192.168.0.232', 1521, 'NEGRAOPU', NULL),
(39, 'NEGRAO', 'BANCO', 'BANCO_2014', '192.168.0.13', 1521, 'NEGRAOSD', NULL),
(42, 'POTIGUAR', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'POTIGUAR', NULL),
(43, 'POTIGUAR', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'POTIGPH', NULL),
(44, 'POTIGUAR', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'POTIGT', NULL),
(45, 'POTIGUAR', 'BANCO', 'BANCO_2014', '192.168.0.12', 1521, 'POTIG', NULL),
(46, 'POTIGUAR', 'BANCO', 'BANCO_2014', '192.168.0.12', 1521, 'POTIGD', NULL),
(48, 'SAO BENTO', 'BANCO', 'BANCO_2014', '192.168.0.13', 1521, 'SAOBENTD', NULL),
(49, 'WALMART', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'WALMARTH', NULL),
(50, 'WALMART', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'WALMART', NULL),
(51, 'WALMART', 'BANCO', 'BANCO_2014', '192.168.0.13', 1521, 'WALMARTD', NULL),
(52, 'PERERE', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'WMSPH', NULL),
(53, 'PERERE', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'WMSSH', NULL),
(54, 'PERERE', 'BANCO', 'BANCO_2014', '192.168.0.13', 1521, 'WMSPD', NULL),
(55, 'PERERE', 'BANCO', 'BANCO_2014', '192.168.0.13', 1521, 'WMSSD', NULL),
(56, 'CARREFOUR', 'BANCO', 'BANCO_2014', '192.168.0.13', 1521, 'CARREFPJ', NULL),
(59, 'NOVA BASE', 'NOVA BASE', 'NOVA BASE', 'NOVA BASE', 0, 'NOVA BASE', NULL),
(60, 'WALMART', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'WMSNGPC', NULL),
(61, 'SAO BENTO', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'UNIFICAR', NULL),
(62, 'SAO BENTO', 'BANCO', 'BANCO_2014', '192.168.0.229', 1521, 'SAOBENTO', NULL),
(69, 'POTIGUAR', 'BANCO', 'BANCO_2014', '192.168.0.112', 1521, 'TERRAZOO', NULL),
(70, 'NEGRAO', 'BANCO', 'BANCO_2014', '192.168.0.232', 1522, 'NGHOMOLO', NULL);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `control_atividades`
--
ALTER TABLE `control_atividades`
  ADD CONSTRAINT `control_atividades_ibfk_1` FOREIGN KEY (`id_responsavel`) REFERENCES `control_reponsavel_pendencia` (`id_reponsavel`);

--
-- Limitadores para a tabela `control_crc`
--
ALTER TABLE `control_crc`
  ADD CONSTRAINT `fk_id_release` FOREIGN KEY (`id_release`) REFERENCES `control_release` (`id_release`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `control_fabrica_viagem`
--
ALTER TABLE `control_fabrica_viagem`
  ADD CONSTRAINT `fk_viagens_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `control_clientes` (`id_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `control_fab_has_pro`
--
ALTER TABLE `control_fab_has_pro`
  ADD CONSTRAINT `fk_control_fabrica_viagem_has_control_profissional_control_fa1` FOREIGN KEY (`id_fabrica_viagem`) REFERENCES `control_fabrica_viagem` (`id_fabrica_viagem`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_control_fab_has_pro_control_profissional1` FOREIGN KEY (`id_profissional`) REFERENCES `control_profissional` (`id_profissional`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `control_release`
--
ALTER TABLE `control_release`
  ADD CONSTRAINT `fk_id_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `control_clientes` (`id_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_id_sistema` FOREIGN KEY (`id_sistema`) REFERENCES `control_sistema` (`id_sistema`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `control_usuarios`
--
ALTER TABLE `control_usuarios`
  ADD CONSTRAINT `fk_nivel_acesso` FOREIGN KEY (`id_nivel`) REFERENCES `control_niveis` (`id_nivel`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
