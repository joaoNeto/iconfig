/** * Função para criar um objeto XMLHTTPRequest */ 
function CriaRequest() {
 try{
 request = new XMLHttpRequest(); }
 catch (IEAtual){ 
 try{ 
 request = new ActiveXObject("Msxml2.XMLHTTP"); }
 catch(IEAntigo){ 
 try{
 request = new ActiveXObject("Microsoft.XMLHTTP"); }
 catch(falha){ 
 request = false; } 
 } 
 } if (!request) alert("Seu Navegador não suporta Ajax!");
 else return request; } 
 /** * Função para enviar os dados */ 
 function getAlocacoes() {
 // Declaração de Variáveis
 var colaborador = document.getElementById("colaboradorAtual").value;
 var crc = document.getElementById("crcAtual").value;
 var inicio      = document.getElementById("inicioAtual").value;
 var fim      = document.getElementById("fimAtual").value;
 var result = document.getElementById("result"); 
 var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
 result.innerHTML = '<img src="image/ajax-loader2.gif"/>';
 // Iniciar uma requisição 
 xmlreq.open("GET", "dados/getAjaxAlocacao.php?col=" + colaborador + "&crc=" + crc + "&inicio=" + inicio + "&fim=" + fim, true); 
 // Atribui uma função para ser executada sempre que houver uma mudança de ado 
 xmlreq.onreadystatechange = function(){
 // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
 if (xmlreq.readyState == 4) {
 // Verifica se o arquivo foi encontrado com sucesso 
 if (xmlreq.status == 200) { 
 result.innerHTML = xmlreq.responseText; }
 else{ 
 result.innerHTML = "Erro: " + xmlreq.statusText; }
 } 
 };
 xmlreq.send(null); 
 }

