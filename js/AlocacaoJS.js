/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function ValidaCadastroAloc(){
    
    var colaborador = document.getElementById('colaborador').value;
    var crc         = document.getElementById('numeroCrc').value;   
    var inicio      = document.getElementById('dataCrcIni').value;
    var fim         = document.getElementById('dataCrcFim').value;
    var objColabo   = document.getElementById("resultadoColaborador");
    var objCrc      = document.getElementById("resultadoCRC");
    var objInicio   = document.getElementById("resultadoInicio");
    var objFim      = document.getElementById("resultadoFim");
    
    
    var dataInicio;
    var diaInicio;
    var mesInicio;
    var anoInicio;
    var validaInicio;
    
    var dataFim;
    var diaFim;
    var mesFim;
    var anoFim;
    var validaFim;
   
    //verifica se os campos estão preenchicos
    
    if(colaborador == ""){
        objColabo.innerHTML = '<div class="alert alert-danger" role="alert" style="padding:0">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe o Colaborador\n\
                                        </div>';
    }else{ objColabo.innerHTML = "";    }
    
    if(crc == "" ){
       
        objCrc.innerHTML = '<div class="alert alert-danger" role="alert" style="padding:0">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe a CRC\n\
                                        </div>';
    }else{
       
        objCrc.innerHTML = ""; 
       var crcLength = crc.length; 
        if(crcLength < 5){
            objCrc.innerHTML = '<div class="alert alert-danger" role="alert" style="padding:0">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        A crc deve conter 5 dígitos\n\
                                        </div>';
            }else{  
              objCrc.innerHTML = ""; 
        
            if(isNaN(crc)){
                
                objCrc.innerHTML = '<div class="alert alert-danger" role="alert" style="padding:0">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe apenas número\n\
                                        </div>';
                            }else{objCrc.innerHTML = ""; }
                }                        
                                        
   
   
 }
    
    
    
    if(inicio == ""){
        objInicio.innerHTML = '<div class="alert alert-danger" role="alert" style="padding:0">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                            Informe a data inicial\n\
                                        </div>';
    }else{objInicio.innerHTML = "";}
    
    if(fim == ""){
        objFim.innerHTML = '<div class="alert alert-danger" role="alert" style="padding:0">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                            Informe a data final\n\
                                        </div>';
    }else{objFim.innerHTML = "";}
    
    
    if(colaborador != ""   &&
               crc != "" &&
      !isNaN(crc) === true &&
              inicio != "" &&
                 fim != ""){
             
             
          objColabo.innerHTML = "";
          objCrc.innerHTML = "";
          objInicio.innerHTML = "";
          objFim.innerHTML = "";
          
          

            if(fim < inicio){
                objInicio.innerHTML = '<div class="alert alert-danger" role="alert" style="padding:0">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                           A data inicial deve ser menor que a data final\n\
                                        </div>';
                
            }else{
                 objInicio.innerHTML = "";
                dataInicio = inicio.split("-");
                diaInicio = dataInicio[2];
                mesInicio = dataInicio[1];
                anoInicio = dataInicio[0];

                dataFim = fim.split("-");
                diaFim = dataInicio[2];
                mesFim = dataInicio[1];
                anoFim = dataInicio[0];

                //dataInicial
                validaInicio = isValidDate(anoInicio,mesInicio,diaInicio);
                if(validaInicio == true){
                    objInicio.innerHTML = "";
                }else{
                    objInicio.innerHTML = '<div class="alert alert-danger" role="alert">\n\
                                             <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                                <span class="sr-only">Error:</span>\n\
                                                    Data Inicial Inválida\n\
                                                </div>';
                }

                //dataFinal
                validaFim = isValidDate(anoFim,mesFim,diaFim);
                if(validaFim == true){
                    objFim.innerHTML = "";
                }else{
                    objFim.innerHTML = '<div class="alert alert-danger" role="alert">\n\
                                             <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                                <span class="sr-only">Error:</span>\n\
                                                    Data Final Inválida\n\
                                                </div>';
                }
                
                
                if(validaInicio == true && validaFim == true){
                 
                 //document.getElementById('FormAlocacao').submit();
                  document.forms["FormAlocacao"].submit();
                }
            }
        }
}

function isValidDate(year, month, day) {
        var ok;
        
        ok = true;
        if( (month==01) || (month==03) || (month==05) || (month==07) || (month==08) || (month==10) || (month==12) )    
            {//mes com 31 dias
                if( (day < 01) || (day > 31) ){
                //alert('invalid date');
                ok = false;
                }
            } else
            if( (month==04) || (month==06) || (month==09) || (month==11) ){//mes com 30 dias
                if( (day < 01) || (day > 30) ){
                //alert('invalid date');
                ok = false;
                }
            } else
            if( (month==02) ){//February and leap year
                if( (year % 4 == 0) && ( (year % 100 != 0) || (year % 400 == 0) ) ){
                    if( (day < 01) || (day > 29) ){
                    //alert('invalid date');
                    ok = false;
                }
                } else {
                            if( (day < 01) || (day > 28) ){
                                //alert('invalid date');
                                ok = false;
                            }
                        }
            }
        
        return ok;
}


function ativaModalConfirma(){
    
       
        
    }
