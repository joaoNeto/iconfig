
/** * Função para criar um objeto XMLHTTPRequest */
function CriaRequest() {
    try {
        request = new XMLHttpRequest();
    }
    catch (IEAtual) {
        try {
            request = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (IEAntigo) {
            try {
                request = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (falha) {
                request = false;
            }
        }
    }
    if (!request)
        alert("Seu Navegador não suporta Ajax!");
    else
        return request;
}

function EditarLink(result,numCrc,idRel,link,funcao){
    // Declaração de Variáveis
    var link2 = document.getElementById(link).value;
    
    if (link2 == "") {
        document.getElementById(result).innerHTML = ' <div class="alert alert-danger" role="alert"><b>Favor informar um link!</b></div>';
    }else if(
            link2.indexOf("https://docs.google.com/a/informata.com.br/spreadsheets/") == -1 || 
            link2.indexOf("https://docs.google.com/a/informata.com.br/spreadsheets/")  != 0 ||
            link2.indexOf("#")  > -1||
            link2.indexOf("\\") > -1||
            link2.indexOf("--") > -1||
            link2.indexOf("?")  > -1||
            link2.indexOf("&")  > -1){
        document.getElementById(result).innerHTML = ' <div class="alert alert-danger" role="alert"><b>Favor informar um link válido!</b></div>';
    }else {
        document.getElementById(result).innerHTML = "<br>";
    
    var json = '{ "dados" : [' +
                '{"funcao":"editar","numCrc":"' + numCrc + '" , "idRel":"' + idRel + '" , "link":"' + link2 + '"}]}';
    var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
        document.getElementById(result).innerHTML = '<img src="image/ajax-loader2.gif"/>';
        // Iniciar uma requisição 
        xmlreq.open("GET", "dados/cronogramaCrcRelease.php?json=" + json, true);
        // Atribui uma função para ser executada sempre que houver uma mudança de ado 
        xmlreq.onreadystatechange = function () {
            // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
            if (xmlreq.readyState == 4) {
                // Verifica se o arquivo foi encontrado com sucesso 
                if (xmlreq.status == 200) {
                    document.getElementById(result).innerHTML = xmlreq.responseText;
                }
                else {
                    document.getElementById(result).innerHTML = "Erro: " + xmlreq.statusText;
                }
            }
        };
        xmlreq.send(null);
    }
}    
