var app = angular.module('myApp', []);

app.controller('topocontroller', function($scope,$http) {


    Highcharts.setOptions({
     colors: ['#00add5', '#ff9525', '#a369d9', '#00a760', '#DDD', '#DDD', '#DDD', '#DDD']
    });


    $http.get('dados/painel.php?metodo=count_topo')
    .success(function(data){
      $scope.chamados    = data.chamados;
      $scope.atendimento = data.atendimento;
      $scope.fabrica     = data.fabrica;
      $scope.homologacao = data.homologacao;
      $scope.expedicao   = data.expedicao;

    })
    .error(function(data){
      alert('erro ao contabilizar inidices atendimento');
    });

});

app.controller('conteudopainel', function($scope,$http) {

    /*
    --------------- EXIBE TODAS AS CRCs CRITICAS
    --------------------------------------------------------------------------
    */
    $http.get('dados/painel.php?metodo=crcs_criticas')
    .success(function(data){
        $scope.crcs_criticas = data;
    })
    .error(function(data){
      alert('erro ao contabilizar CRCs criticas');
    });


    /*
    --------------- GRAPH CLIENT X AREA
    --------------------------------------------------------------------------
    */
    $http.get('dados/painel.php?metodo=grafico_cliente_area')
    .success(function(data){ 


      Highcharts.setOptions({
       colors: ['#00add5', '#ff9525', '#a369d9', '#00a760', '#DDD', '#DDD', '#DDD', '#DDD']
      });


      Highcharts.chart('graph01', {
          chart: {
              type: 'column'
          },
          title: {
              text: 'CLIENTE X ÁREA'
          },
          xAxis: {
              categories: data.listaClientes
          },
          yAxis: {
              min: 0,
              gridLineColor: '#fff',        
              title: {
                  text: '' // title document
              },
              stackLabels: {
                  enabled: true,
                  style: {
                      fontWeight: 'bold',
                      color:  'gray'
                  }
              }
          },

          legend: {
              align: 'right',
              x: -30,
              verticalAlign: 'top',
              y: 25,
              floating: true,
              backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
              borderColor: '#fff',
              borderWidth: 1,
              shadow: false
          },
          tooltip: {
              headerFormat: '<b>{point.x}</b><br/>',
              pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
          },
          plotOptions: {
              column: {
                  stacking: 'normal',
                  dataLabels: {
                      enabled: true,
                      color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                  }
              }
          },
          series: [{
              name: 'atendimento',
              data: data.Validacao
          }, {
              name: 'fabrica',
              data: data.Fabrica
          }, {
              name: 'homologacao',
              data: data.Homologacao
          }, {
              name: 'expedição',
              data: data.Expedicao
          }]
      });

    })
    .error(function(data){
      alert('erro ao gerar grafico cliente x area');
    });



    /*
    --------------- GRAPH PRODUTO X AREA
    --------------------------------------------------------------------------
    */
    $http.get('dados/painel.php?metodo=grafico_produto_area')
    .success(function(data){ 

      Highcharts.setOptions({
       colors: ['#00add5', '#ff9525', '#a369d9', '#00a760', '#DDD', '#DDD', '#DDD', '#DDD']
      });

      Highcharts.chart('graph04', {
          chart: {
              type: 'column'
          },
          title: {
              text: 'PRODUTO X ÁREA'
          },
          xAxis: {
              categories: data.listaClientes
          },
          yAxis: {
             min: 0,
             gridLineColor: '#fff',        
             title: {
                  text: ''
              },
              stackLabels: {
                  enabled: true,
                  style: {
                      fontWeight: 'bold',
                      color:  (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                  }
              }
          },
          legend: {
              align: 'right',
              x: -30,
              verticalAlign: 'top',
              y: 25,
              floating: true,
              backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
              borderColor: '#CCC',
              borderWidth: 1,
              shadow: false
          },
          tooltip: {
              headerFormat: '<b>{point.x}</b><br/>',
              pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
          },
          plotOptions: {
              column: {
                  stacking: 'normal',
                  dataLabels: {
                      enabled: true,
                      color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                  }
              }
          },
          series: [{
              name: 'atendimento',
              data: data.Validacao
          }, {
              name: 'fabrica',
              data: data.Fabrica
          }, {
              name: 'homologacao',
              data: data.Homologacao
          }, {
              name: 'expedição',
              data: data.Expedicao
          }]
      });

    })
    .error(function(data){

    });


    /*
    --------------- SAC
    --------------------------------------------------------------------------
    */
    $http.get('dados/painel.php?metodo=grafico_sac')
    .success(function(data){ 


      Highcharts.setOptions({
       colors: ['#ff9525', '#ff9525', '#a369d9', '#00a760', '#DDD', '#DDD', '#DDD', '#DDD']
      });

      monName = new Array ("janeiro", "fevereiro", "março", "abril", "maio", "junho","julho", "agosto","setembro", "outubro", "novembro", "dezembro")
      now = new Date

      Highcharts.chart('graph02', {
          chart: {
              zoomType: 'xy'
          },
          title: {
              text: 'SAC'
          },
          subtitle: {
              text: monName[now.getMonth()]+' / '+now.getFullYear()
          },
          xAxis: [{
              categories: data.listaDias,
              crosshair: true
          }],
          yAxis: [{ // Primary yAxis
              gridLineColor: '#fff',        
              labels: {
                  format: '{value}',
                  style: {
                      color: Highcharts.getOptions().colors[1]
                  }
              },
              title: {
                  text: 'nº chamados',
                  style: {
                      color: Highcharts.getOptions().colors[1]
                  }
              }
          }, { // Secondary yAxis
              title: {
                  text: '',
                  style: {
                      color: Highcharts.getOptions().colors[0]
                  }
              },
              opposite: true
          }],
          tooltip: {
              shared: true
          },
          legend: {
              layout: 'vertical',
              align: 'left',
              x: 120,
              verticalAlign: 'top',
              y: 100,
              floating: true,
              backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
          },
          series: [{
              name: 'chamado',
              type: 'column',
              yAxis: 0,
              data: data.listaChamados,
              tooltip: {
                  valueSuffix: ' mm'
              }

          }]
      });

    })
    .error(function(data){

    });





    /*
    --------------- SAC
    --------------------------------------------------------------------------
    */
    $http.get('dados/painel.php?metodo=grafico_chamados_mes_ano')
    .success(function(data){ 


      Highcharts.setOptions({
       colors: ['#ff9525', '#ff9525', '#a369d9', '#00a760', '#DDD', '#DDD', '#DDD', '#DDD']
      });

      monName = new Array ("janeiro", "fevereiro", "março", "abril", "maio", "junho","julho", "agosto","setembro", "outubro", "novembro", "dezembro")
      now = new Date

      Highcharts.chart('graph-mes-sac', {
          chart: {
              zoomType: 'xy'
          },
          title: {
              text: 'ANO '+now.getFullYear()
          },
          xAxis: [{
              categories: data.mesesAtual,
              crosshair: true
          }],
          yAxis: [{ // Primary yAxis
              gridLineColor: '#fff',        
              labels: {
                  format: '{value}',
                  style: {
                      color: Highcharts.getOptions().colors[1]
                  }
              },
              title: {
                  text: 'nº chamados',
                  style: {
                      color: Highcharts.getOptions().colors[1]
                  }
              }
          }, { // Secondary yAxis
              title: {
                  text: '',
                  style: {
                      color: Highcharts.getOptions().colors[0]
                  }
              },
              opposite: true
          }],
          tooltip: {
              shared: true
          },
          legend: {
              layout: 'vertical',
              align: 'left',
              x: 120,
              verticalAlign: 'top',
              y: 100,
              floating: true,
              backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
          },
          series: [{
              name: 'chamado',
              type: 'column',
              yAxis: 0,
              data: data.chamadasAtual,
              tooltip: {
                  valueSuffix: ' mm'
              }

          }]
      });

    })
    .error(function(data){

    });



});
