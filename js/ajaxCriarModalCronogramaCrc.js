
/** * Função para criar um objeto XMLHTTPRequest */
function CriaRequest() {
    try {
        request = new XMLHttpRequest();
    }
    catch (IEAtual) {
        try {
            request = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (IEAntigo) {
            try {
                request = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (falha) {
                request = false;
            }
        }
    }
    if (!request)
        alert("Seu Navegador não suporta Ajax!");
    else
        return request;
}

function CriarModal(result, numCrc, idRel, funcao) {
    // Declaração de Variáveis
    var json = '{ "dados" : [' +
            '{"numCrc":"' + numCrc + '" , "idRel":"' + idRel + '" , "funcao":"' + funcao + '"}]}';
    var div_novo = document.getElementById('cronograma_novo'+numCrc);
    var div_view = document.getElementById('cronograma_view'+numCrc);
    var div_edit = document.getElementById('cronograma_edit'+numCrc);
    if (div_novo != null) {
        div_novo.remove();
    }
    if (div_view != null) {
        div_view.remove();
    }
    if (div_edit != null) {
        div_edit.remove();
    }
    var xmlreq = CriaRequest(); // Exibi a imagem de progresso 
    document.getElementById(result).innerHTML = '<img src="image/ajax-loader2.gif"/>';
    // Iniciar uma requisição 
    xmlreq.open("GET", "dados/cronogramaCrcRelease.php?jsonNovoModal=" + json, true);
    // Atribui uma função para ser executada sempre que houver uma mudança de ado 
    xmlreq.onreadystatechange = function () {
        // Verifica se foi concluído com sucesso e a conexão fechada  (readyState=4) 
        if (xmlreq.readyState == 4) {
            // Verifica se o arquivo foi encontrado com sucesso 
            if (xmlreq.status == 200) {
                document.getElementById(result).innerHTML = xmlreq.responseText;
                location.reload();
            }
            else {
                document.getElementById(result).innerHTML = "Erro: " + xmlreq.statusText;
            }
        }
    };
    xmlreq.send(null);
}
