function PercentQualiRelease(){
    var valuePercent  = document.getElementById('valPercent').value;
    var resultPercent = document.getElementById('resultPercent');
    var ok;
    ok = false;
    
    
    if(valuePercent >= 70){
        ok = true;
        faltaPra100 = 100 - valuePercent ;
        resultPercent.innerHTML= '<div style="margin-top: -38px; margin-left: 25%; margin-bottom:0; color:gold; font-size:20px;"><img style="width:50px; "  title="Acima de 70%" src="image/boaQualidade.png"> <span>Boa Qualidade!</span> </div>';
    }
    if(valuePercent >= 50 && valuePercent < 70){
        ok = true;
        faltaPra100 = 100 - valuePercent ;
        resultPercent.innerHTML= '<div style="margin-top: -38px; margin-left: 25%; margin-bottom:0; color:silver; font-size:20px;"><img src="image/qualidadeRegular.png" style="width: 50px; cursor:pointer;" title="Qualidade Regular" /> <span>Qualidade Regular!</span> </div>';
    }
    if(valuePercent < 50){
        ok = true;
        faltaPra100 = 100 - valuePercent ;
         resultPercent.innerHTML= '<div style="margin-top: -38px; margin-left: 25%; margin-bottom:0; color:#CD7F32; font-size:20px; "><img style="width:50px; "  title="Abaixo a de 50%" src="image/baixaQualidade.png"> <span>Baixa Qualidade!</span> </div>';
    }
    if(!valuePercent){
        
       resultPercent.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%;padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe a Porcentagem\n\
                                        </div>';
    }
    return ok;
}



function validaCadastroQualiRelease(){
    
    var valuePercent  = document.getElementById('valPercent').value;
    var resultPercent = document.getElementById('resultPercent');
    var valueObserva  = document.getElementById('valObservacao').value;
    var resultObserva = document.getElementById('resultObservacao');
    var valueFile  = document.getElementById('valFile').value;
    var resultFile = document.getElementById('resultFile');
    var faltaPra100 ;
    
    
    //mensagens do campo File
    if(valueFile == ""){
        resultFile.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -25px; margin-left: 25%;padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Selecione um arquivo\n\
                                        </div>';
    }else{
        resultFile.innerHTML = "";
    }
    
    
    //mensagens do campo Observacao
    if(valueObserva == ""){
        resultObserva.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%;padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe a Observação\n\
                                        </div>';
    }else{
        resultObserva.innerHTML = "";
    }
    
    
    //mensagens do campo Percentual
    if(valuePercent == "" ){
       
        resultPercent.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%;padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe a Porcentagem\n\
                                        </div>';
    }else{
       
        resultPercent.innerHTML = ""; 
       var valuePercentLength = valuePercent.length;
       
        if(valuePercentLength > 3){
            resultPercent.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%; padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        A porcentagem só pode conter 3 números!\n\
                                        </div>';
            }else if(valuePercent > 100){
            resultPercent.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%; padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        A porcentagem deve ser menor ou igual a 100% !\n\
                                        </div>';
            }else{  
              resultPercent.innerHTML = ""; 
        
            if(isNaN(valuePercent)){
                
                resultPercent.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%; padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe apenas número\n\
                                        </div>';
                            }else{resultPercent.innerHTML = ""; }
                }                        
                                        
   
   
 }
 var ok;
 
 if(valuePercent != ""  &&
    valuePercent  <= 100 &&
    valuePercentLength <= 3 &&
    !isNaN(valuePercent) === true){
 
        ok = PercentQualiRelease();
    
    if(ok == true){
        document.forms["formQualidade"].submit();
    }
    
    
}
}


function validaEditaQualiRelease(){
    
    var valuePercent  = document.getElementById('valPercent').value;
    var resultPercent = document.getElementById('resultPercent');
    var valueObserva  = document.getElementById('valObservacao').value;
    var resultObserva = document.getElementById('resultObservacao');
    var faltaPra100 ;
    
    
   
    
    //mensagens do campo Observacao
    if(valueObserva == ""){
        resultObserva.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%;padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe a Observação\n\
                                        </div>';
    }else{
        resultObserva.innerHTML = "";
    }
    
    
    //mensagens do campo Percentual
    if(valuePercent == "" ){
       
        resultPercent.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%;padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe a Porcentagem\n\
                                        </div>';
    }else{
       
        resultPercent.innerHTML = ""; 
       var valuePercentLength = valuePercent.length;
       
        if(valuePercentLength > 3){
            resultPercent.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%; padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        A porcentagem só pode conter 3 números!\n\
                                        </div>';
            }else if(valuePercent > 100){
            resultPercent.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%; padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        A porcentagem deve ser menor ou igual a 100% !\n\
                                        </div>';
            }else{  
              resultPercent.innerHTML = ""; 
        
            if(isNaN(valuePercent)){
                
                resultPercent.innerHTML = '<div class="alert alert-danger" role="alert" style="margin-top: -30px; margin-left: 25%; padding:0; margin-bottom:0;">\n\
                                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n\
                                        <span class="sr-only">Error:</span>\n\
                                        Informe apenas número\n\
                                        </div>';
                            }else{resultPercent.innerHTML = ""; }
                }                        
                                        
   
   
 }
 var ok;
 
 if(valuePercent != ""  &&
    valuePercent  <= 100 &&
    valuePercentLength <= 3 &&
    !isNaN(valuePercent) === true){
 
        ok = PercentQualiRelease();
    
    if(ok == true){
        document.forms["formQualidade"].submit();
    }
    
    
}
}