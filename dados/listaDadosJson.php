<?php 

include "conexao.php";

$data = array();

/*
------------- BUSCAS TELA RELEASES ENVIADOS
-----------------------------------------------------------------------------------------
*/
if($_GET['param'] == 'listaSistemas' and $_GET['pag'] == 'rel_abertos'){

	if(!empty($_GET['filtro'])){

		$sqlQuery = "SELECT 
						control_sistema.tag_sistema as tag_sistema,
						control_sistema.nome_sistema as nome_sistema
					FROM control_release
						join control_sistema on control_release.id_sistema = control_sistema.id_sistema
						join control_clientes on control_clientes.id_cliente = control_release.id_cliente
					WHERE control_clientes.nome_cliente = '".$_GET['filtro']."'
					AND control_release.data_exp    = '0000-00-00' 
					group by control_sistema.nome_sistema
					ORDER BY control_release.id_cliente ASC 
					";
	}else{
		
		$sqlQuery = "SELECT tag_sistema,nome_sistema FROM control_sistema order by 2";
		
	}

}

if($_GET['param'] == 'listaClientes' and $_GET['pag'] == 'rel_abertos'){

	if(!empty($_GET['filtro'])){
		$sqlQuery = "SELECT 
						control_clientes.nome_cliente,
					    control_clientes.tag_cliente
					FROM control_release
						join control_sistema on control_release.id_sistema = control_sistema.id_sistema
						join control_clientes on control_clientes.id_cliente = control_release.id_cliente
					WHERE control_sistema.nome_sistema = '".$_GET['filtro']."'
					AND control_release.data_exp    = '0000-00-00' 
					group by control_clientes.nome_cliente
					ORDER BY control_release.id_cliente ASC ";
	}else{

		$sqlQuery = "SELECT tag_cliente,nome_cliente FROM control_clientes WHERE tag_cliente <> '' order by 2";
	
	}

}
if($_GET['param'] == 'listaSistemas' and $_GET['pag'] == 'rel_env'){

	if(!empty($_GET['filtro'])){

		$sqlQuery = "SELECT 
						control_sistema.tag_sistema as tag_sistema,
						control_sistema.nome_sistema as nome_sistema
					FROM control_release
						join control_sistema on control_release.id_sistema = control_sistema.id_sistema
						join control_clientes on control_clientes.id_cliente = control_release.id_cliente
					WHERE control_clientes.nome_cliente = '".$_GET['filtro']."'
					AND control_release.data_exp  <> '0000-00-00' 
					group by control_sistema.nome_sistema
					ORDER BY control_release.id_cliente ASC 
					";
	}else{
		
		$sqlQuery = "SELECT tag_sistema,nome_sistema FROM control_sistema order by 2";
		
	}

}

if($_GET['param'] == 'listaClientes' and $_GET['pag'] == 'rel_env'){

	if(!empty($_GET['filtro'])){
		$sqlQuery = "SELECT 
						control_clientes.nome_cliente,
					    control_clientes.tag_cliente
					FROM control_release
						join control_sistema on control_release.id_sistema = control_sistema.id_sistema
						join control_clientes on control_clientes.id_cliente = control_release.id_cliente
					WHERE control_sistema.nome_sistema = '".$_GET['filtro']."'
					AND control_release.data_exp <> '0000-00-00' 
					group by control_clientes.nome_cliente
					ORDER BY control_release.id_cliente ASC ";
	}else{

		$sqlQuery = "SELECT tag_cliente,nome_cliente FROM control_clientes WHERE tag_cliente <> '' order by 2";
	
	}

}
/*
------------- BUSCAS TELA RELEASES ABERTOS
-----------------------------------------------------------------------------------------
*/
if($_GET['param'] == 'listaSistemasReleasesAbertos'){

	if(!empty($_GET['filtro'])){

		$sqlQuery = "SELECT 
						control_sistema.nome_sistema,
						control_sistema.tag_sistema
					FROM control_release,control_clientes,control_sistema
					WHERE 
						control_clientes.tag_cliente = '{$_GET['filtro']}'
						AND control_release.id_sistema = control_sistema.id_sistema
						AND control_release.id_cliente = control_clientes.id_cliente
						AND control_release.data_exp = '0000-00-00'
					group by control_sistema.nome_sistema
					";



	}else{
		
		$sqlQuery = "SELECT 
						control_sistema.nome_sistema,
						control_sistema.tag_sistema
					FROM control_release,control_clientes,control_sistema
					WHERE 
						control_release.id_sistema = control_sistema.id_sistema
						AND control_release.id_cliente = control_clientes.id_cliente
						AND control_release.data_exp = '0000-00-00'
					group by control_sistema.nome_sistema";
		
	}

}

if($_GET['param'] == 'listaClientesReleasesAbertos'){

	if(!empty($_GET['filtro'])){
		$sqlQuery = "SELECT 
						control_clientes.nome_cliente,
						control_clientes.tag_cliente
					FROM control_release,control_clientes,control_sistema
					WHERE 
						control_sistema.tag_sistema = '{$_GET['filtro']}'
						AND control_release.id_sistema = control_sistema.id_sistema
						AND control_release.id_cliente = control_clientes.id_cliente
						AND control_release.data_exp = '0000-00-00'
					group by control_clientes.nome_cliente";
	}else{

		$sqlQuery = "SELECT 
						control_clientes.nome_cliente,
						control_clientes.tag_cliente
					FROM control_release,control_clientes,control_sistema
					WHERE 
						control_release.id_sistema = control_sistema.id_sistema
						AND control_release.id_cliente = control_clientes.id_cliente
						AND control_release.data_exp = '0000-00-00'
					group by control_clientes.nome_cliente";
	
	}

}
/*
------------- LISTA RELEASES ABERTOS
-----------------------------------------------------------------------------------------
*/
if($_GET['param'] == 'exibirReleaseAberto'){

	$sqlQuery = "SELECT control_release.id_release,
						control_clientes.nome_cliente,
				        control_clientes.tag_cliente,
						control_sistema.nome_sistema,
				        control_sistema.tag_sistema,
						control_release.branch,
						control_release.tipo,
						control_release.dat_ent_fab,
						control_release.dat_ini_hom,
						control_release.dat_pla_exp,
						control_release.data_exp,
						control_release.observacao,						
				        control_release.emergencial
				FROM control_release,control_clientes,control_sistema
				WHERE 
					control_release.id_sistema = control_sistema.id_sistema
					AND control_release.id_cliente = control_clientes.id_cliente
					AND control_release.data_exp = '0000-00-00'
				ORDER BY dat_pla_exp DESC ";

}


$execQuery = mysql_query($sqlQuery,$conn);

//segundo parametro do mysqlFecthArray passar MYSQL_ASSOC ou MYSQL_NUM.
while ($row = mysql_fetch_array($execQuery,MYSQL_ASSOC)) {
	if(!empty($row['observacao'])){
		$row['observacao'] = str_replace("/", "\/", $row['observacao']);	
		$row['observacao'] = utf8_encode($row['observacao']);
	}
    $data[] = $row;
}

echo json_encode($data);

?>
