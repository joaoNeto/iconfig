<?php 

/*
	SCRIPT UTILIZADO NA PÁGINA PAINEL
	URL.: http://localhost/iconfig/?p=painel
*/
Class Painel{


	public function __construct()
	{
	
	}

	/* EXIBE O COUNT DOS CHAMADOS, ATENDIMENTO, FABRICA, HOMOLOGAÇÃO E EXPEDIÇÃO  */
	public function count_topo()
	{

		include "conexao.php";

		$sql = "select TOP 1 
		(SELECT count(*) FROM InnovativeSuite2.dbo.SMCandidata WHERE TipoMudanca in (4,7,130,138,142) and situacao in (1,2)) as Atendimento, 
		(SELECT count(*) FROM InnovativeSuite2.dbo.SMCandidata WHERE TipoMudanca in (4,7,130,138,142) and situacao = 4) as Fabrica, 
		(SELECT count(*) FROM InnovativeSuite2.dbo.SMCandidata WHERE TipoMudanca in (4,7,130,138,142) and situacao = 7) as Homologacao, 
		(SELECT count(*) FROM InnovativeSuite2.dbo.SMCandidata WHERE TipoMudanca in (4,7,130,138,142) and situacao = 9) as Expedicao,
		                                (SELECT count(*) FROM InnovativeSuite2.dbo.SMCandidata WHERE TipoMudanca in (4,7,130,138,142) and situacao in (1,2,4,7,9)) as Total 
		from
		InnovativeSuite2.dbo.SMCandidata";			

		$resultado = odbc_exec($connect, $sql);
		odbc_fetch_row($resultado);

		$arrayRetorno = array();

		$arrayRetorno['atendimento'] = odbc_result($resultado,'Atendimento');
		$arrayRetorno['fabrica']     = odbc_result($resultado,"Fabrica") ;
		$arrayRetorno['homologacao'] = odbc_result($resultado,"Homologacao") ;
		$arrayRetorno['expedicao']	 = odbc_result($resultado,"Expedicao") ;
		$arrayRetorno['chamados']    = $arrayRetorno['atendimento']+$arrayRetorno['fabrica']+$arrayRetorno['homologacao']+$arrayRetorno['expedicao'];

		echo json_encode($arrayRetorno);

		odbc_free_result($resultado);
		odbc_close($connect);
	}

	public function crcs_criticas()
	{
		include "conexao.php";

		$sql = "SELECT 
					DISTINCT CRC.SMCCode,CRC.prioridade,CRC.Subject,stat.sitDesc,CRC.DtSubmissao,esta.estadoDesc,CLI.CustomerName
				FROM
					SMCandidata AS CRC,
					SituacaoSMC as stat,
					EstadosSMC as esta,
					CL_Customer as CLI
				WHERE CRC.CustomerCode = CLI.Code
					AND stat.sitCode = crc.Situacao
					AND esta.estadoCod = crc.estado
					AND stat.sitDesc <> 'Registrado'
					AND stat.sitCode NOT IN (3,5,10)
					AND CRC.prioridade in (2,4)
					AND CRC.TipoMudanca in (4,7,130,138,142);";

		$resultCriticas = odbc_exec($connect, $sql);
		$Nrows = odbc_num_rows($resultCriticas);

		echo json_encode($Nrows);

	}


	public function grafico_cliente_area()
	{
		include "conexao.php";

		$arrayCliente 	= array();
		$arrayJson 		= array(array());
		$arraySituacao 	= array();
		$arrayPontuacao = array(array());
		$arrayListaCliente = array();
		$qtdLinhaCliente  = 0;
		$qtdLinhaSituacao = 0;		

		/*
		------------- VINCULAR VALORES AOS CLIENTES
		--------------------------------------------------------------
		*/
		$sql = "SELECT 
					control_clientes.tag_cliente as cliente,
				    control_crc.situacao,
				    count(control_crc.situacao) as qtd
				FROM 
					control_release
					JOIN control_clientes ON control_clientes.id_cliente = control_release.id_cliente
				    JOIN control_crc ON control_crc.id_release = control_release.id_release
				WHERE 
				    control_release.data_exp = '0000-00-00'  
				GROUP BY 	
					control_clientes.tag_cliente,
				    control_crc.situacao";

		$resultado = mysql_query($sql);

		while($linha = mysql_fetch_array($resultado,MYSQL_ASSOC))
		{

			// array listando clientes
			if(!in_array($linha['cliente'], $arrayCliente))
			{
				$arrayCliente[] = $linha['cliente'];
				$qtdLinhaCliente++;
			}
			// array listando situacao
			if(!in_array($linha['situacao'], $arraySituacao))
			{
				$arraySituacao[] = $linha['situacao'];
				$qtdLinhaSituacao++;
			}

			$chaveCliente   = (int)(array_search($linha['cliente'], $arrayCliente));
			$chaveSituacao  = (int)(array_search($linha['situacao'], $arraySituacao));
			$arrayPontuacao[$chaveCliente][$chaveSituacao] = $linha['qtd'];
		}


		$sql = "SELECT tag_cliente FROM control_clientes";

		/*
		------------- LISTANDO CLIENTES CLIENTES E DECLARANDO VALORES NULOS A PONTUACAO
		--------------------------------------------------------------
		*/
		$resultado = mysql_query($sql);

		while($linha = mysql_fetch_array($resultado,MYSQL_ASSOC))
		{
			// array listando clientes
			if(!in_array($linha['tag_cliente'], $arrayCliente))
			{
				$arrayCliente[] = $linha['tag_cliente'];
				$qtdLinhaCliente++;
			}
			
		}

		$arrayJson['listaClientes'] = $arrayCliente;

		// FAZENDO AS LISTAS COM AS SITUACOES E SEUS RESPECTIVOS VALORES
		for($i = 0 ; $i < $qtdLinhaSituacao ; $i++)
		{
			for($j = 0 ; $j < $qtdLinhaCliente ; $j++)
			{

				if(empty($arrayPontuacao[$j][$i]) || $arrayPontuacao[$j][$i] == '')
				{
					$arrayPontuacao[$j][$i] = null;
				}

				if(utf8_encode($arraySituacao[$i]) == "Expedição"){
					$arrayJson['Expedicao'][] = (int)$arrayPontuacao[$j][$i];
				}

				if(utf8_encode($arraySituacao[$i]) == "Fábrica"){
					$arrayJson['Fabrica'][]   = (int)$arrayPontuacao[$j][$i];
				}

				if(utf8_encode($arraySituacao[$i]) == "Homologação"){
					$arrayJson['Homologacao'][] = (int)$arrayPontuacao[$j][$i];
				}

				if(utf8_encode($arraySituacao[$i]) == "Validação"){
					$arrayJson['Validacao'][] = (int)$arrayPontuacao[$j][$i];
				}

			}

		}

		echo json_encode($arrayJson);

	}

	public function grafico_sac()
	{

		include "conexao.php";

		// pegando dia inicial e final do mes atual
		$data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
		$data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

		$diaAtual    = date('Y-m-d',$data_fim);
		$mesPassado  = date('Y-m-d',$data_incio);
		$dataAtual   = strtotime( $diaAtual );
		$dataPassada = strtotime( $mesPassado );
		$jsonRetorno = array();
		$arrayDias 	 = array();
		$arrayChamados = array();
		$arrayChamadosAnterior = array();

		// SQL GRAFICO CHAMADOS NO INTERVALO DO MES ATUAL ATÉ 1 MES
		$sqlAtual = "SELECT 
						DATE_FORMAT(dat_ent_fab,'%d/%m') AS diaMes,
					    count(*) as chamadas
					FROM control_release
					WHERE dat_ent_fab BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) and CURDATE()
					AND 
					control_release.data_exp = '0000-00-00' 
					group by DATE_FORMAT(dat_ent_fab,'%d / %M')";

		// SQL GRAFICO CHAMADOS NO INTERVALO DE 1 ANO ATRAS ATÉ 1 ANO E 1 MES
		$sqlAnoAnterior = " SELECT 
								DATE_FORMAT(dat_ent_fab,'%d / %m') AS diaMes,
							    count(*) as chamadas
							FROM control_release
							WHERE dat_ent_fab BETWEEN DATE_SUB(CURDATE(), INTERVAL 385 DAY) and DATE_SUB(CURDATE(), INTERVAL 365 DAY) 
							AND 
							control_release.data_exp = '0000-00-00' 
							group by DATE_FORMAT(dat_ent_fab,'%d / %M')";

		// PEGANDO DIAS NO INTERVALO DE 1 MES
		while ( $dataPassada <= $dataAtual )
		{
			$arrayDias[] = date('d/m',$dataPassada);
			$dataPassada += 86400;
		}

		// montando arrays vazios com o mesmo tamanho dos dias
		for($i = 0 ; $i < count($arrayDias) ; $i++)
		{
			$arrayChamados[$i] = 0;
			$arrayChamadosAnterior[$i] = 0;
		}

		$resultadoQuery = mysql_query($sqlAtual);

		while($linha = mysql_fetch_array($resultadoQuery,MYSQL_ASSOC))
		{
			if(in_array($linha['diaMes'],$arrayDias)){
				$indice = (array_keys($arrayDias,$linha['diaMes']));
				$arrayChamados[(int)($indice[0])] = (int)$linha['chamadas'];
			}
		}

		$resultadoQueryAnterior = mysql_query($sqlAnoAnterior);

		while($linha = mysql_fetch_array($resultadoQueryAnterior,MYSQL_ASSOC))
		{
			if(in_array($linha['diaMes'],$arrayDias)){
				$indice = (array_keys($arrayDias,$linha['diaMes']));
				$arrayChamadosAnterior[(int)($indice[0])] = (int)$linha['chamadas'];
			}			
		}		

		$jsonRetorno['listaDias'] 	  = $arrayDias;
		$jsonRetorno['listaChamados'] = $arrayChamados;
		$jsonRetorno['listaMedia']	  = $arrayChamadosAnterior;

		echo json_encode($jsonRetorno);

	}

	public function grafico_chamados_mes_ano()
	{
		$arrayChamadosAtual 	= array();
		$arrayChamadosPassados 	= array();
		$arrayMeses 			= array();
		$jsonRetorno 			= array();

		include "conexao.php";

		/* -- SQL DE TODOS OS CHAMADOS DO ANO ATUAL SEPARADOS POR MES -- */
		$sql = "SELECT 
				    DATE_FORMAT(dat_ent_fab,'%M') AS Mes,
				    count(*) as chamadas
				FROM control_release
				WHERE
				    dat_ent_fab BETWEEN DATE_FORMAT(CURDATE(),'%Y-01-01') and CURDATE()
				    AND control_release.data_exp <> '0000-00-00' 
				group by DATE_FORMAT(dat_ent_fab,'%m/%Y')";

		/* -- SQL DE TODOS OS CHAMADOS DO ANO PASSADO SEPARADOS POR MES -- */
		$sqlAnoAnterior = "SELECT 
							    DATE_FORMAT(dat_ent_fab,'%M') AS Mes,
							    count(*) as chamadas
							FROM control_release
							WHERE
							    dat_ent_fab BETWEEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR),'%Y-01-01') and DATE_SUB(CURDATE(), INTERVAL 1 YEAR)
							    AND control_release.data_exp <> '0000-00-00' 
							group by DATE_FORMAT(dat_ent_fab,'%m/%Y')";

		$resultado = mysql_query($sql);

		while($atb = mysql_fetch_array($resultado,MYSQL_ASSOC)){
			$arrayMeses[] = $atb['Mes'];
			$arrayChamadosAtual[] = (int)$atb['chamadas'];
		}

		$jsonRetorno['mesesAtual'] 	  = $arrayMeses;
		$jsonRetorno['chamadasAtual'] = $arrayChamadosAtual;

		echo json_encode($jsonRetorno);

	}

	public function grafico_produto_cliente()
	{

		include "conexao.php";
		error_reporting(0);

		$listaClientes = array();
		$listaProdutos = array();
		$arrayJson 	   = array(array());
		$pontuacao 	   = array(array()); // utilizado na tabela dinamica
		$pontuacao2    = array(array()); // utilizado na tabela highchart
		//$ptFinal 	   = array(array());
		$qtdClientes   = 0;
		$qtdProdutos   = 0;

		/*
		------------- LISTA CLIENTES
		-------------------------------------------------------------------------------------------
		*/
		$sql 		 = "SELECT tag_cliente FROM control_clientes";
		$resultado   = mysql_query($sql);
		while($linha = mysql_fetch_array($resultado,MYSQL_ASSOC)){
			$listaClientes[] = $linha['tag_cliente'];
			$qtdClientes++;
		}

		/*
		------------- LISTA PRODUTOS
		-------------------------------------------------------------------------------------------
		*/
		$sql 		 = "SELECT tag_sistema as produto FROM control_sistema";
		$resultado 	 = mysql_query($sql);
		while($linha = mysql_fetch_array($resultado,MYSQL_ASSOC)){
			$listaProdutos[] = $linha['produto'];
			$qtdProdutos++;
		}

		/*
		------------- PREENCHENDO PONTUACAO COM VALORES NULLOS
		-------------------------------------------------------------------------------------------
		*/

		for($linha = 0 ; $linha < $qtdProdutos ; $linha++){
			for($coluna = 0 ; $coluna < $qtdClientes ; $coluna++){
				$pontuacao[$linha][$coluna]  = 0;
				$pontuacao2[] = array($linha,$coluna,0);
				$ptFinal[] = array($linha,$coluna,0);	
			}
		}

		/*
		------------- VINCULAR PONTUACAO AO PRODUTO / CLIENTE
		-------------------------------------------------------------------------------------------
		*/		
		$sql = "SELECT 
					control_sistema.tag_sistema as produto,
				    control_clientes.tag_cliente cliente,
				    count(*) total
				FROM control_release
				    JOIN control_sistema ON control_sistema.id_sistema = control_release.id_sistema
				    JOIN control_clientes ON control_clientes.id_cliente = control_release.id_cliente
				    JOIN control_crc ON control_crc.id_release = control_release.id_release
				GROUP BY
					control_sistema.tag_sistema,
				    control_clientes.tag_cliente";

		$resultado = mysql_query($sql);
		while($atb = mysql_fetch_array($resultado, MYSQL_ASSOC))
		{
			
			$indiceProduto = (int)(array_keys($listaProdutos, $atb['produto'])[0]);
			$indiceCliente = (int)(array_keys($listaClientes, $atb['cliente'])[0]);
			$pontuacao[$indiceProduto][$indiceCliente] = $atb['total'];
			
			/*
			$indiceProduto = (int)(array_keys($listaProdutos, $atb['produto'])[0]);
			$indiceCliente = (int)(array_keys($listaClientes, $atb['cliente'])[0]);
			$pontuacao2[] = array($indiceProduto, $indiceCliente, (int)$atb['total']);
			*/
		}

		/*
		------------- MONTANDO TABELA
		-------------------------------------------------------------------------------------------
		*/	

		echo "<style>
				html{
					overflow:hidden;
				}
				.tabela {
				    width: 113%;
				    font-size: 13px;
				    text-align: center;
				    color: #7b7b7b;
				    margin-left: -105px;
				    margin-top: -5px;
				}

			    #td-titulo-produto{
			    	width:3%;
			    	text-align:right;
			    	border:none !important;
			    	font-family: 'Lucida Grande', 'Lucida Sans Unicode', Arial, Helvetica, sans-serif;
    				color: #434348;
				}
				.tr-produto{

				}
				.tr-cliente{

				}
				#td-pontuacao{
					height: 20px;
				    width:1%;
				    padding-top: 1;
    				padding-bottom: 1;
    				padding-left: 0;
    				padding-right: 0;
				}
				.table {
    				border-collapse: collapse !important;
  				}
  				.table-bordered th,
  				.table-bordered td {
    				border: 1px solid #ddd !important;
  				}
				#td-cliente{
  					width:1%;
  					border:none !important;
  					font-family: 'Lucida Grande', 'Lucida Sans Unicode', Arial, Helvetica, sans-serif;
    				color: #434348;
				}

				.text-success {				  
				    color: #fff;
  					background-color: #3c763d;
  					border-color: #357ebd;
				}

				.text-info {
				  	color: #fff;
  					background-color: #428bca;
  					border-color: #357ebd;
				}

				.text-warning {				  
				  	color: #fff;
  					background-color: #8a6d3b;
  					border-color: #357ebd;
				}

				.text-danger {				  
				  	color: #fff;
  					background-color: #a94442;
  					border-color: #357ebd;
				}
	

			  </style>
			 ";

		// rabdinuzabdi cores
		$cores = array('#016A4B','#9EC701','#414141','#787878', '#9A9A9A','#0132B3','#0055FE','#77AEFF','#00D1B4','#9ec600','#FF8C00','#F4A460','#DEB887','#3CB371','#B8860B','#00BFFF','#4682B4','#2E8B57','#DDA0DD');

		echo "<table border='0' class='tabela table table-bordered'>";
		// listando produtos
		for($i = 0 ; $i < $qtdProdutos ; $i++)
		{
			echo "<tr class='tr-produto' >";
			echo "<td id='td-titulo-produto' style='color:".$cores[$i]."'>".$listaProdutos[$i]."</td>";			
			// listando valores dos produtos x clientes
			for($j = 0 ; $j < $qtdClientes ; $j++)
			{
				if($pontuacao[$i][$j] != 0){
					echo "<td id='td-pontuacao' style='background-color:".$cores[$i]."; color:white'>".$pontuacao[$i][$j]."</td>";
				}else{
					echo "<td id='td-pontuacao'></td>";	
				}
							
			}
			echo "</tr>";
		}

		echo "<tr class='tr-cliente'>";
		echo "<td style='border:none !important;'></td>";
		// listando clientes
		for($i = 0 ; $i< $qtdClientes ; $i++)
		{
			echo "<td id='td-cliente'>".$listaClientes[$i]."</td>";			
		}

		echo "</tr>";

		echo "</table>";

	}

	public function grafico_produto_area()
	{

		include "conexao.php";

		$arrayProduto 	  = array();
		$arrayJson 		  = array(array());
		$arraySituacao 	  = array();
		$arrayPontuacao   = array(array());
		$qtdLinhaProduto  = 0;
		$qtdLinhaSituacao = 0;		

		$sql = "SELECT 
					control_sistema.tag_sistema as tag_produto,
				    control_crc.situacao,
				    count(control_crc.situacao) as qtd
				FROM control_release 
					JOIN control_sistema ON control_sistema.id_sistema = control_release.id_sistema
				    JOIN control_crc ON control_crc.id_release = control_release.id_release
					WHERE 
					control_release.data_exp = '0000-00-00'  
				GROUP BY 	control_sistema.tag_sistema, control_crc.situacao";

		$resultado = mysql_query($sql);

		while($linha = mysql_fetch_array($resultado,MYSQL_ASSOC))
		{

			// array listando clientes
			if(!in_array($linha['tag_produto'], $arrayProduto))
			{
				$arrayProduto[] = $linha['tag_produto'];
				$qtdLinhaProduto++;
			}
			// array listando situacao
			if(!in_array($linha['situacao'], $arraySituacao))
			{
				$arraySituacao[] = $linha['situacao'];
				$qtdLinhaSituacao++;
			}		

			$chaveCliente   = (int)(array_search($linha['tag_produto'], $arrayProduto));
			$chaveSituacao  = (int)(array_search($linha['situacao'], $arraySituacao));
			$arrayPontuacao[$chaveCliente][$chaveSituacao] = $linha['qtd'];
		}

		/*
		------------- LISTANDO CLIENTES CLIENTES E DECLARANDO VALORES NULOS A PONTUACAO
		--------------------------------------------------------------
		*/
		$sql 		 = "SELECT tag_sistema FROM control_sistema";
		$resultado 	 = mysql_query($sql);
		while($linha = mysql_fetch_array($resultado,MYSQL_ASSOC))
		{
			// array listando clientes
			if(!in_array($linha['tag_sistema'], $arrayProduto))
			{
				$arrayProduto[] = $linha['tag_sistema'];
				$qtdLinhaProduto++;
			}
		}

		$arrayJson['listaClientes'] = $arrayProduto;


		// FAZENDO AS LISTAS COM AS SITUACOES E SEUS RESPECTIVOS VALORES
		for($i = 0 ; $i < $qtdLinhaSituacao ; $i++)
		{
			for($j = 0 ; $j < $qtdLinhaProduto ; $j++)
			{

				if(empty($arrayPontuacao[$j][$i]) || $arrayPontuacao[$j][$i] == '')
				{
					$arrayPontuacao[$j][$i] = null;
				}

				if(utf8_encode($arraySituacao[$i]) == "Expedição"){
					$arrayJson['Expedicao'][] = (int)$arrayPontuacao[$j][$i];
				}

				if(utf8_encode($arraySituacao[$i]) == "Fábrica"){
					$arrayJson['Fabrica'][]   = (int)$arrayPontuacao[$j][$i];
				}

				if(utf8_encode($arraySituacao[$i]) == "Homologação"){
					$arrayJson['Homologacao'][] = (int)$arrayPontuacao[$j][$i];
				}

				if(utf8_encode($arraySituacao[$i]) == "Validação"){
					$arrayJson['Validacao'][] = (int)$arrayPontuacao[$j][$i];
				}

			}

		}

		echo json_encode($arrayJson);
	}

}


$objPainel = new Painel();
$objPainel->$_GET['metodo']();

?>