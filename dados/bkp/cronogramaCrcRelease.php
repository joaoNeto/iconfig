<?php

require 'conexao.php';
if (isset($_GET['json']) && !empty($_GET['json'])) {
    // $id_rel = $_GET['idRel'];
    // $id_crc = $_GET['idCrc'];
    $jsonObj = json_decode($_GET['json']);


//cria o array de empregados
    $dados = $jsonObj->dados;

//imprime a data do arquivo e navega pelos elementos do array, imprimindo cada empregado. 
//caso o empregado possua dependentes, estes também são exibidos.
    foreach ($dados as $d) {
        $crc = "$d->numCrc";
        $id_rel = "$d->idRel";
        $link   = "$d->link";
        $funcao = "$d->funcao";

        //FASE 1 - VERIFICA SE O LINK NÃO VEIO VAZIO
        if ($funcao == 'cadastrar') {
                // INSERIR
                $insert = "insert into control_crc_cronograma
                   (id_release,num_crc,link)
                   values
                   ('$id_rel','$crc','$link')";
                $ResultInsert = @mysql_query($insert);
                if ($ResultInsert) {
                    echo '<div class="alert alert-success" role="alert"><b>Link Cadastrado com sucesso!</b></div> ';
                } else {
                    echo '<div class="alert alert-danger" role="alert"><b>Erro ao Cadastrar o Link! [ Link Já Cadastrado ] </b></div>';
                }
        }
        if($funcao == 'editar'){
            //UPDATE
            $update = "UPDATE control_crc_cronograma  
                       SET id_release = '$id_rel',
                           num_crc     = '$crc',
                           link       = '$link'
                       WHERE id_release = '$id_rel'
                         AND num_crc     = '$crc'";                        
                           
            $ResultUpdate = @mysql_query($update);
            if($ResultUpdate){
                    echo '<div class="alert alert-success" role="alert"><b>Link Editado Com Sucesso!</b></div>';
                } else {
                    echo '<div class="alert alert-danger" role="alert"><b>Erro ao Editar o Link!</b></div>';
                }
           }
        if($funcao == 'deletar'){
            //UPDATE
            $update = "DELETE FROM control_crc_cronograma  
                       WHERE id_release = '$id_rel'
                         AND num_crc     = '$crc'";                        
                           
            $ResultUpdate = @mysql_query($update);
            if($ResultUpdate){
                    echo '<div class="alert alert-success" role="alert"><b>Link Deletado Com Sucesso!</b></div>';
                } else {
                    echo '<div class="alert alert-danger" role="alert"><b>Erro ao Deletar o Link!</b></div>';
                }
           }
    }
}

if (isset($_GET['jsonBotoes']) && !empty($_GET['jsonBotoes'])) {
    // $id_rel = $_GET['idRel'];
    // $id_crc = $_GET['idCrc'];
    $jsonObj = json_decode($_GET['jsonBotoes']);


//cria o array de empregados
    $dados = $jsonObj->dados;

//imprime a data do arquivo e navega pelos elementos do array, imprimindo cada empregado. 
//caso o empregado possua dependentes, estes também são exibidos.
    foreach ($dados as $d) {
        $crc = "$d->numCrc";
        $id_rel = "$d->idRel";
        //pega informaçoes da crc em questao
        $sql = "SELECT num_crc FROM control_crc where num_crc = '$crc' and id_release = '$id_rel'";
        $result = @mysql_query($sql);
        $fetch = @mysql_fetch_array($result);
        $crc = $fetch['num_crc'];
         @session_start();
        if(getCronogramaCrc($id_rel,$crc)){
                   
                    if(isset($_SESSION['UsuarioNivel']) && $_SESSION['UsuarioNivel'] == 1 || isset($_SESSION['UsuarioNivel']) && $_SESSION['UsuarioNivel'] == 5 && $_SESSION['setor']==1){
                        
                        $cronograma  = ' <btnCrono style="margin-right:10px;"><button data-toggle="modal" onclick="AtualizaModalView('.$crc.','.$id_rel.')" data-target=".cronograma_view'.$crc.'" data-backdrop="static"><img src="image/cronograma.png" style="width:100%;" title="Cronograma"></button></btnCrono>';
                        $cronograma  .= '<btnCrono style="margin-right:10px;"><button data-toggle="modal" data-target=".cronograma_edit'.$crc.'" data-backdrop="static"><img src="image/Editar.png" style="width:100%;" title="Editar Link"></button></btnCrono>';
                        $cronograma  .= '<btnCrono><button data-toggle="modal" data-target=".cronograma_delet'.$crc.'" data-backdrop="static"><img src="image/delete.jpg" style="width:100%;" title="Excluir Link"></button></btnCrono>';
                        
                    }else{
                        $cronograma = '<btnCrono><button data-toggle="modal" data-target=".cronograma_view'.$crc.'" data-backdrop="static"><img src="image/cronograma.png" style="width:100%;" title="Cronograma"></button></btnCrono>';
                    }
    }else{
                    if(isset($_SESSION['UsuarioNivel']) && $_SESSION['UsuarioNivel'] == 1 || isset($_SESSION['UsuarioNivel']) && $_SESSION['UsuarioNivel'] == 5 && $_SESSION['setor']==1){
                        $cronograma  = '<btnCrono><button data-toggle="modal" data-target=".cronograma_novo'.$crc.'" data-backdrop="static"><img src="image/btnNovo.png" style="width:100%;" title="Cadastrar Link"></button></btnCrono>';
                    }else{
                        $cronograma = '';
                    }
                     
                }
                
                echo $cronograma;
    }
}

if(isset($_GET['jsonModalView']) && !empty($_GET['jsonModalView'])) {
    // $id_rel = $_GET['idRel'];
    // $id_crc = $_GET['idCrc'];
    $jsonObj = json_decode($_GET['jsonModalView']);


//cria o array de empregados
    $dados = $jsonObj->dados;

//imprime a data do arquivo e navega pelos elementos do array, imprimindo cada empregado. 
//caso o empregado possua dependentes, estes também são exibidos.
    foreach ($dados as $d) {
        $crc = "$d->numCrc";
        $id_rel = "$d->idRel";
       

        
        $result = getCronogramaCrc($id_rel, $crc);
        
echo '<iframe src="' . $result . '?widget=true&amp;headers=false"></iframe>';
        
    }
}

if (isset($_GET['jsonNovoModal']) && !empty($_GET['jsonNovoModal'])) {
    // $id_rel = $_GET['idRel'];
    // $id_crc = $_GET['idCrc'];
    $jsonObj = json_decode($_GET['jsonNovoModal']);


//cria o array de empregados
    $dados = $jsonObj->dados;

//imprime a data do arquivo e navega pelos elementos do array, imprimindo cada empregado. 
//caso o empregado possua dependentes, estes também são exibidos.
    foreach ($dados as $d) {
        $crc = "$d->numCrc";
        $id_rel = "$d->idRel";
        $funcao = "$d->funcao";
        //pega informaçoes da crc em questao
        $sql = "SELECT num_crc FROM control_crc where num_crc = '$crc' and id_release = '$id_rel'";
        $result = @mysql_query($sql);
        $fetch = @mysql_fetch_array($result);
        $crc = $fetch['num_crc'];
        
        $modal = "";
        if($funcao == 'cadastrar'){
        $modal .= CriarModal("view", $crc,$id_rel);
        $modal .= CriarModal("editar", $crc,$id_rel);
        $modal .= CriarModal("deletar", $crc,$id_rel);
        $modal .= CriarModal("cadastrar", $crc,$id_rel);
        }
        if($funcao == 'deletar'){
        $modal .= CriarModal("view", $crc,$id_rel);
        $modal .= CriarModal("editar", $crc,$id_rel);
        $modal .= CriarModal("deletar", $crc,$id_rel);
        $modal .= CriarModal("cadastrar", $crc,$id_rel);
        }
        echo $modal;
        
    }
}

function CriarModal($tipo,$crc,$id_rel){
     
    if($tipo == "view"){
     $top = "Cronograma da CRC : ".$crc ;
    
       $html = '<div class="modal fade cronograma_view'.$crc.'" tabindex="-1" role="dialog" aria-labelledby="cronograma_view" aria-hidden="true">';
       $html .= ' <div class="modal-dialog modal-lg" style="width:100%;">';
       $html .= '  <div class="modal-content" >';
       $html .= '   <div class="panel panel-default" >';
       $html .= '    <div class="panel-heading" >';
       $html .= '     <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-target="cronograma_view'.$crc.'"><span aria-hidden="true">&times;</span></button>';
       $html .= '      <a data-toggle="collapse" data-parent="#accordion" href="#collapseFet" style="color: #0E4571; text-decoration:none;" onclick="mudaSetaFet();">'.$top.'</a>';
       $html .= '    </div>';
       $html .= '  <style>
                        iframe{
                            border: 0;
                            width: 100%;
                            height: 500;
                        }
                    </style>';
                    
       $html .= '<div id="atualizamodalview'.$crc.'">
               </div>
            </div>
        </div>
    </div>
</div> ';
     $html .= '';  
       
    
}

    if($tipo == "editar"){
           $top = "Edição de Link do Cronograma da CRC : ".$crc;
    
    $html = '<div class="modal fade cronograma_edit'.$crc.'" tabindex="-1" role="dialog" aria-labelledby="cronograma_edit" aria-hidden="true">';
    $html .= ' <div class="modal-dialog modal-lg" style="width:80%;">';
    $html .= '  <div class="modal-content" >
                 <div class="panel panel-default" >
                  <div class="panel-heading" >
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-target="cronograma_edit'.$crc.'"><span aria-hidden="true">&times;</span></button>';
    $html .= '      <a data-toggle="collapse" data-parent="#accordion" href="#collapseFet" style="color: #0E4571; text-decoration:none;" onclick="mudaSetaFet();">'.$top.'</a>';
    $html .= '    </div>';
    $html .= '  <div id="ResultEditCrono'.$crc.'"> <br> </div>';
    $html .= ' <textarea class="form-control" name="editLink'.$crc.'" id="editLink'.$crc.'" cols="150" rows="2">'; 
    $html .= getCronogramaCrc($id_rel, $crc);
    $html .='</textarea>';
    $html .= '  <br>';
    $html .= '<img src="image/BotaoSalvar.png" style="width:25px;cursor:pointer;" title="Salvar Alterações" onclick="EditarLink(\'ResultEditCrono'.$crc.'\',\''.$crc.'\',\''.$id_rel.'\',\'editLink'.$crc.'\',\'editar\');">';
    $html .= ' </div>
              </div>
            </div>
        </div>';
    $html .= '';
    }
    
    if($tipo == "cadastrar"){
           $top = "Cadastro de Link do Cronograma da CRC : ".$crc;
    
    $html = '<div class="modal fade cronograma_novo'.$crc.'" tabindex="-1" role="dialog" aria-labelledby="cronograma_novo" id="cronograma_novo'.$crc.'" aria-hidden="true">';
    $html .= '<div class="modal-dialog modal-lg" style="width:80%;">
                <div class="modal-content" >
                    <div class="panel panel-default" >
                        <div class="panel-heading" >
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="CriarModal(\'novosmodais'.$crc.'\',\''.$crc.'\',\''.$id_rel.'\',\'cadastrar\');" data-target="cronograma_novo'.$crc.'"><span aria-hidden="true">&times;</span></button>';
    $html .=       $top.'
                </div>';
    $html .= '  <div id="ResultNovoCrono'.$crc.'">  </div>';
    $html .= '   <textarea class="form-control" name="novoLink'.$crc.'" id="novoLink'.$crc.'" cols="150" rows="2"></textarea>';
    $html .= '   <br>';
    $html .= '  <img src="image/BotaoSalvar.png" style="width:25px;cursor:pointer;" title="Cadastrar Link" onclick="CriarLink(\'ResultNovoCrono'.$crc.'\',\''.$crc.'\',\''.$id_rel.'\',\'novoLink'.$crc.'\',\'cadastrar\');">';
    $html .= '</div>
        </div>
    </div>
</div>';
    $html .= ' ';
    }
    
    if($tipo == "deletar"){
        $top = "Excluir Link do Cronograma da CRC : ".$crc;
    $html = ' <div class="modal fade cronograma_delet'.$crc.'" tabindex="-1" role="dialog" aria-labelledby="cronograma_delet" aria-hidden="true">
                <div class="modal-dialog modal-lg" style="width:80%;">
                    <div class="modal-content" >
                        <div class="panel panel-default" >
                            <div class="panel-heading" >
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="CriarModal(\'novosmodais'.$crc.'\',\''.$crc.'\',\''.$id_rel.'\',\'deletar\');" data-target="cronograma_delet'.$crc.'"><span aria-hidden="true">&times;</span></button>';
   $html .= '       <a data-toggle="collapse" data-parent="#accordion" href="#collapseFet" style="color: #0E4571; text-decoration:none;" onclick="mudaSetaFet();">'.$top.'</a>';
   $html .= '      </div>';
   $html .= '   <div id="ResultDeletCrono'.$crc.'"> <br>';
   $html .= '   <br>';
   $html .= '  <button class="btn btn-success" style="cursor:pointer;" title="Excluir Link" onclick="DeletarLink(\'ResultDeletCrono'.$crc.'\',\''.$crc.'\',\''.$id_rel.'\',\'ResultDeletCrono'.$crc.'\',\'deletar\');">Confirmar Exclusão</button>';
   $html .= '   </div>
            </div>
        </div>
    </div>
</div>';
   
    
}
    return $html;
}

function getCronogramaCrc($id_rel, $crc) {
    require 'conexao.php';
    $sql = "SELECT id_cronograma,
                id_release,
                num_crc,
                link
           FROM control_crc_cronograma
           WHERE id_release = '$id_rel'
            AND num_crc     = '$crc'";

    $exec = mysql_query($sql);
    $count = mysql_num_rows($exec);
    if ($count > 0) {
        $fetch = mysql_fetch_array($exec);
        return $fetch['link'];
    } else {
        return false;
    }
}

function verificaTipoMudanca($crc){
    require 'conexao.php';
    $sqlTipoMudanca = "SELECT CRC.SMCCode
                      FROM SMCandidata AS CRC
                     WHERE CRC.SMCCode = 'CRC$crc'
                       AND CRC.TipoMudanca in (127);";
    if($connect == true){
    $resultTipoMudanca = odbc_exec($connect, $sqlTipoMudanca);
    $Nrows = odbc_num_rows($resultTipoMudanca);
 
    if($Nrows > 0){
     return TRUE;
     //return FALSE;
 }else
     return FALSE;

}
 
}


$CrcCronograma = "SELECT crc.id_release,
                             crc.id_crc,
                            crc.num_crc,
                          crc.descricao,
                           crc.situacao,
                             crc.estado,
                           crc.customer 
                    FROM control_crc crc, 
                         control_crc_cronograma cro,
                         control_release rel
                   WHERE crc.customer <> 'Informata '
                     AND cro.num_crc = crc.num_crc
                     AND cro.id_release = rel.id_release
                     AND rel.id_release = crc.id_release
                     AND rel.data_exp = '0000-00-00 00:00:00'
                     order by crc.num_crc";
mysql_query("SET NAMES UTF8");
$exec_CrcCronograma = mysql_query($CrcCronograma,$conn);

?>