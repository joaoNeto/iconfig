<?php


function getResponsavel($crc,$conn){
   $sqlResp = "SELECT ST.SHCode,ST.SHNome, CL.RecordedDate   
  FROM CL_CandidateAllocationHistory CL,
       STAKEHOLDER ST
 WHERE CL.SMCCode = 'CRC$crc'
   AND CL.Responsible = ST.SHCode 
 ORDER BY CL.RecordedDate DESC ";
   $resultResp = odbc_exec($conn, $sqlResp);
   $fetchResp = odbc_fetch_row($resultResp);
   $Responsavel =  odbc_result($resultResp, 'SHNome'); 
    
    return $Responsavel;
}

/*$sqlCriticas = 
'SELECT DISTINCT CRC.SMCCode,CRC.prioridade,
CRC.Subject,stat.sitDesc,CRC.DtSubmissao,esta.estadoDesc,CLI.CustomerName
FROM SMCRelatedSMC AS CRC_CR,
SMCandidata AS CRC,
SOLICITACAOMUDANCA AS CR,
SCM_Release AS REL,
SituacaoSMC as stat,
EstadosSMC as esta,
CL_Customer as CLI
WHERE CRC_CR.SMCode = CRC.SMCCode
AND CRC_CR.relSMFormCode = CR.SMCode
AND CR.NextRelease = REL.Code
AND CRC.CustomerCode = CLI.Code
AND stat.sitCode = crc.Situacao
AND esta.estadoCod = crc.estado
AND REL.LineCode <> -1
AND stat.sitCode NOT IN (3,5,10)
AND CRC.prioridade = 4
AND CRC.TipoMudanca in (4,7,130,138,142)';*/

/*$sqlCriticas = 'SELECT DISTINCT CRC.SMCCode,CRC.prioridade,
CRC.Subject,stat.sitDesc,CRC.DtSubmissao,esta.estadoDesc,CLI.CustomerName
FROM SMCRelatedSMC AS CRC_CR,
SMCandidata AS CRC,
SOLICITACAOMUDANCA AS CR,
SituacaoSMC as stat,
EstadosSMC as esta,
CL_Customer as CLI
WHERE CRC_CR.SMCode = CRC.SMCCode
AND CRC_CR.relSMFormCode = CR.SMCode
AND CRC.CustomerCode = CLI.Code
AND stat.sitCode = crc.Situacao
AND esta.estadoCod = crc.estado
AND stat.sitCode NOT IN (3,5,10)
AND CRC.prioridade = 4
AND CRC.TipoMudanca in (4,7,130,138,142);';*/

$sqlCriticas = 'SELECT DISTINCT CRC.SMCCode,CRC.prioridade,
CRC.Subject,stat.sitDesc,CRC.DtSubmissao,esta.estadoDesc,CLI.CustomerName
FROM
SMCandidata AS CRC,
SituacaoSMC as stat,
EstadosSMC as esta,
CL_Customer as CLI
WHERE CRC.CustomerCode = CLI.Code
AND stat.sitCode = crc.Situacao
AND esta.estadoCod = crc.estado
AND stat.sitDesc <> "Registrado"
AND stat.sitCode NOT IN (3,5,10)
AND CRC.prioridade in (2,4)
AND CRC.TipoMudanca in (4,7,130,138,142);';




 $resultCriticas = odbc_exec($connect, $sqlCriticas);
 $Nrows = odbc_num_rows($resultCriticas);
 
 $verificaCritica = verCrcCriticas($Nrows);
 function verCrcCriticas ($rows){
 if($rows > 0){
     return TRUE;
     //return FALSE;
 }else
     return FALSE;
}