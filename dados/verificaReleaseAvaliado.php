<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


    
    if(isset($_GET['codigo'])){
        $codigo = $_GET['codigo'];
    }
    if(isset($_GET['s']) && $_GET['s'] == 'AJAX'){
        //1 significa que o retorno vai ser uma tag HTML
        $ajax = 1;
    }else{
        //0 significa que o retorno vai ser true ou false
        $ajax = 0;
    }
    
    


function verificaReleaseAvaliado($codigo,$ajax){
    
    
    require 'conexao.php';
    
         
    $sqlRelAvalidado = "SELECT qua.id_quali_rel,
                               qua.id_release,
                               qua.percentual,
                               rel.branch,
                               qua.observacao,
                               qua.arquivo,
                               qua.avaliado
                          FROM control_quali_rel qua,
                               control_release rel
                         WHERE qua.id_release = '$codigo'
                           AND rel.id_release = qua.id_release";
    if($conn == true){
        mysql_query("SET NAMES UTF8");
        $resultsql = mysql_query($sqlRelAvalidado,$conn);
        $row = mysql_num_rows($resultsql);
        while($fetchResult = mysql_fetch_array($resultsql)){
              $id_quali_rel = $fetchResult['id_quali_rel'];
              $percentual   = $fetchResult['percentual'];
              $versao       = $fetchResult['branch'];
              $descricao    = $fetchResult['observacao'];
              $arquivo      = $fetchResult['arquivo'];
              $avaliado     = $fetchResult['avaliado'];
            
        }
        if($ajax == 1){
            if($row == 0){
                echo '<span class="label label-danger">Qualidade Não Avaliada!</span>';
            }
            if($row > 0){
                echo '<input id="dataExp" name="dataExp" class="form-control input-md " maxlength="10"> ';
            }
        }else   if($ajax == 0){
            if($row == 0){
                return null;
            }
            if($row > 0){
                return montaEmoticonsQualidade($percentual,$id_quali_rel,$versao,$descricao,$arquivo,$avaliado);
            }
        }else  if($ajax == 2){
            return $row;
        }
        
    }else  return false;

}

if($ajax == 1){
$result = verificaReleaseAvaliado($codigo,$ajax);
}


//funcao para o viewReleaseAvaliado

function getInfoQualiRel($codigo){
    
    require 'conexao.php';
    
         
    $sqlRelAvalidado = "SELECT id_quali_rel,id_release,percentual,observacao,arquivo,avaliado 
            FROM control_quali_rel WHERE id_quali_rel = '$codigo'";
    if($conn == true){
        return $resultsql = mysql_query($sqlRelAvalidado,$conn);
        
    }
}


function getPercQualiRel($branch){
    
    require 'conexao.php';
    
         
     $sqlRelAvalidado = "SELECT qua.id_quali_rel,
                               qua.id_release,
                               qua.percentual,
                               rel.branch,
                               qua.observacao,
                               qua.arquivo,
                               qua.avaliado
                          FROM control_quali_rel qua,
                               control_release rel
                         WHERE qua.id_release = '$branch'
                           AND rel.id_release = qua.id_release";
    if($conn == true){
        $resultsql = mysql_query($sqlRelAvalidado,$conn);
        $row = mysql_num_rows($resultsql);
        while($fetchResult = mysql_fetch_array($resultsql)){
              $id_quali_rel = $fetchResult['id_quali_rel'];
              $percentual   = $fetchResult['percentual'];
              $versao       = $fetchResult['branch'];
              $descricao    = $fetchResult['observacao'];
              $arquivo      = $fetchResult['arquivo'];
              $avaliado     = $fetchResult['avaliado'];
            
        }
        return montaEmoticonsQualidade($percentual,$id_quali_rel,$versao,$descricao,$arquivo,$avaliado);
}
}

function montaEmoticonsQualidade($percentual,$codigo,$versao,$descricao,$arquivo,$avaliado){
    if(!isset($_SESSION['UsuarioNivel'])){
        $_SESSION['UsuarioNivel'] = null;
    }
    if(isset($_GET['p']) && $_GET['p']== "Plan" && $_SESSION['UsuarioNivel'] == 5){
        $editar  = '<a href="?p=EditQualiRel&cod='.$codigo.'"><span title="Editar Avaliação" class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>';
    }else{
        $editar = '';
    }
    if($avaliado == "N"){
        return '<a style="cursor:pointer;" data-toggle="modal" title="Não Avaliado" data-target=".modal-NaoAvaliado'.$codigo.'">N/A</a>'
                . '<div class="modal fade modal-NaoAvaliado'.$codigo.'" tabindex="-1" role="dialog" aria-labelledby="modal-NaoAvaliado'.$codigo.'" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="modal-NaoAvaliado'.$codigo.'"><span style="margin-right:30%;">O Release: '.$versao.' não pode ser avaliado!</span>'.$editar.'</h3>
      </div>
        <div class="modal-body">
            <textarea cols="140" rows="20" readonly style="resize:none; font-size:12px;">'.$descricao.'</textarea>
      </div>
      <div class="modal-footer">
         
    </div>
    </div>
  </div>
</div>    ';  
    }
    if($percentual >= 95){
        return '<a data-toggle="modal" data-target=".modal-BoaQualidade'.$codigo.'"><img src="image/boaQualidade.png" style="width: 20px; cursor:pointer;" title="Boa Qualidade '.$percentual.'%" /></a>'
                . '<div class="modal fade modal-BoaQualidade'.$codigo.'" tabindex="-1" role="dialog" aria-labelledby="modal-BoaQualidade'.$codigo.'" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="modal-BoaQualidade'.$codigo.'">Avaliação de Qualidade do Release:'.$versao.' <img style="width:50px; "  title="Acima de 70%" src="image/boaQualidade.png"><span  title="Acima de 70%" style="color:gold; margin-right:20%;">'.$percentual.'%</span>'.$editar.'</h3>
      </div>
        <div class="modal-body">
            <textarea cols="140" rows="20" readonly style="resize:none; font-size:12px;">'.$descricao.'</textarea>
      </div>
      <div class="modal-footer">
         <h4>Para fazer o Download da avaliação clique <a href="arquivos/QualidadeReleases/'.$arquivo.'"><button type="button" class="btn btn-info">Aqui!</button></a>
    </div>
    </div>
  </div>
</div>    ';  
    }
    if($percentual >= 80 && $percentual < 95){
        return '<a data-toggle="modal" data-target=".modal-QualidadeRegular'.$codigo.'"><img src="image/qualidadeRegular.png" style="width: 20px; cursor:pointer;" title="Qualidade Regular '.$percentual.'%" /></a>'
                . '<div class="modal fade modal-QualidadeRegular'.$codigo.'" tabindex="-1" role="dialog" aria-labelledby="modal-QualidadeRegular'.$codigo.'" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="modal-QualidadeRegular'.$codigo.'">Avaliação de Qualidade do Release:'.$versao.' <img style="width:50px; "  title="Acima de 70%" src="image/qualidadeRegular.png"><span  title="Entre 50 e 69%" style="color:silver;margin-right:20%;">'.$percentual.'%</span>'.$editar.'</h3>
      </div>
        <div class="modal-body">
            <textarea cols="140" rows="20" readonly style="resize:none; font-size:12px;">'.$descricao.'</textarea>
      </div>
      <div class="modal-footer">
        <h4>Para fazer o Download da avaliação clique <a href="arquivos/QualidadeReleases/'.$arquivo.'"><button type="button" class="btn btn-info">Aqui!</button></a>
    </div>
    </div>
  </div>
</div>    ';   
    }
    if($percentual < 80){
        return '<a data-toggle="modal" data-target=".modal-BaixaQualidade'.$codigo.'"><img src="image/baixaQualidade.png" style="width: 20px; cursor:pointer;" title="Baixa Qualidade '.$percentual.'%" /></a>'
                . '<div class="modal fade modal-BaixaQualidade'.$codigo.'" tabindex="-1" role="dialog" aria-labelledby="modal-BaixaQualidade'.$codigo.'" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="modal-BaixaQualidade'.$codigo.'">Avaliação de Qualidade do Release:'.$versao.' <img style="width:50px; "  title="Abaixo a de 50%" src="image/baixaQualidade.png"><span  title="Entre 50 e 69%" style="color:#CD7F32;margin-right:20%;">'.$percentual.'%</span>'.$editar.'</h3>
      </div>
        <div class="modal-body">
            <textarea cols="140" rows="20" readonly style="resize:none; font-size:12px;">'.$descricao.'</textarea>
      </div>
      <div class="modal-footer">
       <h4>Para fazer o Download da avaliação clique <a href="arquivos/QualidadeReleases/'.$arquivo.'"><button type="button" class="btn btn-info">Aqui!</button></a>
    </div>
    </div>
  </div>
</div>    '; 
    }
    
}
