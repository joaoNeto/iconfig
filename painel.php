<?php 

require 'dados/conexao.php'; 

session_start();


# Inicnando a variavel que vai indentificar se temos que exibir o modal ou não
$exibirModal = false;
# Verificando se não existe o cookie
if(!isset($_COOKIE["usuarioVisualizouModal"]))
{
# Caso não exista entra aqui.

# Vamos criar o cookie com duração de 1 semana
$diasparaexpirar = 1;
setcookie('usuarioVisualizouModal', 'SIM', (time() + ($diasparaexpirar) * 1 * 1440 ));

# Seto nossa variavel de controle com o valor TRUE ( Verdadeiro)
$exibirModal = true;
}

?>
<html ng-app='myApp'>
<head>
   
    <link rel="icon" type="image/ico" href="favicon.ico">
    
    <title>iconfig</title>
    <!-- \/ Alinha o tamanho do conteúdo do documento ao tamanho da tela (device-width)-->
    <meta name="viewport" content="width=device-width" charset=UTF8>
    
    <!-- \/CSS                                                                              -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/estilo.css" rel="stylesheet" type="text/css" />   
  
    <!-- \/ JS       -->

    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/bootstrap.js" type="text/javascript"></script>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
    <link rel='stylesheet' type='text/css' href='css/styles.css' />
    <!--<script src='js/jquery.min.js'></script>-->
    <script type='text/javascript' src='css/menu_jquery.js'></script>
    
   

    
    

        
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
 <?php if(isset($_GET['p']) && $_GET['p'] <> 'Bases'){
     
    echo '<style>
           body {visibility:hidden}
       </style>';
     
 } ?>
<body>

    <?php 
    
    
    
    include 'include/cabecalho.php';
            
    
    ?>
    <div id="conteudo">
        <?php if(isset($_GET['p']) && $_GET['p'] == 'Plan' ){

                include('grid/planejamento_release.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'Env' ){

                include('grid/releases_enviados.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'HistRel' ){

                include('grid/historico_release.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'GeraRel' ){

                include('include/geraNotes.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'GeraRelPdf' ){

                include('include/geraNotesPdf.php');
        } if(isset($_GET['SearchCliente']) || isset($_GET['SearchSistema'])){

                include('grid/planejamento_release.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'crc'){

                include('grid/planejamento_release_crc.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'Geren'){
                
            if(isset($_SESSION['UsuarioID'])){
                
                       include 'grid/gerencia_release.php';
                    
               }else{    include('include/login.php');}
               
        }if(isset($_GET['p']) && $_GET['p'] == 'newEmerg'){
                
            if(isset($_SESSION['UsuarioID'])){
                        
                       include 'include/novoReleaseEmerg.php';
                        
                    }
               else{
                    include('include/login.php');
               }
        } if(isset($_GET['p']) && $_GET['p'] == 'GerRel'){
             if(isset($_SESSION['UsuarioID'])){
                        
                       include 'grid/gerencia_release.php';
                        
                    }
               else{include('include/login.php');
               }
        } if(isset($_GET['p']) && $_GET['p'] == 'GerenViag'){
             if(isset($_SESSION['UsuarioID'])){
                        
                       include 'grid/gerencia_viagens.php';
                        
                    }
               else{include('include/login.php');
        }
        }if(isset($_GET['p']) && $_GET['p'] == 'GerenAtiv'){
             if(isset($_SESSION['UsuarioID'])){
                        
                       include 'grid/gerencia_atividades.php';
                        
                    }
               else{include('include/login.php');
        }
        }if(isset($_GET['op']) && $_GET['op'] == 'Edit'){
               
            include ('include/editaRelease.php');
               
        }if(isset($_GET['op']) && $_GET['op'] == 'EditViag'){
               
            include ('include/editaViagem.php');
               
        } if(isset($_GET['op']) && $_GET['op'] == 'EditReport'){
               
            include ('include/editaReport.php');
               
        } if(isset($_GET['p']) && $_GET['p']  == 'Home' ) {
            
            include 'include/home.php';
        }if(isset($_GET['op']) && $_GET['op'] == 'DeletViag'){

                include('include/deletaViagem.php');
               
        } if(isset($_GET['op']) && $_GET['op'] == 'DeletAtiv'){

                include('include/deletaAtividade.php');
               
        }if(isset($_GET['p']) && $_GET['p'] == 'ReportViag'){

                include('include/reportaViagem.php');
               
        } if(isset($_GET['im']) && $_GET['im'] == 'Mar'){
            echo '<center>
        <image src="image/reuniaoMarco2014.png" >
        </center>';
        } if(isset($_GET['im']) && $_GET['im'] == 'Abr'){
            echo '<center>
        <image src="image/reuniao05052014.png" >
        </center>';
        } if(isset($_GET['p']) && $_GET['p']  == 'Papeis' ) {
            
            include 'include/papeis.php';
        }if(isset($_GET['p']) && $_GET['p']  == 'Papel' ) {
            
            include 'include/papel.php';
        }if(isset($_GET['op']) && $_GET['op']  == 'Novo' ) {
            
            include 'include/novoRelease.php';
        }if(isset($_GET['op']) && $_GET['op']  == 'NovaViag' ) {
            
            include 'include/novaViagem.php';
        }if(isset($_GET['op']) && $_GET['op']  == 'Login' ) {
            
            include 'include/login.php';
        }if(isset($_GET['p']) && $_GET['p']  == 'Bases' ) {
            
            include 'include/versoesbase.php';
        } if(isset($_GET['bd'] )) {
            
             include 'grid/versoesbase.php';
        }
         if(isset($_GET['action']) && $_GET['action']  == 'sair' ) {
            session_unset();
            session_destroy();
        echo '<script>window.location="?p=Plan";</script>';   
        
           
        } if(isset($_GET['p']) && $_GET['p']  == 'ApMensalTime' ) {
            
            include 'grid/aplicacoes_mensal.php';
        } if(isset($_GET['p']) && $_GET['p']  == 'Viag' ) {
            
            include 'grid/viagens.php';
        } if(isset($_GET['p']) && $_GET['p']  == 'AtiViag' ) {
            
            include 'grid/atividades_viagens.php';
        } if(isset($_GET['p']) && $_GET['p']  == 'MntBc' ) {
            include 'include/monitoraBanco.php';
        } if(isset($_GET['gridBc']) && $_GET['gridBc']  == 'TOP_CPU' ) {
            include 'grid/top_cpu.php';
        } if(isset($_GET['fluxo'])) {
            include './include/fluxo.php';
            
        }if(isset($_GET['op']) && $_GET['op'] == 'Delet'){

                include('include/deletaRelease.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'Aloc'){
            include('grid/alocacao.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'GerenAloc'){
            if(isset($_SESSION['UsuarioID'])){
                        
            include('grid/gerencia_alocacao.php');
            }
               else{include('include/login.php');
        }
        }if(isset($_GET['op']) && $_GET['op'] == 'NovaAloc'){
            if(isset($_SESSION['UsuarioID'])){
                        
                    include('include/novaAlocacao.php');
                        
                    }
               else{include('include/login.php');
        }
         
        }if(isset($_GET['op']) && $_GET['op'] == 'EditAloc'){
            if(isset($_SESSION['UsuarioID'])){
                        
                    include('include/editaAlocacao.php');
                        
                    }
               else{include('include/login.php');
        }
         
        }if(isset($_GET['op']) && $_GET['op'] == 'DeletAloc'){
            if(isset($_SESSION['UsuarioID'])){
                        
                    include('include/deletaAlocacao.php');
                        
                    }
               else{include('include/login.php');
        }
         
        }if(isset($_GET['p']) && $_GET['p'] == 'Eventos'){
            include('include/agendaEventosInformata.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'CronoCrc'){
            include('include/cronogramaCrcRelease.php');
        }if(isset($_GET['p']) && $_GET['p'] == 'CrcCrono'){
            include('grid/planejamento_crc_cronograma.php');
        }
        if(isset($_GET['p']) && $_GET['p'] == 'QualiRel'){
            include('include/novaQualidadeRelease.php');
        }
        if(isset($_GET['p']) && $_GET['p'] == 'EditQualiRel'){
            include('include/editaQualidadeRelease.php');
        }
        if(isset($_GET['p']) && $_GET['p'] == 'ViewQualiRel'){
            include('include/viewQualidadeRelease.php');
        }
        if(isset($_GET['p']) && $_GET['p'] == 'ViewEstRes'){
            include('grid/viewResultadoEst.php');
        }
        if(isset($_GET['p']) && $_GET['p'] == 'ViewDetailsResultEst'){
            include('include/viewResultadoEst.php');
        }
        if(isset($_GET['p']) && $_GET['p'] == 'acompCliente'){
            include('grid/acompanhaCliente.php');
        }
        if(isset($_GET['p']) && $_GET['p'] == 'ViewInfoNucleo'){
            include('grid/infoNucleo.php');
        }
        if(isset($_GET['idb'])){
            include('idb/page/montaTemplate.php');
        }
        if(isset($_GET['op']) && $_GET['op']  == 'NovoResult' ) {
            
            include 'include/novoResultado.php';
        }
        if(isset($_GET['p']) && $_GET['p']  == 'consultaCrc' ) {
            
            include 'dados/consultaCrc.php';
        }
        if(isset($_GET['p']) && $_GET['p']  == 'svn' ) {
            
            include 'grid/svn.php';
        }
		if(isset($_GET['p']) && $_GET['p']  == 'pdrWeb' ) {
            
            include 'grid/produtos_web.php';
        }if($_GET['p']  == 'painel' ){
            include 'grid/painel.php';
        }
        ?>
       <!-- Large modal -->
  
        
    </div>

    <button id="btNews" data-toggle="modal" data-target=".modalIconfigNews" style="display: none;"></button>
    <?php
require 'dados/iconfigNews.php'; 

$rel = getExpedicaoHoje(1);
if($exibirModal == true && $rel > 0) : 

    require 'include/modalNews.php';

    ?>

    
<script type="text/javascript">
$(document).ready(function()
{
document.getElementById('btNews').click();
});
</script>

<?php endif;?>


	
	</body>
        
</html>
<script type="text/javascript">
    onload=function(){
        document.body.style.visibility="visible"
    }
</script>